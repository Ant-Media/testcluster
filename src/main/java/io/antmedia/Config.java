package io.antmedia;

public class Config {
	public static final String SUBNET = "172.24.0.0/24";
	public static final String NETWORK_NAME="clusterTestNet";

	public static final String HOST_MACHINE_IP = "172.24.0.1";
	//mongo ip must be in change_mode.sh
	public static final String MONGO_1_IP = "172.24.0.2";
	public static final String ORIGIN_1_IP = "172.24.0.3";
	public static final String ORIGIN_2_IP = "172.24.0.4";

	//edge ip must be set in load balancer conf
	public static final String EDGE_1_IP = "172.24.0.6"; 
	public static final String EDGE_2_IP = "172.24.0.7";
	public static final String NGINX_IP = "172.24.0.8";
	public static final String STANDALONE_IP = "172.24.0.9";
	public static final String MONGO_2_IP = "172.24.0.10";
	public static final String MONGO_3_IP = "172.24.0.11";
	public static final String EDGE_3_IP = "172.24.0.12";
	public static final String EDGE_4_IP = "172.24.0.13";
	public static final String STANDALONE_2_IP = "172.24.0.14";

	
	
	public static final String MONGO_NAME = "mongo";
	public static final String NGINX_NAME = "nginx-lb";
	public static final String ORIGIN_1_NAME = "origin1";
	public static final String ORIGIN_2_NAME = "origin2";
	public static final String ORIGIN_3_NAME = "origin3";
	public static final String EDGE_1_NAME = "edge1"; 
	public static final String EDGE_2_NAME = "edge2";
	public static final String EDGE_3_NAME = "edge3";
	public static final String EDGE_4_NAME = "edge4";
	public static final String STANDALONE = "standalone";
    public static final String STANDALONE_2 = "standalone2";
	
	
	
	public static final String NONE_PRIVATE_SUBNET =  "132.24.0.0/24";
	public static final String NONE_PRIVATE_SUBNET_NAME="clusterTestNetNonePrivate";

	public static final String NONE_PRIVATE_MONGO_1_NAME = "NONE_PRIVATE_MONGO_1_NAME";
	public static final String NONE_PRIVATE_MONGO_1_IP = "132.24.0.2";
	
	public static final String NONE_PRIVATE_ORIGIN_1_NAME = "NONE_PRIVATE_ORIGIN_1_NAME";
	public static final String NONE_PRIVATE_ORIGIN_1_IP = "132.24.0.3";
	
	public static final String NONE_PRIVATE_ORIGIN_2_NAME = "NONE_PRIVATE_ORIGIN_2_NAME";
	public static final String NONE_PRIVATE_ORIGIN_2_IP = "132.24.0.4";
	
	
	
	public static final String TEST_LOG_DIRECTORY = "test_logs/";
	
}
