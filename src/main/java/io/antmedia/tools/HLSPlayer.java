package io.antmedia.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.awaitility.Awaitility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HLSPlayer {
	private String server;
	private String app;
	private Process process;
	private String stream;
	private File dir;
	private int id;
	
	
	private static Logger log = LoggerFactory.getLogger(HLSPlayer.class);

	private static BasicCookieStore httpCookieStore = new BasicCookieStore();

	
	public HLSPlayer(String serverIp, String appName, String streamId, File dir, int id) {
		this.server = serverIp;
		this.app = appName;
		this.stream = streamId;
		this.dir = dir;
		this.id = id;
	}

	public void start() {
		
		
		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(() -> {
			log.info("Checking m3u8 url");
			return checkURLExist("http://"+server+":5080/"+app+"/streams/"+stream+".m3u8");
		});
		
		
		String command = "ffmpeg -i http://"+server+":5080/"+app+"/streams/"+stream+".m3u8 -codec copy -f null /dev/null";
		log.info(command);
		
		ProcessBuilder pb = new ProcessBuilder(command.split(" "));
		pb.redirectOutput(new File(dir, id+"_hls_out.txt"));
		pb.redirectError(new File(dir, id+"_hls_err.txt"));
		try {
			process = pb.start();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		

	}
	
	public void stop() {
		process.destroyForcibly();
	}
	
	public static boolean checkURLExist(String url) throws Exception {
		int statusCode = getStatusCode(url, true);
		if (statusCode == 200) {
			return true;
		}
		return false;
	}
	
	public static StringBuffer getURL(String url) throws IOException
	{
		HttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy())
				.setDefaultCookieStore(new BasicCookieStore()).build();

		HttpUriRequest post = RequestBuilder.get().setUri(url).build();

		HttpResponse response = client.execute(post);

		return readResponse(response);
	}
	
	public static StringBuffer readResponse(HttpResponse response) throws IOException {
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		return result;
	}
	
	public static int getStatusCode(String url, boolean useCookie) throws Exception {

		HttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy())
				.setDefaultCookieStore(httpCookieStore).build();

		HttpUriRequest post = RequestBuilder.head().setUri(url).build();

		HttpResponse response = client.execute(post);

		return response.getStatusLine().getStatusCode();
	}
	
	
}
