package io.antmedia.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;


public class ConfigManager {

	private static Logger log = LoggerFactory.getLogger(ConfigManager.class);

	
	public static Config conf;
	static String file = "/usr/local/test/config.json";
	static String fileJson = ""; 

	public static String getConfig() {
		if(new File(file).exists()) {
			readFromFile();
			log.info(fileJson);
			conf = new Gson().fromJson(fileJson, Config.class);
		}
		else {
			conf = new Config();
		}

		return new Gson().toJson(conf);
	}

	public static void setConfig(String confJson) {
		log.info("conf:"+confJson);

		conf = new Gson().fromJson(confJson, Config.class);
		writeToFile();
	}

	private static void writeToFile() {
		try {
			FileWriter fw = new FileWriter(file);
			fw.write(new Gson().toJson(conf));
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void readFromFile() {
		try {
			FileInputStream fstream = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			
			fileJson = "";

			while ((strLine = br.readLine()) != null)   {
				fileJson += strLine;
			}

			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
