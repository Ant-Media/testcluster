package io.antmedia.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.awaitility.Awaitility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.Config;
import io.antmedia.tools.HLSPlayer;


public class DockerManager {
	//these must be convenient with name dockerfiles
	public static String ANTMEDIA_IMAGE="ant-media-server-enterprise-k8s:test";
	
	public static final String ANTMEDIA_IMAGE_MEDIA_PUSH="ant-media-server-enterprise-k8s:test-mediapush";

	public static final String ANTMEDIA_NVIDIA_IMAGE="amsgpu";

	public static final String MONGO_IMAGE="mongo:6.0-jammy";

	public static final String NGINX_IMAGE="nginx";


	public static final String SUDO = "sudo";

	private static Process tmpExec;

	private static Logger log = LoggerFactory.getLogger(DockerManager.class);

	public static void setImageId(String imageId) {
		ANTMEDIA_IMAGE = imageId;
	}

	/**
	 * Free style of command lets us better flexibility
	 * @param command
	 * @return
	 */
	public static synchronized Process execCommand(String command) {
		log.info("Running command -> " + command);
		tmpExec = null;
		new Thread() {
			@Override
			public void run() {
				ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", command);
				try {
					tmpExec = pb.start();
					InputStream istream = tmpExec.getErrorStream();
					int length;
					byte[] data = new byte[1024];
					while ((length = istream.read(data, 0, data.length)) > 0) {
						log.info("Process error output: " + new String(data, 0, length));
					}
				
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.start();

		int i = 0;
		while (tmpExec == null) {
			try {
				i++;
				if (i > 3) {
					System.err.println("Waiting for exec get initialized. It's waiting for " + i + "secs");
				}
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return tmpExec;
	}

	public boolean createNetwork(String subnet, String networkName) {
		execCommand("docker network create --driver=bridge --subnet="+ subnet + " " + networkName);

		try {
			return tmpExec.waitFor() == 0;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean createNetwork() {
		return createNetwork(Config.SUBNET,  Config.NETWORK_NAME);
	}

	//172.24.0.2:27017 
	/**
	 * This method call ant media server in cluster mode
	 * @param ip
	 * @param name
	 * @return
	 */
	public boolean createAntMediaContainer(String ip, String name, String mongoIP, String networkname, boolean waitUntilRunning) {
		Process process = execCommand("docker run -dit --name " + name + " --net "+ networkname + " --ip " + ip + " --privileged " + 
				ANTMEDIA_IMAGE + " -h " + mongoIP + " -m cluster -c 95 -e 95 ");

		boolean result = false;
		if (waitUntilRunning) {
			result = isContainerRunning(name);
		}
		try {
			result = process.waitFor() == 0;
		} catch (InterruptedException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
		}
		return result;
	}
	
	public boolean createAntMediaContainer(String ip, String name, String mongoIP, boolean waitUntilRunning)  {
		return createAntMediaContainer(ip, name, mongoIP,  Config.NETWORK_NAME, waitUntilRunning);
	}

	/**
	 * This method starts ant media server container in standalone mode
	 * @param ip
	 * @param name
	 * @return
	 */
	public boolean createAntMediaStandaloneContainer(String ip, String name) {
		execCommand("docker run -dit --name " + name + " --net "+ Config.NETWORK_NAME + " --ip " + ip + " --privileged " +
				ANTMEDIA_IMAGE + " -c 95 -e 95");

		return isContainerRunning(name);
	}


	public boolean createAntMediaGPUContainer(String ip, String name) {
		execCommand("docker run -dit --name " +  name +  " --runtime nvidia --net " + Config.NETWORK_NAME +
				"  --ip " + ip + " --privileged " + ANTMEDIA_NVIDIA_IMAGE);

		Utils.mustSleep(2000);

		return isContainerRunning(name);
	}
	
	public boolean createMongoContainer(String name, String network, String ip) 
	{
		execCommand("docker run -dit --name " +name +
				" --net " +  network +
				" --ip " + ip +
				" --privileged " +
				MONGO_IMAGE +
				" mongod ");

		return isContainerRunning(name);
	}
	
	

	public boolean createMongoContainer() {
		
		return createMongoContainer(Config.MONGO_NAME, Config.NETWORK_NAME, Config.MONGO_1_IP);
	}

	public boolean createNginxContainer() {

		//there is a install_and_configure_nginx.sh file deployed by yml file in Enterprise
		Process process = execCommand("./install_and_configure_nginx.sh -o \""+Config.ORIGIN_1_IP+","+Config.ORIGIN_2_IP+"\" -e \""+Config.EDGE_1_IP+","+Config.EDGE_2_IP+","+Config.EDGE_3_IP+"\" -d example.com -c");
		//this command creates a configured nginx.conf file in the same directory
		//We map that file below for the nginx container



		try {
			if (process.waitFor() != 0) {
				throw new IllegalStateException("Nginx configuration problem");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		File nginxConf = new File("nginx.conf");
		if (!nginxConf.exists()) {
			throw new IllegalStateException("Nginx configuration is not created");
		}


		execCommand("docker " +  " run " +  " -dit " +
				" --name " + Config.NGINX_NAME +
				" --net " + Config.NETWORK_NAME +
				" --ip " + Config.NGINX_IP +
				" -v "+ nginxConf.getAbsolutePath() +":/etc/nginx/nginx.conf" +  //map local config file
				" --privileged " +
				NGINX_IMAGE);

		return isContainerRunning(Config.NGINX_NAME);

	}


	public static boolean stopAndRemoveContainer(String containerName) {
		try {
			Process process = execCommand("docker " + " kill " + containerName);
			process.waitFor();
			process = execCommand("docker " + " rm " + containerName);

			return process.waitFor() == 0;
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		return false;
	}

	public boolean removeNetwork(String networkName) {
		Process execCommand = execCommand("docker " + " network " +  " rm " + networkName);
		try {
			int exitStatus = execCommand.waitFor();
			if (exitStatus != 0) {
				System.err.println("Cannot remove the network " + networkName);
			}
			return exitStatus == 0;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean removeNetwork() {
		return removeNetwork(Config.NETWORK_NAME);
	}
	

	public static void clearCluster() {
		//kill containers
		execCommand("docker " + " kill " + Config.MONGO_NAME + " " + Config.NGINX_NAME + " " + Config.ORIGIN_1_NAME +  " " +  Config.ORIGIN_2_NAME + " " +  Config.ORIGIN_3_NAME + " "
				+ " " + Config.EDGE_1_NAME + " " + Config.EDGE_2_NAME+ " " + Config.EDGE_3_NAME+ " " + Config.EDGE_4_NAME+ " " 
				
				);

		//remove containers
		execCommand("docker " + " rm " + Config.MONGO_NAME + " " + Config.NGINX_NAME + " " + Config.ORIGIN_1_NAME + " " +  " " +  Config.ORIGIN_2_NAME + " " +  Config.ORIGIN_3_NAME + " "
				+ " " + Config.EDGE_1_NAME + " " + Config.EDGE_2_NAME+ " " + Config.EDGE_3_NAME+ " " + Config.EDGE_4_NAME+ " " 
				
				);

	}

	public void clearStandalone() {
		execCommand("docker " + " kill " + Config.STANDALONE);

		//remove containers
		execCommand("docker " + " rm " + Config.STANDALONE);

	}
	
	public void clearStandalone2() {
		execCommand("docker " + " kill " + Config.STANDALONE_2);

		//remove containers
		execCommand("docker " + " rm " + Config.STANDALONE_2);

	}

	public static boolean isContainerRunningImmediate(String containerName) {
		Process  process = execCommand(" docker " + " inspect " + containerName);
		String out;
		try {
			InputStream istream = tmpExec.getInputStream();
			byte[] data = new byte[1024];
			int length = 0;
			StringBuffer stringBuffer = new StringBuffer();
			//don't print out, just read to not fill the buffer
			while ((length = istream.read(data, 0, data.length)) > 0) 
			{
				stringBuffer.append(new String(data, 0, length));
			}
			
			return stringBuffer.toString().contains("running");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isContainerRunning(String containerName) {

		try {
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				log.info("Checking if " + containerName + " is running");
				boolean isRunning = isContainerRunningImmediate(containerName);
				if (isRunning) {
					log.info(containerName + " is running now");
				}
				return isRunning;
			});

		}
		catch (Exception e) {
			//if it throws exception it means that container is not running
			System.err.println(containerName + " is not running");
			return false;
		}

		return true;
	}

	public boolean createMongoReplicaSet() {
		execCommand("docker " + " run " + " -dit " +
				" --name " + Config.MONGO_NAME +"1" +
				" --net " + Config.NETWORK_NAME +
				" --ip " + Config.MONGO_1_IP +
				" --privileged " +
				MONGO_IMAGE +
				" mongod " + " --replSet " + " my-mongo-set");

		execCommand("docker " + " run " + " -dit " +
				" --name " + Config.MONGO_NAME+"2" +
				" --net " + Config.NETWORK_NAME +
				" --ip " + Config.MONGO_2_IP +
				" --privileged " +
				MONGO_IMAGE +
				" mongod " + " --replSet " + " my-mongo-set");

		execCommand("docker " + " run " + " -dit " +
				" --name " + Config.MONGO_NAME+"3" +
				" --net " + Config.NETWORK_NAME +
				" --ip " + Config.MONGO_3_IP +
				" --privileged " +
				MONGO_IMAGE +
				" mongod "+ " --replSet " + " my-mongo-set");

		execCommand("docker " + " exec " + Config.MONGO_NAME+"1" + 
				" mongo " + " --eval " + 
				" config={_id:\"my-mongo-set\",members:[{_id:0,host:\"172.24.0.2:27017\"},{_id:1,host:\"172.24.0.12:27017\"},{_id:3,host:\"172.24.0.13:27017\"}]};rs.initiate(config);rs.status(); ");

		return true;
	}


	/**
	 * Run command in a container
	 * 
	 * @param containerName
	 * @param command
	 * @return true if command is successful
	 * false if command is not successful
	 */
	public boolean runCommand(String containerName, String command) {
		execCommand("docker" + " exec " + containerName+" " + command);
		try {
			return tmpExec.waitFor() == 0;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static Process getTmpExec() {
		return tmpExec;
	}



}
