package io.antmedia.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {
	
	private static Logger log = LoggerFactory.getLogger(Utils.class);

	public static void mustSleep(int delay) {
		try {
			log.info("wait for "+delay+" ms");
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
