package io.antmedia.utils;



import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.awaitility.Awaitility;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.cluster.ClusterNode;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.datastore.db.types.ConferenceRoom;
import io.antmedia.datastore.db.types.ConnectionEvent;
import io.antmedia.datastore.db.types.User;
import io.antmedia.model.Endpoint;
import io.antmedia.rest.BroadcastRestService.SimpleStat;
import io.antmedia.rest.model.BasicStreamInfo;
import io.antmedia.rest.model.Result;
import io.antmedia.settings.ServerSettings;

public class RestManager {

	static Gson gson = new Gson();


	private static Logger log = LoggerFactory.getLogger(RestManager.class);

	static CookieStore cookieStore = new BasicCookieStore();
	static HttpContext httpContext = new BasicHttpContext();
	static User user;
	static {
		user = new User();
		user.setFirstName("test");
		user.setLastName("antmedia.io");
		user.setPassword("testtest");
		user.setEmail("test@antmedia.io");
		httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
	}


	/**
	 * This file is copy paste of ApplicationInfo in ManagementConsole_WebApp project
	 */
	public static class ApplicationInfo {
		public String name;
		public int liveStreamCount;
		public int vodCount;
		public long storage;
	}

	public static List<ClusterNode> getClusterNodesFrom(String ip) {
		String url = getRootURL(ip)+"/cluster/nodes/0/10";
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		List<ClusterNode> nodes = null;

		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, httpContext);
			String content = EntityUtils.toString(response.getEntity());

			log.info("url:"+url);
			log.info("response:"+content);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			Type listType = new TypeToken<List<ClusterNode>>() {}.getType();

			nodes = gson.fromJson(content, listType);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return nodes;
	}

	private static String getRootURL(String ip) {
		if (ip.equals(Config.NGINX_IP)) {
			return "http://"+ip+":4445/rest/v2"; //dashboard http access is 4445
		}
		else {
			return "http://"+ip+":5080/rest/v2";
		}
	}

	public static boolean signUp(String ip) {
		boolean result = false;

		try {

			HttpUriRequest post = RequestBuilder.post().setUri(getRootURL(ip)+"/users/initial")
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.setEntity(new StringEntity(gson.toJson(user)))
					.build();

			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(post, httpContext);
			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			result = (response.getStatusLine().getStatusCode() == 200);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static boolean login(String ip) {
		return login(ip, null);
	}

	public static boolean login(String ip, HttpContext customHttpContext) {

		try {
			//PAY ATTENTION: The value(40) below is high because nginx timeout values are not optimized and it takes time to find the working node
			Awaitility.await().atMost(40, TimeUnit.SECONDS).pollDelay(2, TimeUnit.SECONDS).until(() -> 
			{
				boolean result = false;

				try {

					HttpUriRequest post = RequestBuilder.post().setUri(getRootURL(ip)+"/users/authenticate")
							.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
							.setEntity(new StringEntity(gson.toJson(user)))
							.build();

					CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
					CloseableHttpResponse response = client.execute(post, customHttpContext != null ? customHttpContext : httpContext);

					String content = EntityUtils.toString(response.getEntity());

					if (response.getStatusLine().getStatusCode() != 200) {
						log.info(response.getStatusLine()+content);
					}
					
					log.info("result string: " + content);

					result = (response.getStatusLine().getStatusCode() == 200);

				} catch (Exception e) {
					e.printStackTrace();
				}
				return result;
				
			});
			return true;
		}
		catch (Exception e) {
			log.error("Exception in login", e);
			return false;
		}

	}
	
	
	public static Result startMediaPush(String ip, String app, String url, HttpContext customContext, boolean directCall) throws ParseException, IOException {
		
		String requestUrl = null;
		
		if (directCall) {
			requestUrl = "http://" + ip + ":5080/" + app + "/rest/v1/media-push/start";
		}
		else {
			requestUrl = getRootURL(ip)+"/request?_path=/"+app+"/rest/v1/media-push/start";
		}
		
		HttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
		
		Endpoint endpoint = new Endpoint();
		endpoint.setWidth(1280);
		endpoint.setHeight(720);
		endpoint.setUrl(url);
		
		HttpUriRequest post = RequestBuilder.post().setUri(requestUrl).setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.setEntity(new StringEntity(gson.toJson(endpoint))).build();
		
		HttpResponse response = client.execute(post, customContext != null ? customContext : httpContext);

		String content = EntityUtils.toString(response.getEntity());

		log.info("result string: " + content);
		return gson.fromJson(content, Result.class);
		
	}
	
	public static Result stopMediaPush(String ip, String app, String streamId, HttpContext customContext, boolean directCall) throws ParseException, IOException {
		
		String requestUrl = null;
		
		if (directCall) {
			requestUrl = "http://" + ip + ":5080/" + app + "/rest/v1/media-push/stop/" + streamId ;
		}
		else {
			requestUrl = getRootURL(ip)+"/request?_path=/"+app+"/rest/v1/media-push/stop/" + streamId;
		}
		
		
		
		
		HttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();

		HttpUriRequest post = RequestBuilder.post().setUri(requestUrl)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();
		
		HttpResponse response = client.execute(post, customContext != null ? customContext : httpContext);

		String content = EntityUtils.toString(response.getEntity());

		log.info("result string: " + content);
		return gson.fromJson(content, Result.class);
		
	}


	public static Broadcast createBroadcast(String ip, String app, HttpContext customContext) throws Exception {
		return createBroadcast(ip, app, customContext, false, null);
	}
	
	


	public static Broadcast createBroadcast(String ip, String app, HttpContext customContext, boolean directCall, String subfolder) throws Exception{
		String url = null;

		if (directCall) {
			url = "http://" + ip + ":5080/" + app + "/rest/v2/broadcasts/create";
		}
		else {
			url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/create";
		}
		HttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
		Gson gson = new Gson();
		Broadcast broadcast = new Broadcast();
		broadcast.setName("testBroadcast");
		if (subfolder != null) {
			broadcast.setSubFolder(subfolder);
		}
		HttpUriRequest post = RequestBuilder.post().setUri(url).setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.setEntity(new StringEntity(gson.toJson(broadcast))).build();

		HttpResponse response = client.execute(post, customContext != null ? customContext : httpContext);

		String content = EntityUtils.toString(response.getEntity());

		if (response.getStatusLine().getStatusCode() != 200) {
			log.info(response.getStatusLine()+content);
		}
		log.info("result string: " + content);
		Broadcast tmp = gson.fromJson(content, Broadcast.class);

		return tmp;

	}

	public static Result updateBroadcast(String ip, String app, String streamId, Broadcast broadcast, boolean directCall, HttpContext customContext) {
		String url = null;

		if (directCall) {
			url = "http://" + ip + ":5080/" + app + "/rest/v2/broadcasts/" + streamId;
		}
		else {
			url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/" + streamId;
		}


		HttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();

		try {
			Gson gson = new Gson();
			HttpUriRequest post = RequestBuilder.put().setUri(url)
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.setEntity(new StringEntity(gson.toJson(broadcast))).build();

			HttpResponse response = client.execute(post, customContext != null ? customContext : httpContext);

			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception(content.toString());
			}
			log.info("result string: " + content.toString());
			Result tmp = gson.fromJson(content.toString(), Result.class);

			return tmp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public static Result deleteBroadcast(String ip, String id, String appName, boolean callDirecly) throws Exception {

		String url = null;

		if (callDirecly) {
			url = "http://" + ip + ":5080/" + appName + "/rest/v2/broadcasts/" + id;
		}
		else {
			url = getRootURL(ip)+"/request?_path=/"+appName+"/rest/v2/broadcasts/"+id;
		}


		CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();

		HttpUriRequest post = RequestBuilder.delete().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json").build();

		CloseableHttpResponse response = client.execute(post, httpContext);

		String content = EntityUtils.toString(response.getEntity());

		if (response.getStatusLine().getStatusCode() != 200) {
			throw new Exception(content.toString());
		}
		log.info("result string: " + content.toString());
		Result result2 = gson.fromJson(content.toString(), Result.class);
		return result2;

	}


	public static Broadcast getBroadcast(String ip, String app, String streamId) {
		return getBroadcast(ip, app, streamId, false);
	}


	public static Broadcast getBroadcast(String ip, String app, String streamId, boolean directCall) {

		long startTime = System.currentTimeMillis();

		String url = null;

		if (directCall) {
			url = "http://" + ip + ":5080/" + app + "/rest/v2/broadcasts/" + streamId;
		}
		else {
			url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/"+streamId;
		}

		log.info("url: {}", url);
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		Broadcast broadcast = null;

		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, httpContext);
			String content = EntityUtils.toString(response.getEntity());

			log.info("response: {}", content);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);

			}
			if (response.getStatusLine().getStatusCode() == 404) {
				return null;
			}

			broadcast = gson.fromJson(content, Broadcast.class);

		} catch (Exception e) {
			e.printStackTrace();
		}

		long timeDiff = System.currentTimeMillis() - startTime;

		log.info("getBroadcast executed in {} ms", timeDiff);

		return broadcast;
	}	


	public static BasicStreamInfo[] getStreamInfoList(String ip, String app, String streamId) {
		String url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/"+streamId+"/stream-info";


		log.info("url:"+url);
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		BasicStreamInfo[] streamIfoList = null;

		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, httpContext);
			String content = EntityUtils.toString(response.getEntity());

			log.info(content);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}
			if (response.getStatusLine().getStatusCode() == 404) {
				return null;
			}

			streamIfoList = gson.fromJson(content, BasicStreamInfo[].class);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return streamIfoList;
	}

	public static AppSettings getSettings(String ip, String app) {
		return getSettings(ip, app, null);
	}

	public static Result removeEndpoint(String ip, String app, HttpContext customContext, String streamId, String endpointId) throws Exception
	{
		String url = getRootURL(ip) +"/request?_path=/"+ app + "/rest/v2/broadcasts/"+ streamId +"/rtmp-endpoint?endpointServiceId="+endpointId;
		CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();

		HttpUriRequest post = RequestBuilder.delete().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		HttpResponse response = client.execute(post, customContext != null ? customContext : httpContext);

		String content = EntityUtils.toString(response.getEntity());
		if (response.getStatusLine().getStatusCode() != 200) {
			log.info("Response not 200: \n" + response.getStatusLine()+content);
		}
		log.info("result string: " + content);
		return  gson.fromJson(content, Result.class);
	}

	public static Result addEndpoint(String ip, String app, HttpContext customContext, String streamId, io.antmedia.datastore.db.types.Endpoint endpoint) throws Exception {

		String url = getRootURL(ip) +"/request?_path=/"+ app + "/rest/v2/broadcasts/"+ streamId +"/rtmp-endpoint";

		CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();

		HttpUriRequest post = RequestBuilder.post().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.setEntity(new StringEntity(gson.toJson(endpoint))).build();

		HttpResponse response = client.execute(post, customContext != null ? customContext : httpContext);

		String content = EntityUtils.toString(response.getEntity());
		if (response.getStatusLine().getStatusCode() != 200) {
			log.info("Response not 200: \n" + response.getStatusLine()+content);
		}
		log.info("result string: " + content);
		return  gson.fromJson(content, Result.class);
	}

	public static AppSettings getSettings(String ip, String app, HttpContext context) {
		
		
		String url = getRootURL(ip)+"/applications/settings/"+app;


		log.info("url:"+url);
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		AppSettings settings = null;
		long startTime = System.currentTimeMillis();
		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, context != null ? context : httpContext);
			String content = EntityUtils.toString(response.getEntity());


			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine().toString());
			}

			//write settings for debugging purposes
			log.info("Settings -> " + content);

			settings = gson.fromJson(content, AppSettings.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long now = System.currentTimeMillis() - startTime;
		
		log.info("getSettings executed in {} ms", now);

		return settings;
	}



	public static boolean changeSettings(String ip, String app, AppSettings settings) {
		return changeSettings(ip, app, settings, null);
	}

	public static boolean changeSettings(String ip, String app, AppSettings settings, HttpContext context) {
		boolean result = false;
		String json = gson.toJson(settings);

		try {
			HttpUriRequest post = RequestBuilder.post().setUri(getRootURL(ip)+"/applications/settings/"+app)
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.setEntity(new StringEntity(json))
					.build();

			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(post, context != null ? context : httpContext);

			String content = EntityUtils.toString(response.getEntity());


			log.info(response.getStatusLine() + " " + content);


			result = (response.getStatusLine().getStatusCode() == 200);

		} catch (Exception e) {
			e.printStackTrace();
		}


		return result;
	}

	public static int getBroadcastSize(String ip, String app) {
		String url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/active-live-stream-count";


		log.info("url:"+url);
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		//make it -1 to understand if there is any exception or not below
		SimpleStat stats = new SimpleStat(-1);

		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, httpContext);
			String content = EntityUtils.toString(response.getEntity());

			log.info(content);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			stats = gson.fromJson(content, SimpleStat.class);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return (int) stats.number;
	}

	public static void getSystemResourcesInfo(String ip) {
		String url = getRootURL(ip)+"/system-resources";


		log.info("url:"+url);
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, httpContext);
			String content = EntityUtils.toString(response.getEntity());

			log.info(content);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean getShutdownStatus(String ip) {
		String url = getRootURL(ip)+"/shutdown-proper-status?appNames=LiveApp";

		boolean result = false;

		log.info("url:"+url);
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, httpContext);
			String content = EntityUtils.toString(response.getEntity());

			log.info(content);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			result = gson.fromJson(content, Result.class).isSuccess();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static boolean setStreamRecording(String ip, String app, String streamId, boolean enabled, String type, HttpContext context) {
		boolean result = false;
		String url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/"+streamId
				+"/recording/"+enabled+"?recordType="+type;
		try {
			HttpUriRequest put = RequestBuilder.put().setUri(url)
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.build();

			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(put, context != null ? context : httpContext);

			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			result = (response.getStatusLine().getStatusCode() == 200);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static boolean changeBroadcastProperties(String ip, String app, Broadcast broadcast, HttpContext context) {
		boolean result = false;
		String url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/"+broadcast.getStreamId();
		try {
			HttpUriRequest put = RequestBuilder.put().setUri(url)
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.setEntity(new StringEntity(gson.toJson(broadcast)))
					.build();

			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(put, context != null ? context : httpContext);

			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			result = (response.getStatusLine().getStatusCode() == 200);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static ServerSettings getServerSettings(String ip, HttpContext context) {
		String url = getRootURL(ip)+"/server-settings";


		log.info("url:"+url);
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		ServerSettings settings = null;

		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, context != null ? context : httpContext);
			String content = EntityUtils.toString(response.getEntity());


			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			settings = gson.fromJson(content, ServerSettings.class);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return settings;
	}

	public static boolean changeServerSettings(String ip, ServerSettings settings, HttpContext customHttpContext) {
		boolean result = false;

		try {

			HttpUriRequest post = RequestBuilder.post().setUri(getRootURL(ip)+"/server-settings")
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.setEntity(new StringEntity(gson.toJson(settings)))
					.build();

			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(post, customHttpContext != null ? customHttpContext : httpContext);

			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			result = (response.getStatusLine().getStatusCode() == 200);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static boolean createApplication(String ip, String appName, File warFile, HttpContext customHttpContext) {
		boolean result = false;

		try {

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();      
			builder.setMode(HttpMultipartMode.STRICT);
			builder.addBinaryBody("file", warFile, ContentType.DEFAULT_BINARY, warFile.getName());


			HttpEntity entity = builder.build();

			HttpPut put = new HttpPut(getRootURL(ip)+ "/applications/" + appName);
			put.setEntity(entity);

			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(put, customHttpContext != null ? customHttpContext : httpContext);

			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			log.info("result string: " + content);
			return  gson.fromJson(content, Result.class).isSuccess();


		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}


	public static boolean createApplication(String ip, String appName, HttpContext customHttpContext) {
		boolean result = false;

		try {

			HttpUriRequest post = RequestBuilder.post().setUri(getRootURL(ip)+"/applications/"+appName)
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.build();


			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(post, customHttpContext != null ? customHttpContext : httpContext);

			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			result = (response.getStatusLine().getStatusCode() == 200);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	public static boolean deleteApplication(String ip, String appName, HttpContext customHttpContext) {
		boolean result = false;

		try {

			HttpUriRequest delete = RequestBuilder.delete().setUri(getRootURL(ip)+"/applications/"+appName)
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.build();

			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(delete, customHttpContext != null ? customHttpContext : httpContext);

			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			result = (response.getStatusLine().getStatusCode() == 200);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	public static List<ApplicationInfo> getApplications(String ip, HttpContext customHttpContext) {
		try {

			HttpUriRequest get = RequestBuilder.get().setUri(getRootURL(ip)+"/applications-info")
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.build();

			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, customHttpContext != null ? customHttpContext : httpContext);

			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			Type listType = new TypeToken<List<ApplicationInfo>>() {}.getType();
			log.info("content:"+content);

			return gson.fromJson(content, listType);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public static ConferenceRoom getRoomInfo(String ip, String app, HttpContext context, String roomId) {
		String url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/conference-rooms/"+roomId;


		log.info("url:"+url);
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		ConferenceRoom room = null;

		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, context != null ? context : httpContext);
			String content = EntityUtils.toString(response.getEntity());


			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}

			room = gson.fromJson(content, ConferenceRoom.class);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return room;

	}

	public static Result addSubscriber(String ip, String app, String subscriberId, String secret, String streamId, String type, boolean directCall, HttpContext context) {

		//curl -X POST -H "Accept: Application/json" -H "Content-Type: application/json" http://localhost:5080/WebRTCAppEE/rest/v2/broadcasts/stream1/subscribers -d 
		//'{"subscriberId":"publisherA", "b32Secret":"mysecret", "type":"publish"}'

		String url = null;
		try {
			if (directCall) {
				url = "http://" + ip + ":5080/" + app + "/rest/v2/broadcasts/" + streamId  +"/subscribers";
			}
			else {
				url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/"+streamId  +"/subscribers";
			}

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("subscriberId", subscriberId);
			jsonObject.put("b32Secret", secret);
			jsonObject.put("type", type);


			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();

			HttpUriRequest post = RequestBuilder.post().setUri(url)
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.setEntity(new StringEntity(jsonObject.toJSONString()))
					.build();

			HttpResponse response = client.execute(post, context != null ? context : httpContext);

			String result = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception(result.toString());
			}
			log.info("result string: " + result.toString());
			Result tmp = gson.fromJson(result.toString(), Result.class);

			return tmp;

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Result(false);


	}

	///{id}/subscribers/list/{offset}/{size

	public static List<ConnectionEvent> getConnectionEvents(String ip, String app, String streamId, boolean directCall, HttpContext context) {


		String url = null;

		if (directCall) {
			url = "http://" + ip + ":5080/" + app + "/rest/v2/broadcasts/" + streamId + "/connection-events//0/50";
		}
		else {
			url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/"+streamId + "/connection-events/0/50";
		}

		log.info("url:"+url);
		HttpUriRequest get = RequestBuilder.get().setUri(url)
				.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.build();

		try {
			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
			CloseableHttpResponse response = client.execute(get, context != null ? context : httpContext);
			String content = EntityUtils.toString(response.getEntity());

			log.info(content);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info(response.getStatusLine()+content);
			}
			if (response.getStatusLine().getStatusCode() == 404) {
				return null;
			}

			Type listType = new TypeToken<List<ConnectionEvent>>() {}.getType();

			return gson.fromJson(content, listType);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Result blockSubscriber(String ip, String app, String subscriberId, String streamId,  String blockedType, int seconds, boolean directCall, HttpContext context){
		try {
			//@Path("/{id}/subscribers/{sid}/block/{seconds}/{type}")

			String url;

			if (directCall) {
				url = "http://" + ip + ":5080/" + app + "/rest/v2/broadcasts/" + streamId + "/subscribers/" + subscriberId +"/block/" + seconds + "/" + blockedType;
			}
			else {
				url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/"+streamId + "/subscribers/" + subscriberId +"/block/" + seconds + "/" + blockedType;;
			}


			CloseableHttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();

			HttpUriRequest post = RequestBuilder.put().setUri(url)
					.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.build();

			HttpResponse response = client.execute(post , context != null ? context : httpContext);

			String content = EntityUtils.toString(response.getEntity());

			if (response.getStatusLine().getStatusCode() != 200) {
				log.info("status code:"+response.getStatusLine().getStatusCode());
				throw new Exception(content.toString());
			}

			log.info("blockSubscriber response string: " + content);

			return gson.fromJson(content, Result.class);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Result(false);

	}


	public static Result addStreamSource(Broadcast broadcast, String ip, String app, boolean autoStart) throws Exception {

		Result result = new Result(false);

		String url = getRootURL(ip)+"/request?_path=/"+app+"/rest/v2/broadcasts/create?autoStart="+autoStart;


		HttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
		Gson gson = new Gson();

		HttpUriRequest post = RequestBuilder.post().setUri(url).setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.setEntity(new StringEntity(gson.toJson(broadcast))).build();

		HttpResponse response = client.execute(post,  httpContext);

		String content = EntityUtils.toString(response.getEntity());
		if (response.getStatusLine().getStatusCode() != 200) {
			log.info("Response not 200: \n" + response.getStatusLine()+content);
			return result;
		}
		log.info("result string: " + content);
		result.setSuccess(true);
		return result;

	}


	public static Result sendData(String ip, String streamId, String app, String data) throws Exception {
		Result result = new Result(false);

		String url = "http://" + ip + ":5080/" + app + "/rest/v2/broadcasts/"+streamId+"/data";


		HttpClient client = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy())
				.setDefaultCookieStore(cookieStore)
				.build();

		HttpUriRequest post = RequestBuilder.post().setUri(url).setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.setEntity(new StringEntity(data)).build();

		HttpResponse response = client.execute(post,  httpContext);

		String content = EntityUtils.toString(response.getEntity());
		if (response.getStatusLine().getStatusCode() != 200) {
			log.info("Response not 200: \n" + response.getStatusLine()+content);
			return result;
		}
		log.info("result string: " + content);
		result.setSuccess(true);
		return result;
	}



}