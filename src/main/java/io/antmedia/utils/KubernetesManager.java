package io.antmedia.utils;



import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.awaitility.Awaitility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KubernetesManager 
{
	
	private static Logger log = LoggerFactory.getLogger(KubernetesManager.class);

	
	public boolean createDeployment(File deploymentFile, String deploymentName) {
		try {
		//we should use sudo command for compatibility
		DockerManager.execCommand("sudo kubectl create -f " + deploymentFile.getAbsolutePath());
				
		DockerManager.getTmpExec().waitFor();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(()-> {
			Process execCommand = DockerManager.execCommand("sudo kubectl get deployment " + deploymentName);
						
			InputStream inputStream = execCommand.getInputStream();
			StringBuilder buffer = new StringBuilder();
			byte[] data = new byte[1024];
			int length = 0;
			while ((length = inputStream.read(data, 0, data.length)) > 0) {
				buffer.append(new String(data, 0, length));
			}
			String result = buffer.toString();
			log.info("Process result is " + result);
			return result.contains("1/1");
			
		});
		
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
			
	}

	public void deleteDeployment(String deployment) {
		DockerManager.execCommand("sudo kubectl delete deployment  " + deployment);
	}
	
}
