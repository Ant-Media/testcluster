package io.antmedia.utils;

public class Constants {

	public static final String LIVEAPP = "LiveApp";
	public static final String WEBRTCAPPEE = "WebRTCAppEE";
	
	public static final int SERVER_START_TIME = 15000;
	public static final int MONGO_START_TIME = 2000;
	public static final int NETWORK_START_TIME = 1000;
	public static final int HAPROXY_START_TIME = 2000;

	public static final String NO_MESSAGE = "no message";
}
