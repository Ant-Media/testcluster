package io.antmedia.utils;


import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;

public class FileManager {
	public static  final String WIDTH = "width";
	public static  final String HEIGHT = "height";
	public static  final String BITRATE = "bitrate";
	public static  final String CODEC = "codec_name";
	public static  final String DURATION = "duration";
	public static  final String FPS = "r_frame_rate";

	public static  final char VIDEO = 'v';
	public static  final char AUDIO = 'a';


	public static FileInfo getFileInfo(String path) {
		FileInfo info = new FileInfo();
		info.isExist = new File(path).exists();
		info.path = path;
		info.videoCodec = getProperty(path, VIDEO, CODEC);
		info.videoWidth = getProperty(path, VIDEO, WIDTH);
		info.videoHeight = getProperty(path, VIDEO, HEIGHT);
		info.videoFps = getProperty(path, VIDEO, FPS);
		info.videoBitrate = getProperty(path, VIDEO, BITRATE);
		info.videoDuration = run("ffprobe "+path+" -show_streams -select_streams v 2>&1 | sed -n 's/TAG:DURATION=//p'").trim();
		info.audioDuration = run("ffprobe "+path+" -show_streams -select_streams a 2>&1 | sed -n 's/TAG:DURATION=//p'").trim();
		info.videoPacketsCount = Integer.parseInt(run("ffprobe "+path+" -show_packets 2>&1 | grep codec_type=video | wc -l").trim());
		info.audioPacketsCount = Integer.parseInt(run("ffprobe "+path+" -show_packets 2>&1 | grep codec_type=audio | wc -l").trim());

		info.audioCodec = getProperty(path, AUDIO, CODEC);
		info.audioBitrate = getProperty(path, AUDIO, BITRATE);
		info.videoDurationMS = toMsDuration(info.videoDuration);
		info.audioDurationMS = toMsDuration(info.audioDuration);
		return info;
	}

	private static long toMsDuration(String strDuration) {
		if(strDuration.isEmpty()) {
			return 0;
		}
		String[] tokens = strDuration.split(":");
		long hours = Long.parseLong(tokens[0])*3600*1000;
		long minutes = Long.parseLong(tokens[1])*60*1000;
		long seconds = (long) (Double.parseDouble(tokens[2])*1000);

		return hours+minutes+seconds;
	}

	public static String getProperty(String path, char type, String property) {
		String command = "ffprobe -v error -select_streams "+type+":0 -show_entries stream="+property+" -of csv=s=x:p=0 "+path;
		return run(command).trim();
	}

	public static String run(String command) {
		ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", command);
		try {
			return IOUtils.toString(pb.start().getInputStream(), StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
}
