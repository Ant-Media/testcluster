package io.antmedia.utils;

import java.util.List;

import com.mongodb.client.MongoClients;

import dev.morphia.Datastore;
import dev.morphia.Morphia;
import dev.morphia.query.filters.Filter;
import dev.morphia.query.filters.Filters;
import io.antmedia.Config;
import io.antmedia.cluster.ClusterNode;
import io.antmedia.datastore.db.types.StreamInfo;

public class DBManager {
	private Morphia morphia;
	private Datastore clusterDatastore;
	private Datastore webRTCDatastore;
	private com.mongodb.client.MongoClient mongoClient;

	public static DBManager instance = new DBManager();

	private DBManager() {
			
		mongoClient = MongoClients.create("mongodb://"+Config.MONGO_1_IP);
		clusterDatastore = Morphia.createDatastore(mongoClient, "clusterdb");
		clusterDatastore.ensureIndexes();
		
		webRTCDatastore = Morphia.createDatastore(mongoClient, "webrtcappee");
		webRTCDatastore.ensureIndexes();
	}

	public static List<ClusterNode> getClusterNodes() {
		long activeIntervalValue = System.currentTimeMillis() - (6 * ClusterNode.NODE_UPDATE_PERIOD);
		Filter filter = Filters.gte("lastUpdateTime", activeIntervalValue);
		List<ClusterNode> nodes = instance.clusterDatastore.find(ClusterNode.class).filter(filter).iterator().toList();
		
		return nodes;
	}

	public List<StreamInfo> getStreamInfos() {
		List<StreamInfo> infos = instance.webRTCDatastore.find(StreamInfo.class).iterator().toList();
		return infos;
	}
}
