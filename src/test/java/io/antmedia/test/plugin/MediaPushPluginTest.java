package io.antmedia.test.plugin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.http.ParseException;
import org.awaitility.Awaitility;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import io.antmedia.AntMediaApplicationAdapter;
import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.plugin.MediaPushPlugin;
import io.antmedia.rest.model.Result;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.DockerManager;
import io.antmedia.utils.RestManager;

public class MediaPushPluginTest extends SetUpTest {


	private ChromeDriver chromeDriver;


	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();

	}

	@After
	public void after() {
		chromeDriver.quit();
	}

	/**
	 *
	 * This is the test for this bug -> https://github.com/ant-media/Ant-Media-Server/issues/6946
	 * @throws ParseException
	 * @throws IOException
	 * @throws org.json.simple.parser.ParseException
	 */
	@Test
	public void testMediaPushStop() throws ParseException, IOException, org.json.simple.parser.ParseException {
		
		
		DockerManager.setImageId(DockerManager.ANTMEDIA_IMAGE_MEDIA_PUSH);

		//reset cluster ready
		resetClusterReady();

		//it will seetup cluster again with the image id
		setUpCluster();

		assertTrue(RestManager.login(Config.ORIGIN_1_IP));


		//start the media push plugin in origin 1
		
		Result result = RestManager.startMediaPush(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, "https://google.com", null, false);
		assertTrue(result.isSuccess());
		String streamId = result.getDataId();
		

		//check that media push plugin is started in origin 1 and it's streaming in server side
		Awaitility.await().atMost(20, TimeUnit.SECONDS).until(() -> {
			Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
			return broadcast != null && broadcast.getStatus().equals(AntMediaApplicationAdapter.BROADCAST_STATUS_BROADCASTING);
		});
		
		
		Broadcast broadcastLocal = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
		String metadata = broadcastLocal.getMetaData();
		JSONParser parser = new JSONParser();
		JSONObject jsObject = (JSONObject) parser.parse(metadata);
		
		String driverIp = (String) jsObject.get(MediaPushPlugin.ORIGIN_IP_OF_DRIVER);
		assertEquals(Config.ORIGIN_1_IP, driverIp);

		//send stop request to media push plugin in origin 2
		assertTrue(RestManager.login(Config.EDGE_1_IP));

		//it should be forwarded to the origin 1 and media push plugin should stop in origin 1
		result = RestManager.stopMediaPush(Config.EDGE_1_IP, Constants.WEBRTCAPPEE, streamId, null, false);
		assertTrue(result.isSuccess());
		
		//check that media push plugin is stopped in origin 1
		Awaitility.await().atMost(20, TimeUnit.SECONDS).until(() -> {
			Broadcast broadcast = RestManager.getBroadcast(Config.EDGE_1_IP, Constants.WEBRTCAPPEE, streamId);
			return broadcast == null;
		});
		
		
		resetClusterReady();
		DockerManager.setImageId(DockerManager.ANTMEDIA_IMAGE);
		
		DockerManager.clearCluster();

	}


}
