package io.antmedia.test.utils;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;
import org.awaitility.Awaitility;
import org.awaitility.core.ConditionTimeoutException;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.Config;
import io.antmedia.test.standalone.WebMRecordingTest;

public class FrontEndUtils {

	protected static Logger logger = LoggerFactory.getLogger(FrontEndUtils.class);


	public static List<String> chromeOptionList = Arrays.asList("--disable-extensions",
			"--disable-gpu", 
			"--headless=new",
			"--no-sandbox",
			"--disable-dev-shm-usage",
			"--log-level=1",
			"--remote-allow-origins=*",
			"--use-fake-ui-for-media-stream",
			"--use-fake-device-for-media-stream",
			//we use 80 port to access origin to publish video
			"--unsafely-treat-insecure-origin-as-secure=http://" + Config.NGINX_IP + ",http://" + Config.NGINX_IP + ":5080" + ",http://" + Config.STANDALONE_IP + ":5080" 
			+ ",http://" + Config.STANDALONE_IP	+ ",http://" + Config.ORIGIN_1_IP+":5080"	 + ",http://" + Config.ORIGIN_2_IP	 + ":5080"	
			+ ",http://" + Config.NONE_PRIVATE_ORIGIN_1_IP + ":5080" + ",http://" +  Config.NONE_PRIVATE_ORIGIN_2_IP + ":5080"
			+ ",http://" + Config.EDGE_1_IP + ":5080"
			+ ",http://" + Config.EDGE_2_IP + ":5080"
			+ ",http://" + Config.STANDALONE_2_IP + ":5080"
			);
	public static ChromeDriver getChromeDriver() {
		ChromeOptions chrome_options = new ChromeOptions();

		chrome_options.addArguments(chromeOptionList);
		for (String option : chromeOptionList) {
			logger.info("chrome option: " + option);
		}
		//
		//log.info("");
		LoggingPreferences logPrefs = new LoggingPreferences();
		//To get console log
		logPrefs.enable(LogType.BROWSER, Level.ALL);
		chrome_options.setCapability( "goog:loggingPrefs", logPrefs );

		ChromeDriver driver = new ChromeDriver(chrome_options);
		driver.manage().timeouts().pageLoadTimeout( Duration.ofSeconds(10));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));


		//
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));

		return driver;
	}


	public static void publishStream(ChromeDriver driver, String hostname, String appName, String streamId) {


		boolean quitDriver = false;
		ChromeDriver localDriver;
		if (driver == null) {
			quitDriver = true;
			localDriver = getChromeDriver();
		}
		else {
			localDriver = driver;
		}
		localDriver.get("http://" + hostname  +":5080/" + appName + "/index.html?id=" + streamId);

		//Check we landed on the page


		localDriver.switchTo().frame(0);
		WebDriverWait wait = new WebDriverWait(localDriver, Duration.ofSeconds(15));

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='start_publish_button']")));


		localDriver.findElement(By.xpath("//*[@id='start_publish_button']")).click();

		try {
			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				String iceConnectionState = (String) localDriver.executeScript("return window.webRTCAdaptor.iceConnectionState('"+streamId+"')");
				logger.info("ice connection state: {} for publishing stream: {}", iceConnectionState, streamId);
				return "connected".equals(iceConnectionState);
			});
		}
		catch (ConditionTimeoutException e) {
			LogEntries entry = driver.manage().logs().get(LogType.BROWSER);
			List<LogEntry> logs= entry.getAll();
			// Print one by one
			for(LogEntry log: logs)
			{
				logger.info("Browser log -> " + log);
			}
			e.printStackTrace();
			fail(e.getMessage());
		}

		if (quitDriver) {
			driver.quit();
		}
	}

	public static void playStreamWithBrowser(ChromeDriver driver, String hostname, String appName, String streamId, String playOrder, String query, boolean embeddedPlayer) 
	{

		boolean quitDriver = false;
		ChromeDriver localDriver;
		if (driver == null) {
			quitDriver = true;
			localDriver = getChromeDriver();
		}
		else {
			localDriver = driver;
		}

		//play.html or player.html matters
		//playerOrder does not matter in player.html because it's webrtc player
		localDriver.get("http://" + hostname +  ":5080/" + appName + "/play"+ (embeddedPlayer ? "" : "er") +".html?id=" + streamId + "&playOrder="+playOrder+"&" + (query != null ? query : ""));

		if (!embeddedPlayer) {
			localDriver.findElement(By.id("start_play_button")).click();

		}
		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			return checkVideoElementState(localDriver);
		});

		if (embeddedPlayer) {
			String playType = (String) localDriver.executeScript("return window.webPlayer.currentPlayType");

			assertTrue(playOrder.contains(playType));
		}

		if (quitDriver) {
			localDriver.quit();
		}		
	}


	public static boolean checkVideoElementState(ChromeDriver driver) {
		try {
			String readyState = driver.findElement(By.tagName("video")).getDomProperty("readyState");
			//this.driver.findElement(By.xpath("//*[@id='video-player']")).
			logger.info("player ready state -> {}", readyState);

			return readyState != null && readyState.equals("4");
		}
		catch (StaleElementReferenceException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void joinMultiTrackConference(ChromeDriver driver, String hostname, String appName, String roomId, String streamId, String query) 
	{
		driver.get("http://" + hostname  +":5080/" + appName + "/multitrack-conference.html?roomId=" + roomId + "&streamId=" + streamId + ( query != null ? "&"+query : ""));

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));


		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='join_publish_button']")));

		driver.findElement(By.xpath("//*[@id='join_publish_button']")).click();

		if (streamId != null && (!StringUtils.isBlank(query) && !query.equals("playOnly=true"))) {
			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				String iceConnectionState = (String) driver.executeScript("return window.webRTCAdaptor.iceConnectionState('"+streamId+"')");
				logger.info("ice connection state: {} for publishing stream:{} ", iceConnectionState, streamId);
				return "connected".equals(iceConnectionState);
			});
		}
		else {
			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				String iceConnectionState = (String) driver.executeScript("return window.webRTCAdaptor.iceConnectionState('"+roomId+"')");
				logger.info("ice connection state: {} for roomId:{}", iceConnectionState, roomId);
				return "connected".equals(iceConnectionState);
			});
		}

	}

	public static void sendConferenceMessage(ChromeDriver driver, String windowHandle, String message){
		driver.switchTo().window(windowHandle);

		WebElement dataTextbox = driver.findElement(By.id("dataTextbox"));
		if(!dataTextbox.isDisplayed()){
			WebElement optionsButton = driver.findElement(By.id("options"));
			optionsButton.click();

		}

		WebElement sendButton = driver.findElement(By.id("send"));

		dataTextbox.sendKeys(message);
		sendButton.click();
	}

	public static boolean isConferenceMessageFound(ChromeDriver driver, String windowHandle, String message, boolean once) {
		driver.switchTo().window(windowHandle);

		WebElement messageBox = driver.findElement(By.id("all-messages"));
		if(!messageBox.isDisplayed()){
			WebElement optionsButton = driver.findElement(By.id("options"));
			optionsButton.click();

		}
		String value = messageBox.getText();
		
		System.out.println(windowHandle+" messages:"+value+" expected:"+message);

		int messageCount = countOccurrences(value, message);

		if(once){
			return messageCount == 1;
		}

		return messageCount > 0;

	}


	private static int countOccurrences(String text, String substring) {
		int count = 0;
		int index = 0;

		while (index != -1) {
			index = text.indexOf(substring, index);
			if (index != -1) {
				count++;
				index += substring.length();
			}
		}

		return count;
	}

}
