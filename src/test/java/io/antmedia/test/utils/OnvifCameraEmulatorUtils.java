package io.antmedia.test.utils;

import java.io.IOException;

public class OnvifCameraEmulatorUtils {

    public static void startCameraEmulator() {
        stopCameraEmulator();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ProcessBuilder pb = new ProcessBuilder("/usr/local/onvif/runme.sh");
        try {
            pb.start();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void stopCameraEmulator() {
        // close emulator in order to simulate cut-off
        String[] argsStop = new String[] { "/bin/bash", "-c",
                "kill -9 $(ps aux | grep 'onvifser' | awk '{print $2}')" };
        String[] argsStop2 = new String[] { "/bin/bash", "-c",
                "kill -9 $(ps aux | grep 'rtspserve' | awk '{print $2}')" };
        try {
            Process procStop = new ProcessBuilder(argsStop).start();
            Process procStop2 = new ProcessBuilder(argsStop2).start();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}