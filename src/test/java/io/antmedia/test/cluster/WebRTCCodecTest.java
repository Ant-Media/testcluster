package io.antmedia.test.cluster;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.EncoderSettings;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.datastore.db.types.StreamInfo;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.DBManager;
import io.antmedia.utils.FileInfo;
import io.antmedia.utils.FileManager;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;
import io.antmedia.webrtc.VideoCodec;

public class WebRTCCodecTest extends SetUpTest{

	private static Logger log = LoggerFactory.getLogger(WebRTCCodecTest.class);

	private static String baseStreamId = "TestWebrtcStream_CodecTest_";
	public String streamId;
	private ChromeDriver chromeDriver;

	@BeforeClass
	public static void setUp() {
		setUpCluster();
	}
	
	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();

	}
	
	@After
	public void after() {
		chromeDriver.quit();

	}
	
	
	@Test
	public void testH264WhenOnlyH264CodecEnabledSFU() {
		streamId = baseStreamId + "testH264WhenOnlyH264CodecEnabledSFU";
		testCodec(true, false, "src/test/resources/test.mp4", "h264", true);
	}
	
	@Test
	public void testH264WhenOnlyH264CodecEnabled() {
		streamId = baseStreamId + "testH264WhenOnlyH264CodecEnabled";
		testCodec(true, false, "src/test/resources/test.mp4", "h264", false);
	}
	
	@Test
	public void testH264WhenH264AndVP8CodecEnabled() {
		streamId = baseStreamId + "testH264WhenH264AndVP8CodecEnabled";
		testCodec(true, true, "src/test/resources/test.mp4", "h264", false);
	}

	@Test
	public void testVP8WhenOnlyVP8CodecEnabledSFU() {
		streamId = baseStreamId + "testVP8WhenOnlyVP8CodecEnabledSFU";
		testCodec(false, true, "src/test/resources/test.webm", "VP8", true);
	}
	
	@Test
	public void testVP8WhenOnlyVP8CodecEnabled() {
		streamId = baseStreamId + "testVP8WhenOnlyVP8CodecEnabled";
		testCodec(false, true, "src/test/resources/test.webm", "VP8", false);
	}
	
	@Test
	public void testVP8WhenH264AndVP8CodecEnabled() {
		streamId = baseStreamId + "testVP8WhenH264AndVP8CodecEnabled";
		testCodec(true, true, "src/test/resources/test.webm", "VP8", false);
	}
	
	//TODO: Add H265 support to WebRTC stack
	/*
	@Test
	public void testH265() {
		
		log.info("conf:"+FFmpegUtilities.getBuildConfiguration());
		
		if(!FFmpegUtilities.getBuildConfiguration().contains("--enable-gpl")) {
			return;
		}
		streamId = baseStreamId + "";
		AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
		settings.setH264Enabled(false);
		settings.setVp8Enabled(false);
		settings.setH265Enabled(true);
		
		ArrayList<EncoderSettings> encoderSettings = new ArrayList<>();
		encoderSettings.add(new EncoderSettings(240,500000,32000,true));
		settings.setEncoderSettings(encoderSettings);
		
		assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));
		
		
		CookieStore cookieStore = new BasicCookieStore();
		HttpContext customContext = new BasicHttpContext();
		customContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
		RestManager.login(Config.ORIGIN_1_IP, customContext);
		
		//wait to see the origin has the settings
		Awaitility.await().atMost(11, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS)
		.until(() -> {
			AppSettings appSettings = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, customContext);
			boolean sizeEqual = false;
			if (encoderSettings.size() == 0) 
			{
				sizeEqual = appSettings.getEncoderSettings() == null || appSettings.getEncoderSettings().isEmpty();
			}
			else {
				sizeEqual =  appSettings.getEncoderSettings() != null && appSettings.getEncoderSettings().size() == encoderSettings.size();
			}
			
			boolean encoderSettingSet = true;
			if (sizeEqual && encoderSettings.size() > 0) {
				//there can be just one bitrate
				boolean videoBitrateEqual = false;
				boolean audioBitrateEqual = false;
				boolean heightEqual = false;
		
				if (appSettings.getEncoderSettings() != null && appSettings.getEncoderSettings().size() > 0) 
				{
					videoBitrateEqual = appSettings.getEncoderSettings().get(0).getVideoBitrate() == encoderSettings.get(0).getVideoBitrate();
					audioBitrateEqual = appSettings.getEncoderSettings().get(0).getAudioBitrate() == encoderSettings.get(0).getAudioBitrate();
					heightEqual = appSettings.getEncoderSettings().get(0).getHeight() == encoderSettings.get(0).getHeight();
				}
				encoderSettingSet = videoBitrateEqual && audioBitrateEqual && heightEqual;
			}
			
			boolean codecsSet = (!appSettings.isH264Enabled()) && (!appSettings.isVp8Enabled())
					&& appSettings.isH265Enabled();
			
			
			return sizeEqual && encoderSettingSet && codecsSet;
		});
		
		
		AppSettings settings2 = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
		assertEquals(false, settings2.isH264Enabled());
		assertEquals(false, settings2.isVp8Enabled());
		assertEquals(true, settings2.isH265Enabled());
		
		
		RTMPPublisher rtmpPublisher = new RTMPPublisher(Config.ORIGIN_1_IP, "src/test/resources/Test_600kbps.mp4", Constants.WEBRTCAPPEE, streamId, testFolder, 1);

		rtmpPublisher.start();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			log.info("Getting stream from Load Balancer");
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId);
			if (broadcast != null) {
				return broadcast.isPublicStream(); 
			}
			return false;
		});
		
		
		List<StreamInfo> streamInfos = DBManager.instance.getStreamInfos();
		
		assertTrue(streamInfos.get(0).getVideoCodec() == VideoCodec.H265);
		
		String playerCodec = "h265";
		String player1OutFile = testFolder+"/output1.h265";
		String player2OutFile = testFolder+"/output2.h265";
		
		assertEquals(0, RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount());
		
		WebRTCPeer play1 = new WebRTCPeer(Config.EDGE_1_IP, "player", streamId, player1OutFile, playerCodec, 1, false);
		play1.start();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		WebRTCPeer play2 = new WebRTCPeer(Config.EDGE_2_IP, "player", streamId, player2OutFile, playerCodec, 1, false);
		play2.start();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 2);
	
		Utils.mustSleep(20000);
		
		play2.stop();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		play1.stop();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 0);
		
		rtmpPublisher.stop();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId); 
			return  (broadcast == null);
		});
		
		FileInfo info1 = FileManager.getFileInfo(player1OutFile);
		FileInfo info2 = FileManager.getFileInfo(player2OutFile);
		
		assertTrue(info1.isExist);
		assertEquals("hevc", info1.videoCodec);
		log.info(player1OutFile + " info -> " + info1.toString());
		assertTrue(info2.isExist);
		assertEquals("hevc", info2.videoCodec);
		log.info(player2OutFile + " info -> " + info2.toString());
		
	}
	*/
	
	public void testCodec(boolean h264Enabled, boolean VP8Enabled, String file, String codec, boolean isSFU)  {
		
		AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
		settings.setH264Enabled(h264Enabled);
		settings.setVp8Enabled(VP8Enabled);
		settings.setH265Enabled(false);
		
		ArrayList<EncoderSettings> encoderSettings = new ArrayList<>();
		
		if((h264Enabled && VP8Enabled) || !isSFU) {
			encoderSettings.add(new EncoderSettings(240,500000,32000,true));
		}
		
		int encoderSettingsSize = encoderSettings.size();
		settings.setEncoderSettings(encoderSettings);
		
		assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));
		
		CookieStore cookieStore = new BasicCookieStore();
		HttpContext customContext = new BasicHttpContext();
		customContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
		RestManager.login(Config.ORIGIN_1_IP, customContext);
		
		
		//wait to see the origin has the settings
		Awaitility.await().atMost(11, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS)
		.until(() -> {
			AppSettings appSettings = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, customContext);
			
			boolean sizeEqual = false;
			if (encoderSettingsSize == 0) 
			{
				sizeEqual = appSettings.getEncoderSettings() == null || appSettings.getEncoderSettings().isEmpty();
			}
			else {
				sizeEqual =  appSettings.getEncoderSettings() != null && appSettings.getEncoderSettings().size() == encoderSettings.size();
			}
			
			boolean encoderSettingSet = true;
			if (sizeEqual && encoderSettingsSize > 0) {
				//there can be just one bitrate
				boolean videoBitrateEqual = false;
				boolean audioBitrateEqual = false;
				boolean heightEqual = false;
		
				if (appSettings.getEncoderSettings() != null && appSettings.getEncoderSettings().size() > 0) 
				{
					videoBitrateEqual = appSettings.getEncoderSettings().get(0).getVideoBitrate() == encoderSettings.get(0).getVideoBitrate();
					audioBitrateEqual = appSettings.getEncoderSettings().get(0).getAudioBitrate() == encoderSettings.get(0).getAudioBitrate();
					heightEqual = appSettings.getEncoderSettings().get(0).getHeight() == encoderSettings.get(0).getHeight();
				}
				encoderSettingSet = videoBitrateEqual && audioBitrateEqual && heightEqual;
			}
			
			boolean codecsSet = (h264Enabled == appSettings.isH264Enabled()) && (VP8Enabled == appSettings.isVp8Enabled())
					&& !appSettings.isH265Enabled();
			
			
			return sizeEqual && encoderSettingSet && codecsSet;
		});
		
		AppSettings settings2 = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
		assertEquals(h264Enabled, settings2.isH264Enabled());
		assertEquals(VP8Enabled, settings2.isVp8Enabled());
		
		FrontEndUtils.publishStream(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
		String publisherHandle = chromeDriver.getWindowHandle();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId); 
			if (broadcast != null) {
				return broadcast.isPublicStream();
			}
			return false;
		});
		
		List<StreamInfo> streamInfos = DBManager.instance.getStreamInfos();
		//log.info("Size of stream infos -> " + streamInfos.size());
		
		for (StreamInfo streamInfo : streamInfos) 
		{
			log.info("Stream info streamId: " + streamInfo.getStreamId() + " height: " + streamInfo.getVideoHeight() 
				+ " width: " + streamInfo.getVideoWidth() + " videoBitrate: " + streamInfo.getVideoBitrate()
				+ " audioBitrate: " + streamInfo.getAudioBitrate() + " videoRTimebase: " + streamInfo.getVideoRTimebase()
				+ " audioRTimebase: " + streamInfo.getAudioRTimebase() + " host: " + streamInfo.getHost() 
				+ " videoEnabled: " + streamInfo.isVideoEnabled() 
				+ " audioEnabled: " + streamInfo.isAudioEnabled() + " dataChannelEnabled: " + streamInfo.isDataChannelEnabled()
				+ " videoCodec: " + streamInfo.getVideoCodec() + " nodGroup: " + streamInfo.getNodeGroup() 
				+ " isGlobalHost: " + streamInfo.isGlobalHost());
		}
		
		
		String player1Codec = "h264";
		String player2Codec = "h264";
		String player1OutFile = "";
		String player2OutFile = "";

		if(VP8Enabled && h264Enabled) {
			assertEquals(2, streamInfos.size());
			assertTrue(streamInfos.get(0).getVideoCodec() == VideoCodec.H264 || 
					streamInfos.get(1).getVideoCodec() == VideoCodec.H264);
			assertTrue(streamInfos.get(0).getVideoCodec() == VideoCodec.VP8 || 
					streamInfos.get(1).getVideoCodec() == VideoCodec.VP8);
			
			player1Codec = "h264";
			player2Codec = "VP8";
			player1OutFile = testFolder+"/output.h264";
			player2OutFile = testFolder+"/output.ivf";
		}
		else if(h264Enabled) {
			assertEquals(1, streamInfos.size());
			assertEquals(VideoCodec.H264, streamInfos.get(0).getVideoCodec());

			player1Codec = "h264";
			player2Codec = "h264";
		}
		else if(VP8Enabled) {
			assertEquals(1, streamInfos.size());
			assertEquals(VideoCodec.VP8, streamInfos.get(0).getVideoCodec());
			
			player1Codec = "VP8";
			player2Codec = "VP8";
		}
		
		assertEquals(0, RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount());
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player1WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player2WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_2_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 2);
	
		Utils.mustSleep(5000);
		
		chromeDriver.close();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		chromeDriver.switchTo().window(player1WindowHandle);
		chromeDriver.close();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 0);
		
		chromeDriver.switchTo().window(publisherHandle);
		chromeDriver.close();
		
		
		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId); 
			return  (broadcast == null);
		});
		
	}

}
