package io.antmedia.test.cluster;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Duration;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.rest.model.Result;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.tools.RTMPPublisher;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.github.bonigarcia.wdm.WebDriverManager;

public class TokenTest extends SetUpTest {

	private static String appName = Constants.WEBRTCAPPEE;
	
	private static Logger log = LoggerFactory.getLogger(TokenTest.class);


	private ChromeDriver chromeDriver;



	@BeforeClass
	public static void setUpClass() {
		setUpCluster();

		assertTrue(RestManager.login(Config.ORIGIN_1_IP));
		

		log.info("loged in to origin");
	}
	
	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();
	}

	@After
	public void after() {
		chromeDriver.quit();
	}
	
	public String generateJWTToken(String jwtSecretKey, String streamId, String type, Date expireDateType) {
		String jwtTokenId = null;
		try {
			Algorithm algorithm = Algorithm.HMAC256(jwtSecretKey);

			jwtTokenId = JWT.create().
					withClaim("streamId", streamId).
					withClaim("type", type).
					withExpiresAt(expireDateType).
					sign(algorithm);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return jwtTokenId;
		
	}

	@Test
	public void testPlaybackWithJWTToken() {
		//enable JWT token for playing

		//get settings from origin
		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
		{
			AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, appName);
			return settings != null;
		});
		
		AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, appName);
		settings.setPlayJwtControlEnabled(true);
		settings.setDashMuxingEnabled(true);
		String jwtSecretKey = RandomStringUtils.randomAlphanumeric(32);
		settings.setJwtStreamSecretKey(jwtSecretKey);
		
		boolean changeSettings = RestManager.changeSettings(Config.ORIGIN_1_IP, appName, settings);
		assertTrue(changeSettings);

		//publish live stream to the origin
		
		String streamId = "stream" + (int)(Math.random()*100000);
		RTMPPublisher rtmpPublisher = new RTMPPublisher(Config.ORIGIN_1_IP, "src/test/resources/Test_600kbps.mp4", Constants.WEBRTCAPPEE, streamId, testFolder, 1);
		rtmpPublisher.start();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast2 = RestManager.getBroadcast(Config.ORIGIN_1_IP, appName, streamId);
			if (broadcast2 != null) {
				return broadcast2.isPublicStream(); 
			}
			return false;
		});

		//play the live stream on the edge with HLS
	
		String token = generateJWTToken(jwtSecretKey, streamId, "play", new Date(System.currentTimeMillis() + 600000));
		
		//it should fail because there is no token
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				!AdaptiveStreamingTest.testFile("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + ".m3u8"));

		//it should pss becase there is a token
		

		try {
			
			String url = "http://"+Config.EDGE_1_IP+":5080/"+appName+ "/" ;
			chromeDriver.get(url + "play.html?id=" + streamId + "&playOrder=webrtc&token="+token);
			
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {

				String readyState = chromeDriver.findElement(By.tagName("video")).getDomProperty("readyState");
				//this.driver.findElement(By.xpath("//*[@id='video-player']")).
				log.info("player ready state -> " + readyState);

				return readyState != null && readyState.equals("4");
			});
			
			//check component visibility
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS) .until(() -> {
				
				return this.chromeDriver.findElement(By.id("video_container")).getCssValue("display").equals("block") &&
						this.chromeDriver.findElement(By.id("video_info")).getCssValue("display").equals("none");
			});
			
			String playType = (String) this.chromeDriver.executeScript("return window.webPlayer.currentPlayType");
			assertEquals("webrtc", playType);
			
			
			//check hls is not playing without token
			this.chromeDriver.get(url + "play.html?id=" + streamId + "&playOrder=hls");
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS) .until(() -> {
				
				return this.chromeDriver.findElement(By.id("video_container")).getCssValue("display").equals("none") &&
						this.chromeDriver.findElement(By.id("video_info")).getCssValue("display").equals("block");
			});
			
			
			
			//play with hls
			this.chromeDriver.get(url + "play.html?id=" + streamId + "&token="+token+"&playOrder=hls");
			
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {

				String readyState = this.chromeDriver.findElement(By.tagName("video")).getDomProperty("readyState");
				log.info("player ready state -> " + readyState);

				return readyState != null && readyState.equals("4");
			});
			
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS) .until(() -> {
				
				return this.chromeDriver.findElement(By.id("video_container")).getCssValue("display").equals("block") &&
						this.chromeDriver.findElement(By.id("video_info")).getCssValue("display").equals("none");
			});
			
			playType = (String) this.chromeDriver.executeScript("return window.webPlayer.currentPlayType");
			assertEquals("hls", playType);
			
			
			//Reinit driver because token is kept in cache for the session
			this.chromeDriver.quit();
			
			this.chromeDriver =  FrontEndUtils.getChromeDriver();
			this.chromeDriver.manage().timeouts().pageLoadTimeout( Duration.ofSeconds(10));
			this.chromeDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			//
			WebDriverWait wait = new WebDriverWait(chromeDriver, Duration.ofSeconds(15));
			
			//check with dash is not playing without token
			this.chromeDriver.get(url + "play.html?id=" + streamId + "&playOrder=dash");
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS) .until(() -> {
				
				return this.chromeDriver.findElement(By.id("video_container")).getCssValue("display").equals("none") &&
						this.chromeDriver.findElement(By.id("video_info")).getCssValue("display").equals("block");
			});
			
			this.chromeDriver.get(url + "play.html?id=" + streamId + "&token="+token+"&playOrder=dash");
			
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {

				String readyState = this.chromeDriver.findElement(By.tagName("video")).getDomProperty("readyState");
				log.info("player ready state -> " + readyState);

				return readyState != null && readyState.equals("4");
			});
			
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS) .until(() -> {
				
				return this.chromeDriver.findElement(By.id("video_container")).getCssValue("display").equals("block") &&
						this.chromeDriver.findElement(By.id("video_info")).getCssValue("display").equals("none");
			});
			
			playType = (String) this.chromeDriver.executeScript("return window.webPlayer.currentPlayType");
			assertEquals("dash", playType);
			
						
			settings.setPlayJwtControlEnabled(false);
			
			changeSettings = RestManager.changeSettings(Config.ORIGIN_1_IP, appName, settings);
			assertTrue(changeSettings);
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		} 

		
		//stop rtmp publisher
		rtmpPublisher.stop();

	}

	@Test
	public void testPlaybackWithOneTimeToken() {
		//TODO: Implement this test
	}

	@Test
	public void testPlaybackWithTOTP() {
		//TODO: Implement this test	
	}

}
