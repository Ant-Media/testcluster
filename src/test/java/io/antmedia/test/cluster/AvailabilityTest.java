package io.antmedia.test.cluster;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.datastore.db.types.ConnectionEvent;
import io.antmedia.datastore.db.types.Subscriber;
import io.antmedia.datastore.db.types.SubscriberStats;
import io.antmedia.muxer.IAntMediaStreamHandler;
import io.antmedia.rest.model.Result;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.tools.RTMPPublisher;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.github.bonigarcia.wdm.WebDriverManager;

public class AvailabilityTest extends SetUpTest {

	private static String appName = Constants.WEBRTCAPPEE;

	private static Logger log = LoggerFactory.getLogger(AvailabilityTest.class);



	@BeforeClass
	public static void setUpClass() {
		setUpCluster();

		assertTrue(RestManager.login(Config.NGINX_IP));
		WebDriverManager.chromedriver().setup();
		log.info("logged in to origin");

		//get settings from origin
		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
		{
			AppSettings settings = RestManager.getSettings(Config.NGINX_IP, appName);
			return settings != null;
		});

		AppSettings settings = RestManager.getSettings(Config.NGINX_IP, appName);
		settings.setWriteSubscriberEventsToDatastore(true);
		
		boolean changeSettings = RestManager.changeSettings(Config.NGINX_IP, appName, settings);
		assertTrue(changeSettings);
		
		//it needs max 5 seconds to let app settings  to be distributed
		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			return true;
		});
        
	}
	
	@AfterClass
	public static void afterClass() {
		
		//make sure that it's logged in to nginx because the backend instance may be removed
		assertTrue(RestManager.login(Config.NGINX_IP));
		
		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
		{
			AppSettings settings = RestManager.getSettings(Config.NGINX_IP, appName);

			if (settings == null) {
				return false;
			}
			settings.setWriteSubscriberEventsToDatastore(false);
			
			boolean changeSettings = RestManager.changeSettings(Config.NGINX_IP, appName, settings);
			return changeSettings;
			
		});
		
	}

	private ChromeDriver driver;
	private WebDriverWait wait;
	private String firstOriginAddress;
	private String secondOriginAddress;

	private static Logger logger = LoggerFactory.getLogger(AvailabilityTest.class);

	@Before
	public void before() {
		if (driver == null) {
			ChromeOptions chrome_options = new ChromeOptions();

			chrome_options.addArguments(FrontEndUtils.chromeOptionList);
			for (String option : FrontEndUtils.chromeOptionList) {
				log.info("chrome option: " + option);
			}
			//
			//log.info("");
			LoggingPreferences logPrefs = new LoggingPreferences();
			//To get console log
			logPrefs.enable(LogType.BROWSER, Level.ALL);
			chrome_options.setCapability( "goog:loggingPrefs", logPrefs );

			this.driver = new ChromeDriver(chrome_options);
			this.driver.manage().timeouts().pageLoadTimeout( Duration.ofSeconds(10));
			this.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));


			//
			wait = new WebDriverWait(driver, Duration.ofSeconds(15));
		}
		assertTrue(RestManager.login(Config.NGINX_IP));
	}

	@After
	public void after() {
		if(this.driver != null) {
			this.driver.quit();
			this.driver = null;
		}
		//let logged in to nginx beccause the container may be killed
		assertTrue(RestManager.login(Config.NGINX_IP));
		
	}

	public boolean checkAlert()
	{
		try
		{
			String alert = this.driver.switchTo().alert().getText();
			log.info(alert);
			if(alert.equalsIgnoreCase("highResourceUsage")){
				log.info("High resource usage blocks testing");
				this.driver.switchTo().alert().accept();
				return false;
			}
			else if(alert.equalsIgnoreCase("no_stream_exist")){
				log.info("No stream available check the publishing");
				this.driver.switchTo().alert().accept();
				return false;
			}
			else{
				log.info("Unexpected pop-up alert on browser ="  + alert);
				this.driver.switchTo().alert().dismiss();
				return false;
			}
		}
		catch (NoAlertPresentException e)
		{
			return true;
		}
	}


	@Test
	public void testPublishAvailability() {


		try {
			//one origin is already running, setup another origin
			if (!dm.isContainerRunningImmediate( Config.ORIGIN_2_NAME)) {
				assertTrue(dm.createAntMediaContainer(Config.ORIGIN_2_IP, Config.ORIGIN_2_NAME, Config.MONGO_1_IP, true));
			}
			String streamId = RandomStringUtils.randomAlphanumeric(12);

			String subscriberId = "subpublish" + RandomStringUtils.randomAlphanumeric(12);

			String url = "http://"+Config.NGINX_IP+"/"+appName+ "/" + "index.html?id=" + streamId;

			log.info("url to open :" + url);
			this.driver.get(url);

			this.driver.switchTo().frame(0);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='start_publish_button']")));
			assertTrue(checkAlert());


			this.driver.findElement(By.xpath("//*[@id='start_publish_button']")).click();

			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				String iceConnectionState = (String) this.driver.executeScript("return window.webRTCAdaptor.iceConnectionState('"+streamId+"')");
				log.info("Checking ice connection state:" + iceConnectionState + " it should be connected");
				return "connected".equals(iceConnectionState) || "completed".equals(iceConnectionState);
			});

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> 
			{ 
				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);

				if (broadcast != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus())) {
					log.info("broadcast object is not null and origin address: " + broadcast.getOriginAdress());
					firstOriginAddress = broadcast.getOriginAdress();
					return broadcast.getOriginAdress().equals(Config.ORIGIN_2_IP) ||
							broadcast.getOriginAdress().equals(Config.ORIGIN_1_IP);
				}
				return false;
			});

			String containerName = null;

			if (firstOriginAddress.equals(Config.ORIGIN_1_IP)) {
				containerName = Config.ORIGIN_1_NAME;
			}
			else if (firstOriginAddress.equals(Config.ORIGIN_2_IP)) {
				containerName = Config.ORIGIN_2_NAME;
			}
			assertNotNull(containerName);

			//kill the edge
			assertTrue(dm.stopAndRemoveContainer(containerName));


			String finalContainerName = containerName;

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				log.info("Checking if " + finalContainerName + " is killed");

				boolean isRunning = dm.isContainerRunningImmediate(finalContainerName);
				if (!isRunning) {
					log.info(finalContainerName + " is killed");
				}
				return !isRunning;
			});

			assertTrue(RestManager.login(Config.NGINX_IP));

			//it should be connected again in 5 seconds and origin addres shoud be different
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
			{
				String iceConnectionState = (String) this.driver.executeScript("return window.webRTCAdaptor.iceConnectionState('"+streamId+"')");
				logger.info("ice connection state:{}" , iceConnectionState);
				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);

				if ("connected".equals(iceConnectionState) && 
						broadcast != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus())) 
				{
					log.info("broadcast object is not null and origin address: " + broadcast.getOriginAdress());
					secondOriginAddress = broadcast.getOriginAdress();
					return !secondOriginAddress.equals(firstOriginAddress) && 
							(StringUtils.equals(secondOriginAddress, Config.ORIGIN_1_IP) || 
									StringUtils.equals(secondOriginAddress, Config.ORIGIN_2_IP));
				}

				return false;
			});



			//these should not be equal
			assertNotEquals(secondOriginAddress, firstOriginAddress);

			//start the  container again if it's origin 1 because it's used in other tests
			if (firstOriginAddress.equals(Config.ORIGIN_1_IP)) {
				containerName = Config.ORIGIN_1_NAME;
				assertTrue(dm.createAntMediaContainer(Config.ORIGIN_1_IP, Config.ORIGIN_1_NAME, Config.MONGO_1_IP, true));
			}

		}
		catch (Exception e) {
			LogEntries entry = driver.manage().logs().get(LogType.BROWSER);
			List<LogEntry> logs= entry.getAll();
			// Print one by one
			for(LogEntry logEntry: logs)
			{
				log.info(logEntry.toString());
			}
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	private Boolean checkVideoElementState() {
		try {
			String readyState = this.driver.findElement(By.tagName("video")).getDomProperty("readyState");
			//this.driver.findElement(By.xpath("//*[@id='video-player']")).
			logger.info("player ready state -> {}", readyState);

			return readyState != null && readyState.equals("4");
		}
		catch (StaleElementReferenceException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 5080 is the group for edge. Recently, we introduce support for target=origin and target=edge
	 * So if publish through 5080, it should still go to origin group because JS SDK adds the query automatically	
	 */
	@Test
	public void testPublishPlayWithTargetQueryInJS_SDK() 
	{
		try {
			String createdContainerName = null;
			if (!dm.isContainerRunningImmediate(Config.ORIGIN_1_NAME)) 
			{
				assertTrue(dm.createAntMediaContainer(Config.ORIGIN_1_IP, Config.ORIGIN_1_NAME, Config.MONGO_1_IP, true));
				createdContainerName = Config.ORIGIN_1_NAME;
			}
			else if (!dm.isContainerRunningImmediate(Config.ORIGIN_2_NAME)) 
			{
				assertTrue(dm.createAntMediaContainer(Config.ORIGIN_2_IP, Config.ORIGIN_2_NAME, Config.MONGO_1_IP, true));
				createdContainerName = Config.ORIGIN_2_NAME;
			}



			String streamId = RandomStringUtils.randomAlphanumeric(12);

			List<ConnectionEvent> subscriberStats = RestManager.getConnectionEvents(Config.NGINX_IP, appName, streamId, false, null);
			assertEquals(0, subscriberStats.size());

			//5080 is the group for edge. Recently, we introduce support for target=origin and target=edge
			//So if publish through 5080, it should still go to origin group because JS SDK adds the query automatically

			String url = "http://"+Config.NGINX_IP+":5080/"+appName+ "/" + "index.html?id=" + streamId;

			log.info("url to open :" + url);
			this.driver.get(url);

			String windowHandle = this.driver.getWindowHandle();

			this.driver.switchTo().frame(0);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='start_publish_button']")));
			assertTrue(checkAlert());

			this.driver.findElement(By.xpath("//*[@id='start_publish_button']")).click();

			//let it try 30 seconds because server may return high resource usage or other errors
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				String iceConnectionState = (String) this.driver.executeScript("return window.webRTCAdaptor.iceConnectionState('"+streamId+"')");
				log.info("Checking ice connection state:" + iceConnectionState + " it should be connected");
				return "connected".equals(iceConnectionState) || "completed".equals(iceConnectionState);
			});

			RestManager.login(Config.NGINX_IP);
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> 
			{ 
				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);

				if (broadcast != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus())) {
					log.info("broadcast object is not null and origin address: " + broadcast.getOriginAdress());
					firstOriginAddress = broadcast.getOriginAdress();
					return 
							broadcast.getOriginAdress().equals(Config.ORIGIN_2_IP) ||
							broadcast.getOriginAdress().equals(Config.ORIGIN_1_IP);
				}
				return false;
			});


			this.driver.switchTo().newWindow(WindowType.WINDOW);

			//80 is the group for origin. Recently, we introduce support for target=origin and target=edge
			//So if play through 80, it should still go to edge group because JS SDK adds the query automatically

			String subscriberId = "subplay" + RandomStringUtils.randomAlphanumeric(12);
			//no need to have secret because we just use for getting info
			Result result = RestManager.addSubscriber(Config.NGINX_IP, appName, subscriberId, "", streamId, Subscriber.PLAY_TYPE, false, null);
			assertTrue(result.isSuccess());

			String urlToPlay = "http://"+Config.NGINX_IP+":80/"+appName+ "/" + "play.html?id=" + streamId + "&playOrder=webrtc&subscriberId="+subscriberId;

			log.info("url to open :" + urlToPlay);
			this.driver.get(urlToPlay);

			//check that it's playing
			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {

				log.info("checking player is started");
				return checkVideoElementState();
			});

			//check component visibility
			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS) .until(() -> {

				return this.driver.findElement(By.id("video_container")).getCssValue("display").equals("block") &&
						this.driver.findElement(By.id("video_info")).getCssValue("display").equals("none");
			});


			List<ConnectionEvent> connectionEvents  = RestManager.getConnectionEvents(Config.NGINX_IP, appName, streamId, false, null);


			String edgeIp = null;
			for (ConnectionEvent connectionEvent : connectionEvents) {
				if (connectionEvent.getEventType().equals(ConnectionEvent.CONNECTED_EVENT)) {
					edgeIp = connectionEvent.getInstanceIP();
				}
			}
			assertNotNull(edgeIp);

			String containerName = null;
			if (Config.EDGE_1_IP.equals(edgeIp)) {
				containerName = Config.EDGE_1_NAME;
			}
			else if (Config.EDGE_2_IP.equals(edgeIp)) {
				containerName = Config.EDGE_2_NAME;
			}
			else if (Config.EDGE_3_IP.equals(edgeIp)) {
				containerName = Config.EDGE_3_NAME;
			}
			else if (Config.EDGE_4_IP.equals(edgeIp)) {
				containerName = Config.EDGE_4_NAME;
			}
			assertNotNull(containerName);
		}
		catch (Exception e) {
			LogEntries entry = driver.manage().logs().get(LogType.BROWSER);
			List<LogEntry> logs= entry.getAll();
			// Print one by one
			for(LogEntry logEntry: logs)
			{
				log.info(logEntry.toString());
			}
			e.printStackTrace();
			fail(e.getMessage());
		}

	}

	@Test
	public void testPlayAvailability() {

		//create at least two edges
		//two edges are already available


		try {
			String streamId = RandomStringUtils.randomAlphanumeric(12);

			//publish stream
			RTMPPublisher rtmpPublisher = new RTMPPublisher(Config.NGINX_IP, "src/test/resources/Test_600kbps.mp4", Constants.WEBRTCAPPEE, streamId, testFolder, 1);
			rtmpPublisher.start();

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> 
			{ 
				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);

				if (broadcast != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus())) {
					log.info("broadcast object is not null and origin address: " + broadcast.getOriginAdress());
					return broadcast.getOriginAdress().equals(Config.ORIGIN_2_IP) ||
							broadcast.getOriginAdress().equals(Config.ORIGIN_1_IP);
				}
				return false;
			});

			String subscriberId = "subplay" + RandomStringUtils.randomAlphanumeric(12);

			//no need to have secret because we just use for getting info
			Result result = RestManager.addSubscriber(Config.NGINX_IP, appName, subscriberId, "", streamId, Subscriber.PLAY_TYPE, false, null);
			assertTrue(result.isSuccess());

			//play the live stream to the edge 1 with web browser through LB
			String url = "http://"+Config.NGINX_IP+":5080/"+appName+ "/" ;
			this.driver.get(url + "play.html?id=" + streamId + "&playOrder=webrtc&subscriberId="+subscriberId);

			//check that it's playing
			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {

				log.info("checking player is started");
				return checkVideoElementState();
			});

			//check component visibility
			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS) .until(() -> {

				return this.driver.findElement(By.id("video_container")).getCssValue("display").equals("block") &&
						this.driver.findElement(By.id("video_info")).getCssValue("display").equals("none");
			});

			List<ConnectionEvent> connectionEvents = RestManager.getConnectionEvents(Config.NGINX_IP, appName, streamId, false, null);


			String edgeIp = null;
			for (ConnectionEvent connectionEvent : connectionEvents) {
				if (connectionEvent.getEventType().equals(ConnectionEvent.CONNECTED_EVENT)) {
					edgeIp = connectionEvent.getInstanceIP();
				}
			}
			assertNotNull(edgeIp);

			String containerName = null;

			if (Config.EDGE_1_IP.equals(edgeIp)) {
				containerName = Config.EDGE_1_NAME;
			}
			else if (Config.EDGE_2_IP.equals(edgeIp)) {
				containerName = Config.EDGE_2_NAME;
			}
			else if (Config.EDGE_3_IP.equals(edgeIp)) {
				containerName = Config.EDGE_3_NAME;
			}
			else if (Config.EDGE_4_IP.equals(edgeIp)) {
				containerName = Config.EDGE_4_NAME;
			}

			assertNotNull(containerName);

			//kill the edge
			assertTrue(dm.stopAndRemoveContainer(containerName));

			String finalContainerName = containerName;

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				logger.info("Checking if {} is killed", finalContainerName);

				boolean isRunning = dm.isContainerRunningImmediate(finalContainerName);
				if (!isRunning) {
					logger.info("{} is killed", finalContainerName);
				}
				return !isRunning;
			});


			//check that it's playing again because it should switch the next - web browser respond late in the environment this is why we use 45 secs
			//Typically it takes a few seconds
			Awaitility.await().atMost(45, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {

				return checkVideoElementState();
			});

			//check component visibility
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS) .until(() -> {

				return this.driver.findElement(By.id("video_container")).getCssValue("display").equals("block") &&
						this.driver.findElement(By.id("video_info")).getCssValue("display").equals("none");
			});

			RestManager.login(Config.NGINX_IP);
			connectionEvents = RestManager.getConnectionEvents(Config.NGINX_IP, appName, streamId, false, null);


			edgeIp = null;
			long timestamp = 0;
			for (ConnectionEvent connectionEvent : connectionEvents) {
				if (connectionEvent.getEventType().equals(ConnectionEvent.CONNECTED_EVENT) && connectionEvent.getTimestamp() > timestamp) {
					edgeIp = connectionEvent.getInstanceIP();
					timestamp = connectionEvent.getTimestamp();
				}
			}

			assertNotNull(edgeIp);

			String containerName2 = null;

			if (Config.EDGE_1_IP.equals(edgeIp)) {
				containerName2 = Config.EDGE_1_NAME;
			}
			else if (Config.EDGE_2_IP.equals(edgeIp)) {
				containerName2 = Config.EDGE_2_NAME;
			}
			else if (Config.EDGE_3_IP.equals(edgeIp)) {
				containerName2 = Config.EDGE_3_NAME;
			}
			else if (Config.EDGE_4_IP.equals(edgeIp)) {
				containerName2 = Config.EDGE_4_NAME;
			}

			assertNotNull(containerName2);
			assertNotEquals(containerName, containerName2);	

			rtmpPublisher.stop();

		}
		catch (Exception e) {

			LogEntries entry = driver.manage().logs().get(LogType.BROWSER);
			List<LogEntry> logs= entry.getAll();
			// Print one by one
			for(LogEntry log: logs)
			{
				logger.info(log.toString());
			}
			e.printStackTrace();
			fail(e.getMessage());
		}

	}



}