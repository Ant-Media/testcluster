package io.antmedia.test.cluster;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;

public class StressTest extends SetUpTest {

	
	private static Logger log = LoggerFactory.getLogger(StressTest.class);

	/**
	 * This is just a simple test - We actually make the stress test in automation under load directory in enterprise project
	 */
	private static final String STREAM_ID_BASE = "streamTest";
	private static Process testPublishers;

	@BeforeClass
	public static void setUpClass() {
		setUpCluster();

		assertTrue(RestManager.login(Config.ORIGIN_1_IP));
		log.info("loged in to origin");
	}

	@AfterClass
	public static void tearDownClass() {
		log.info("Tear Down:");

		if (testPublishers != null) {
			testPublishers.destroyForcibly();
			testPublishers = null;
		}
	}
	
	private ChromeDriver chromeDriver;
	
	
	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();

	}
	
	@After
	public void after() {
		chromeDriver.quit();
	}
	


	@Test
	public void testStress() {

		//getting settings from origin makes sure that server is initialized
		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(5, TimeUnit.SECONDS).until(() -> 
		{
			AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
			return settings != null;
		});



		int BENCH_STREAM_SIZE = 3;

		Utils.mustSleep(2000);

		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> 0 == RestManager.getBroadcastSize(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE));

		//WebRTCPeer bench = new WebRTCPeer(Config.ORIGIN_IP, "publisher", "bench", "src/test/resources/test.mp4", "h264", BENCH_STREAM_SIZE, false);
		//bench.start();

		startTestPublishers(BENCH_STREAM_SIZE);

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcastSize(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE) == BENCH_STREAM_SIZE);


		//stop after some delay
		Utils.mustSleep(2000);

		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcastSize(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE) == BENCH_STREAM_SIZE);

		chromeDriver.quit();
		testPublishers = null;

		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcastSize(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE) == 0);
	}

	@Test
	public void testEdgeConnectionUnder() {

		//getting settings from origin makes sure that server is initialized
		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
		{
			AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
			return settings != null;
		});

		int BENCH_STREAM_SIZE = 3;

		startTestPublishers(BENCH_STREAM_SIZE);
		

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcastSize(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE) == BENCH_STREAM_SIZE);


		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player1WindowHandle = chromeDriver.getWindowHandle();
		//play the first strem
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE + "0", "webrtc", null, false);
		

		Utils.mustSleep(3000);

		RestManager.getSystemResourcesInfo(Config.ORIGIN_1_IP);

		chromeDriver.quit();

		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcastSize(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE) == 0);
	}


	public List<String> startTestPublishers(int count) {

		List<String> windowHandleList = new ArrayList<>();
		for (int i = 0; i<count;i++) {
			String streamId = STREAM_ID_BASE + i;
			chromeDriver.switchTo().newWindow(WindowType.WINDOW);
			
			FrontEndUtils.publishStream(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
			windowHandleList.add(chromeDriver.getWindowHandle());
		}


		return windowHandleList;
	}
}