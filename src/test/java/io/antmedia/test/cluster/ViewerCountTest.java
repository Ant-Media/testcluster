package io.antmedia.test.cluster;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.EncoderSettings;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.tools.HLSPlayer;
import io.antmedia.tools.RTMPPublisher;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;

public class ViewerCountTest extends SetUpTest{

	private static Logger log = LoggerFactory.getLogger(ViewerCountTest.class);

	
	private static RTMPPublisher rtmpPublisher;
	private String streamId = "test";
	
	@BeforeClass
	public static void setUp() {
		setUpCluster();
		
	}
	
	@Before
	public void before() {
		AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
		settings.setH264Enabled(true);
		settings.setVp8Enabled(false);
		settings.setH265Enabled(false);

		ArrayList<EncoderSettings> encoderSettings = new ArrayList<>();
		settings.setEncoderSettings(encoderSettings);
		
		assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));
		
		CookieStore cookieStore = new BasicCookieStore();
		HttpContext customContext = new BasicHttpContext();
		customContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
		RestManager.login(Config.ORIGIN_1_IP, customContext);
		
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS)
		.until(() -> {
			AppSettings appSettings = RestManager.getSettings(Config.ORIGIN_1_IP,  Constants.WEBRTCAPPEE, customContext);
			return appSettings.isH264Enabled() == true && 
					appSettings.isVp8Enabled() == false && 
					appSettings.isH265Enabled() == false;
		});
		
		
		
		streamId = "ViewerCountTest" + (int) (Math.random()*1000);
		rtmpPublisher = new RTMPPublisher(Config.ORIGIN_1_IP, "src/test/resources/Test_600kbps.mp4", Constants.WEBRTCAPPEE, streamId, testFolder, 1);

		rtmpPublisher.start();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			log.info("Getting stream from Load Balancer");
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId);
			if (broadcast != null) {
				return broadcast.isPublicStream(); 
			}
			return false;
		});
		
		chromeDriver = FrontEndUtils.getChromeDriver();
	}
	
	@After
	public void after() {
		rtmpPublisher.stop();
		chromeDriver.quit();
	}
	
	private ChromeDriver chromeDriver;
	

	
	@Test
	public void testHLSViewerCount()  {
		try {
			
			//I think we should make more aggressive test with a controlled cookie way
			assertEquals(0, RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getHlsViewerCount());
			
			
			String url = "http://"+Config.ORIGIN_1_IP+":5080/"+Constants.WEBRTCAPPEE+"/streams/"+streamId+".m3u8";
			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(() -> {
				log.info("Checking m3u8 url");
				return HLSPlayer.checkURLExist(url);
			});
			
			StringBuffer response = HLSPlayer.getURL(url);
			log.info("Response: " + response);
			
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getHlsViewerCount() == 1);
			
			
			response = HLSPlayer.getURL(url);
			log.info("Response: " + response);
			
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getHlsViewerCount() == 2);
		
			
			Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getHlsViewerCount() == 0);
		}
		catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public void testWebRTCViewerCount()  {
		assertEquals(0, RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount());
		
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		String player1WindowHandle = chromeDriver.getWindowHandle();
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player2WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		
		

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			int webrtcViewerCount = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount();
			log.info("webrtc viewer count: " + webrtcViewerCount + " it should 2");
			return webrtcViewerCount == 2;
		});
	
		chromeDriver.close();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			int webrtcViewerCount = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount();
			log.info("webrtc viewer count: " + webrtcViewerCount);
			return webrtcViewerCount == 1;
		});
		
		
		chromeDriver.switchTo().window(player1WindowHandle);
		chromeDriver.close();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 0);
	}
}
