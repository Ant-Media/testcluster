package io.antmedia.test.cluster;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.Timer;

import org.awaitility.Awaitility;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.Config;
import io.antmedia.cluster.ClusterNode;
import io.antmedia.test.SetUpTest;
import io.antmedia.utils.DBManager;
import io.antmedia.utils.DockerManager;
import io.antmedia.utils.RestManager;
import net.bytebuddy.utility.RandomString;

public class ApplicationSyncTest extends SetUpTest{


	private static Logger log = LoggerFactory.getLogger(ApplicationSyncTest.class);

	@BeforeClass
	public static void setUp() {
		setUpCluster();
	}

	@Test
	public void testCustomAppCreation() throws InterruptedException
	{
		String file = getStreamAppWar(Config.ORIGIN_1_NAME);
		testAppCreation(file);
	}

	@Test
	public void testBigAppCreationWhileScaling() {
		String file = "/tmp/BigStreamApp.war";
		testAppCreation(file);
	}


	public void testAppCreation(String file)
	{	
		List<ClusterNode> clusterNodes = DBManager.getClusterNodes();

		log.info("Cluster nodes count:{}", clusterNodes.size());
		int i = 0;
		for (ClusterNode clusterNode : clusterNodes) {
			log.info("Node[{}]: {}", i, clusterNode.getIp());
			i++;
		}

		assertNotNull(file);
		File warFile = new File(file);
		assertTrue(warFile.exists());

		String appName = RandomString.make(11);
		RestManager.login(Config.ORIGIN_1_IP); 

		List<RestManager.ApplicationInfo> applications = RestManager.getApplications(Config.ORIGIN_1_IP, null);
		int applicationCount = applications.size();

		Timer timer = new Timer();

		// after 5 seconds scale the system by creating a new node
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (!dm.isContainerRunningImmediate( Config.ORIGIN_2_NAME)) {
					dm.createAntMediaContainer(Config.ORIGIN_2_IP, Config.ORIGIN_2_NAME, Config.MONGO_1_IP, true);
				}
				timer.cancel();
			}
		}, 5000);

		long t0 = System.currentTimeMillis();
		boolean appCreated = RestManager.createApplication(Config.ORIGIN_1_IP, appName, warFile, null);
		assertTrue(appCreated);
		long t1 = System.currentTimeMillis();

		log.info("Application {} installed in {}ms with war:{}", appName, (t1-t0), warFile);

		RestManager.login(Config.ORIGIN_2_IP); 
		Awaitility.await().atMost(150, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.ORIGIN_2_IP, null);
			return  applicationsInfo != null && applicationsInfo.size() == applicationCount + 1;
		});

		RestManager.login(Config.ORIGIN_1_IP); 
		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.ORIGIN_1_IP, null);
			return  applicationsInfo != null && applicationsInfo.size() == applicationCount + 1;
		});

		RestManager.login(Config.EDGE_1_IP); 
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.EDGE_1_IP, null);
			return  applicationsInfo != null && applicationsInfo.size() ==  applicationCount + 1;
		});

		RestManager.login(Config.EDGE_2_IP); 
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.EDGE_2_IP, null);
			return  applicationsInfo != null && applicationsInfo.size() ==  applicationCount + 1;
		});

		RestManager.login(Config.ORIGIN_1_IP); 

		//kill the origin 1 that first app is deployed
		dm.stopAndRemoveContainer(Config.ORIGIN_1_NAME);
		assertFalse(DockerManager.isContainerRunningImmediate(Config.ORIGIN_1_NAME));

		//start a new edge
		assertFalse(DockerManager.isContainerRunningImmediate(Config.EDGE_3_NAME));
		assertTrue(dm.createAntMediaContainer(Config.EDGE_3_IP, Config.EDGE_3_NAME, Config.MONGO_1_IP, true));

		//check that new edge have the app
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS)
		.until(() -> {
			return RestManager.login(Config.EDGE_3_IP);
		});
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.EDGE_3_IP, null);
			return  applicationsInfo != null && applicationsInfo.size() ==  applicationCount + 1;
		});


		log.info("Deleting application " + appName);
		assertTrue(RestManager.deleteApplication(Config.EDGE_3_IP, appName, null));

		assertTrue(dm.createAntMediaContainer(Config.ORIGIN_1_IP, Config.ORIGIN_1_NAME, Config.MONGO_1_IP, true));

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS)
		.until(() -> {
			return RestManager.login(Config.ORIGIN_1_IP);
		});

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollDelay(1, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.ORIGIN_1_IP, null);
			return applicationsInfo != null && applicationsInfo.size() ==  applicationCount;
		});

		RestManager.login(Config.EDGE_1_IP); 
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.EDGE_1_IP, null);

			return  applicationsInfo != null && applicationsInfo.size() == applicationCount;
		});

		RestManager.login(Config.EDGE_2_IP); 
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.EDGE_2_IP, null);

			return applicationsInfo != null && applicationsInfo.size() == applicationCount;
		});

		//stop and remove the edge because it's created in this test
		DockerManager.stopAndRemoveContainer(Config.EDGE_3_NAME);
		DockerManager.stopAndRemoveContainer(Config.ORIGIN_2_NAME);


		//The Instance size should be same as the begging
		//Wait for 40 secs because nodes are removed from the cluster after 30 seconds
		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			var clusterNodesTmp = DBManager.getClusterNodes();

			log.info("Cluster nodes count:{} and it should be {}", clusterNodesTmp.size(), clusterNodes.size());
			int t = 0;
			for (ClusterNode clusterNode : clusterNodesTmp) {
				log.info("Node[{}]: {}", t, clusterNode.getIp());
				t++;
			}
			
			return clusterNodesTmp.size() == clusterNodes.size();
		});



		log.info("size:"+applications.size());

	}

	@Test
	public void testAppCreationDestroy() 
	{
		String appName = "TestApp";
		RestManager.login(Config.ORIGIN_1_IP); 
		List<RestManager.ApplicationInfo> applications = RestManager.getApplications(Config.ORIGIN_1_IP, null);
		int applicationCount = applications.size();

		RestManager.createApplication(Config.ORIGIN_1_IP, appName, null);

		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.ORIGIN_1_IP, null);
			return  applicationsInfo != null && applicationsInfo.size() == applicationCount+1;
		});

		RestManager.login(Config.EDGE_1_IP); 
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.EDGE_1_IP, null);
			return  applicationsInfo != null && applicationsInfo.size() == applicationCount+1;
		});

		RestManager.login(Config.EDGE_2_IP); 
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.EDGE_2_IP, null);
			return  applicationsInfo != null && applicationsInfo.size() == applicationCount+1;
		});

		RestManager.login(Config.ORIGIN_1_IP); 
		log.info("Deleting application " + appName);
		assertTrue(RestManager.deleteApplication(Config.ORIGIN_1_IP, appName, null));

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollDelay(1, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.ORIGIN_1_IP, null);
			return applicationsInfo != null && applicationsInfo.size() == applicationCount;
		});

		RestManager.login(Config.EDGE_1_IP); 
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.EDGE_1_IP, null);

			return  applicationsInfo != null && applicationsInfo.size() == applicationCount;
		});

		RestManager.login(Config.EDGE_2_IP); 
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
			List<io.antmedia.utils.RestManager.ApplicationInfo> applicationsInfo = RestManager.getApplications(Config.EDGE_2_IP, null);

			return applicationsInfo != null && applicationsInfo.size() == applicationCount;
		});

		log.info("size:"+applications.size());

		//after this test, logs are not written to the file
	}
}
