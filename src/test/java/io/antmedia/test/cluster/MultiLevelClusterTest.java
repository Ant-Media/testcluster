package io.antmedia.test.cluster;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.muxer.IAntMediaStreamHandler;
import io.antmedia.settings.ServerSettings;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.DockerManager;
import io.antmedia.utils.RestManager;

public class MultiLevelClusterTest extends SetUpTest{

	private static Logger log = LoggerFactory.getLogger(MultiLevelClusterTest.class);

	private static final String GROUP_2_NAME = "grp2";
	private static final String GROUP_1_NAME = "grp1";
	private static String globalStreamId = "TestWebrtcStream_MultiLevelCluster";

	
	/**
	 * EDGE_1 and EDGE_2 are in grp1
	 * EDGE_3 and EDGE_4 are in grp2
	 */
	@BeforeClass
	public static void setUp() {
		setUpCluster();
		
		log.info("edge 1");
		RestManager.login(Config.EDGE_1_IP); 
		ServerSettings edge1Settings = RestManager.getServerSettings(Config.EDGE_1_IP, null);
		edge1Settings.setNodeGroup(GROUP_1_NAME);
		assertTrue(RestManager.changeServerSettings(Config.EDGE_1_IP, edge1Settings, null));
		assertEquals(GROUP_1_NAME, RestManager.getServerSettings(Config.EDGE_1_IP, null).getNodeGroup());
		
		
		
		log.info("edge 2");
		RestManager.login(Config.EDGE_2_IP); 
		ServerSettings edge2Settings = RestManager.getServerSettings(Config.EDGE_2_IP, null);
		edge2Settings.setNodeGroup(GROUP_1_NAME);
		assertTrue(RestManager.changeServerSettings(Config.EDGE_2_IP, edge2Settings, null));
		assertEquals(GROUP_1_NAME, RestManager.getServerSettings(Config.EDGE_2_IP, null).getNodeGroup());
		
		log.info("edge 3");
		if (!dm.isContainerRunningImmediate(Config.EDGE_3_NAME)) 
		{
			//if it2s not running it may take time to spin up
			assertTrue(dm.createAntMediaContainer(Config.EDGE_3_IP, Config.EDGE_3_NAME, Config.MONGO_1_IP, true));
			
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS)
			.until(() -> {
				return RestManager.login(Config.EDGE_3_IP);
			});
		}
		else {
			RestManager.login(Config.EDGE_3_IP); 
		}
		ServerSettings edge3Settings = RestManager.getServerSettings(Config.EDGE_3_IP, null);
		edge3Settings.setNodeGroup(GROUP_2_NAME);
		assertTrue(RestManager.changeServerSettings(Config.EDGE_3_IP, edge3Settings, null));
		assertEquals(GROUP_2_NAME, RestManager.getServerSettings(Config.EDGE_3_IP, null).getNodeGroup());
		
		log.info("edge 4");
		if (!dm.isContainerRunningImmediate(Config.EDGE_4_NAME)) 
		{
			//if it2s not running it may take time to spin up
			assertTrue(dm.createAntMediaContainer(Config.EDGE_4_IP, Config.EDGE_4_NAME, Config.MONGO_1_IP, true));
			
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS)
			.until(() -> {
				return RestManager.login(Config.EDGE_4_IP);
			});
		}
		else {
			RestManager.login(Config.EDGE_4_IP); 
		}
		
		ServerSettings edge4Settings = RestManager.getServerSettings(Config.EDGE_4_IP, null);
		edge4Settings.setNodeGroup(GROUP_2_NAME);
		assertTrue(RestManager.changeServerSettings(Config.EDGE_4_IP, edge4Settings, null));
		assertEquals(GROUP_2_NAME, RestManager.getServerSettings(Config.EDGE_4_IP, null).getNodeGroup());
		
		assertTrue(RestManager.login(Config.NGINX_IP));

	}
	
	@AfterClass
	public static void tearDown() {
		
		String[] ipAddressesOfInstances = new String[]{Config.EDGE_1_IP, Config.EDGE_2_IP, Config.EDGE_3_IP, Config.EDGE_4_IP};
		
		for (int i = 0; i < ipAddressesOfInstances.length; i++) {
			RestManager.login(ipAddressesOfInstances[i]); 
			ServerSettings edge1Settings = RestManager.getServerSettings(ipAddressesOfInstances[i], null);
			edge1Settings.setNodeGroup(ServerSettings.DEFAULT_NODE_GROUP);
			RestManager.changeServerSettings(ipAddressesOfInstances[i], edge1Settings, null);
			assertEquals(ServerSettings.DEFAULT_NODE_GROUP, RestManager.getServerSettings(ipAddressesOfInstances[i], null).getNodeGroup());
		}
		
		DockerManager.stopAndRemoveContainer(Config.EDGE_3_NAME);
		DockerManager.stopAndRemoveContainer(Config.EDGE_4_NAME);

		
		
	}


	private ChromeDriver chromeDriver;
	
	
	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();

	}
	
	@After
	public void after() {
		chromeDriver.quit();
	}
	

	@Test
	public void webRTCPublishPlay()  {
		String streamId = "deneme";
		
		FrontEndUtils.publishStream(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
		String publisherHandle = chromeDriver.getWindowHandle();
		
		
		
		assertTrue(RestManager.login(Config.ORIGIN_1_IP));
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId); 
			if (broadcast != null) {
				return IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus());
			}
			return false;
		});
		
		assertEquals(0, RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount());
		
		assertEquals(1, RestManager.getStreamInfoList(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).length);
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player1WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		assertEquals(2, RestManager.getStreamInfoList(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).length);
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player2WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_2_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 2);
		
		assertEquals(2, RestManager.getStreamInfoList(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).length);
		
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player3WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_3_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 3);
		
		assertEquals(3, RestManager.getStreamInfoList(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).length);
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player4WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_4_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 4);

		assertEquals(3, RestManager.getStreamInfoList(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).length);
		
		
		
		chromeDriver.close();
		

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 3);

		chromeDriver.switchTo().window(player3WindowHandle);
		chromeDriver.close();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 2);

		chromeDriver.switchTo().window(player2WindowHandle);
		chromeDriver.close();
				
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		chromeDriver.switchTo().window(player1WindowHandle);
		chromeDriver.close();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 0);
	
		chromeDriver.switchTo().window(publisherHandle);
		chromeDriver.close();

		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId); 
			return  (broadcast == null);
		});
	}

}
