package io.antmedia.test.cluster;

import static org.bytedeco.ffmpeg.global.avcodec.av_init_packet;
import static org.bytedeco.ffmpeg.global.avcodec.av_packet_unref;
import static org.bytedeco.ffmpeg.global.avformat.av_read_frame;
import static org.bytedeco.ffmpeg.global.avformat.avformat_close_input;
import static org.bytedeco.ffmpeg.global.avformat.avformat_find_stream_info;
import static org.bytedeco.ffmpeg.global.avformat.avformat_open_input;
import static org.bytedeco.ffmpeg.global.avutil.AVMEDIA_TYPE_AUDIO;
import static org.bytedeco.ffmpeg.global.avutil.AVMEDIA_TYPE_VIDEO;
import static org.bytedeco.ffmpeg.global.avutil.AV_NOPTS_VALUE;
import static org.bytedeco.ffmpeg.global.avutil.AV_PIX_FMT_NONE;
import static org.bytedeco.ffmpeg.global.avutil.av_dict_free;
import static org.bytedeco.ffmpeg.global.avutil.av_dict_set;
import static org.bytedeco.ffmpeg.global.avutil.av_rescale_q;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.awaitility.Awaitility;
import org.bytedeco.ffmpeg.avcodec.AVCodecContext;
import org.bytedeco.ffmpeg.avcodec.AVCodecParameters;
import org.bytedeco.ffmpeg.avcodec.AVPacket;
import org.bytedeco.ffmpeg.avformat.AVFormatContext;
import org.bytedeco.ffmpeg.avutil.AVDictionary;
import org.bytedeco.ffmpeg.avutil.AVRational;
import org.bytedeco.ffmpeg.global.avformat;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.EncoderSettings;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.settings.ServerSettings;
import io.antmedia.test.SetUpTest;
import io.antmedia.tools.HLSPlayer;
import io.antmedia.tools.RTMPPublisher;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
public class AdaptiveStreamingTest extends SetUpTest{

	private static Logger log = LoggerFactory.getLogger(AdaptiveStreamingTest.class);


	private static long videoDuration;
	private static long audioDuration;
	private static String appName = Constants.WEBRTCAPPEE;

	@BeforeClass
	public static void setUp() {
		setUpCluster();
	}

	@Test
	public void testAdaptiveStreaming() {
		assertTrue(RestManager.login(Config.ORIGIN_1_IP));

		//get settings from origin
		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
		{
			AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, appName);
			return settings != null;
		});

		AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, appName);

		List<EncoderSettings> encoderSettings = settings.getEncoderSettings();
		//Encoder settings should be null or empty
		assertTrue(encoderSettings == null || encoderSettings.isEmpty());

		encoderSettings = new ArrayList<>();
		encoderSettings.add(new EncoderSettings(240, 300000, 32000,true));

		settings.setEncoderSettings(encoderSettings);
		settings.setDashMuxingEnabled(true);
		settings.setForceAspectRatioInTranscoding(true);

		boolean changeSettings = RestManager.changeSettings(Config.ORIGIN_1_IP, appName, settings);
		assertTrue(changeSettings);
		
		ServerSettings originServerSettings = RestManager.getServerSettings(Config.ORIGIN_1_IP, null);
		originServerSettings.setLogLevel(ServerSettings.LOG_LEVEL_DEBUG);
		assertTrue(RestManager.changeServerSettings(Config.ORIGIN_1_IP, originServerSettings, null));
		

		
		
		testHttpAdaptiveWithKnownStream(null);
		
		testHttpAdaptiveWithKnownStream("subfolder");
		
		testHttpAdaptiveStreaming();

		//restore the settings
		settings.setEncoderSettings(null);
		assertTrue(RestManager.changeSettings(Config.ORIGIN_1_IP, appName, settings));

		settings = RestManager.getSettings(Config.ORIGIN_1_IP, appName);

		encoderSettings = settings.getEncoderSettings();
		//Encoder settings should be null or empty
		assertTrue(encoderSettings == null || encoderSettings.isEmpty());
			
	}
	



	public void testHttpAdaptiveWithKnownStream(String subfolder) {

		try {
			CookieStore cookieStore = new BasicCookieStore();
			HttpContext customContext = new BasicHttpContext();
			customContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

			assertTrue(RestManager.login(Config.EDGE_1_IP, customContext));
			//create broadcast in the edge
			Broadcast broadcast = RestManager.createBroadcast(Config.EDGE_1_IP, appName, customContext, false, subfolder);
			
			//set log level to "debug" to debug easily
			ServerSettings serverSettings = RestManager.getServerSettings(Config.EDGE_1_IP, customContext);
			serverSettings.setLogLevel(ServerSettings.LOG_LEVEL_DEBUG);
			assertTrue(RestManager.changeServerSettings(Config.EDGE_1_IP, serverSettings, customContext));
			

			
			String streamId = broadcast.getStreamId();
			//publish stream to origin
			RTMPPublisher rtmpPublisher = new RTMPPublisher(Config.ORIGIN_1_IP, "src/test/resources/Test_600kbps.mp4", Constants.WEBRTCAPPEE, broadcast.getStreamId(), testFolder, 1);
			rtmpPublisher.start();

			//check that stream is started
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> { 
				Broadcast broadcast2 = RestManager.getBroadcast(Config.ORIGIN_1_IP, appName, streamId);
				if (broadcast2 != null) {
					return broadcast2.isPublicStream(); 
				}
				return false;
			});

			//check that stream exists in the origin
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					testFile("http://"+Config.ORIGIN_1_IP+":5080/"+appName+"/streams/" + (subfolder != null ? (subfolder + "/") : "") + streamId + ".m3u8"));

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					testFile("http://"+Config.ORIGIN_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + ".mpd"));

			//check original stream
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					testFile("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + (subfolder != null ? (subfolder + "/") : "") + streamId + ".m3u8"));

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					testFile("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + ".mpd"));

			//check 240p stream
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					testFile("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + (subfolder != null ? (subfolder + "/") : "") + streamId + "_240p300kbps.m3u8"));



			//adaptive segments should exist for dash
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					HLSPlayer.checkURLExist("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + "_0.m4s"));

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					HLSPlayer.checkURLExist("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + "_1.m4s"));

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					HLSPlayer.checkURLExist("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + "_2.m4s"));

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					HLSPlayer.checkURLExist("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + "_3.m4s"));

			//check adaptive stream
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> 
					testFile("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + (subfolder != null ? (subfolder + "/") : "") + streamId + "_adaptive.m3u8"));

			//stop rtmp publisher
			rtmpPublisher.stop();

		}
		catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}

	}

	public void testHttpAdaptiveStreaming() 
	{

		//this is 360p stream so it should create 240p stream
		String streamId = "streamId_" + (int)(Math.random()*9999999);

		//publish stream to origin
		RTMPPublisher rtmpPublisher = new RTMPPublisher(Config.ORIGIN_1_IP, "src/test/resources/Test_600kbps.mp4", Constants.WEBRTCAPPEE, streamId, testFolder, 1);
		rtmpPublisher.start();

		//check that stream is started
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, appName, streamId);
			if (broadcast != null) {
				return broadcast.isPublicStream(); 
			}
			return false;
		});

		//check that stream exists in the origin
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				testFile("http://"+Config.ORIGIN_1_IP+":5080/"+appName+"/streams/" + streamId + ".m3u8"));
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				testFile("http://"+Config.ORIGIN_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + ".mpd"));

		//check original stream
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				testFile("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + ".m3u8"));
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				testFile("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + ".mpd"));

		//check 240p stream
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				testFile("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "_240p300kbps.m3u8"));


		//check adaptive stream
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				testFile("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "_adaptive.m3u8"));

		//adaptive segments should exist for dash
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				HLSPlayer.checkURLExist("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + "_0.m4s"));

		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				HLSPlayer.checkURLExist("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + "_1.m4s"));

		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				HLSPlayer.checkURLExist("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + "_2.m4s"));

		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
				() -> 
				HLSPlayer.checkURLExist("http://"+Config.EDGE_1_IP+":5080/"+appName+"/streams/" + streamId + "/" + streamId + "_3.m4s"));


		//stop rtmp publisher
		rtmpPublisher.stop();

	}


	public static boolean testFile(String url) {
		return testFile(url, 0, false, true, true, 0, 0);
	}


	public static boolean testFile(String absolutePath, int expectedDurationInMS, boolean fullRead,
			boolean controlVideo, boolean controlAudio, int videoBitrate, int audioBitrate) {
		int ret;

		AVFormatContext inputFormatContext = avformat.avformat_alloc_context();

		log.info("testing stream -> " + absolutePath);

		if (inputFormatContext == null) {
			System.err.println("cannot allocate input context");
			return false;
		}
		
		AVDictionary optionsDictionary = new AVDictionary();
		av_dict_set(optionsDictionary, "analyzeduration", 10000000+"", 0);
		av_dict_set(optionsDictionary, "probesize", 10000000+"", 0);


		if ((ret = avformat_open_input(inputFormatContext, absolutePath, null, (AVDictionary) optionsDictionary)) < 0) {
			System.err.println("cannot open input context");
			return false;
		}
		
		av_dict_free(optionsDictionary);
		optionsDictionary.close();
		
		ret = avformat_find_stream_info(inputFormatContext, (AVDictionary)null);
		if (ret < 0) {
			log.info("Could not find stream information\n");
			return false;
		}

		int streamCount = inputFormatContext.nb_streams();

		if (streamCount == 0) {
			log.info("Could not find stream information\n");
			return false;
		}


		boolean streamExists = false;
		AVRational milliSeconds = new AVRational();
		milliSeconds.num(1).den(1000);
		for (int i = 0; i < streamCount; i++) {
			AVCodecParameters codecparams = inputFormatContext.streams(i).codecpar();

			if (codecparams.codec_type() == AVMEDIA_TYPE_VIDEO) {
				if (!controlVideo) {
					continue;
				}


				AVRational videoTimebase = new AVRational();
				videoTimebase.num( inputFormatContext.streams(i).time_base().num());
				videoTimebase.den( inputFormatContext.streams(i).time_base().den());
				videoDuration = av_rescale_q(inputFormatContext.streams(i).duration(),  inputFormatContext.streams(i).time_base(), milliSeconds);

				log.info("video bitrate: " + inputFormatContext.streams(i).codecpar().bit_rate());
				log.info("video duration: " + videoDuration);
				assertTrue(codecparams.width() != 0);
				assertTrue(codecparams.height() != 0);
				assertTrue(codecparams.format() != AV_PIX_FMT_NONE);
				if (videoBitrate != 0 && Math.abs(videoBitrate - codecparams.bit_rate()) > 200000) {
					fail("Video bitrate does not match expected: " + videoBitrate + "actual: " + codecparams.bit_rate());
				}
				streamExists = true;
			} else if (codecparams.codec_type() == AVMEDIA_TYPE_AUDIO) {
				if (!controlAudio) {
					continue;
				}
				audioDuration = av_rescale_q(inputFormatContext.streams(i).duration(),  inputFormatContext.streams(i).time_base(), milliSeconds);

				log.info("audio bitrate: " + inputFormatContext.streams(i).codecpar().bit_rate());
				log.info("audio duration: " + audioDuration);
				assertTrue(codecparams.sample_rate() != 0);
				streamExists = true;

				if (audioBitrate != 0 && Math.abs(audioBitrate - codecparams.bit_rate()) > 20000) {
					fail("audio bitrate does not match expected: " + audioBitrate + "actual: " + codecparams.bit_rate());
				}
			}
		}
		if (!streamExists) {
			return streamExists;
		}

		log.info("testing stream -> reading packets for stream: " + absolutePath);
		int i = 0;
		AVPacket pkt = new AVPacket();
		while (fullRead || i < 3) {
			av_init_packet(pkt);
			pkt.data(null);
			pkt.size(0);
			ret = av_read_frame(inputFormatContext, pkt);

			if (ret < 0) {
				break;

			}
			i++;
			av_packet_unref(pkt);
		}

		assertFalse(expectedDurationInMS != 0 && inputFormatContext.duration() == AV_NOPTS_VALUE);

		log.info("testing stream -> duration check for : " + absolutePath);
		if (inputFormatContext.duration() != AV_NOPTS_VALUE) {
			long durationInMS = inputFormatContext.duration() / 1000;

			if (expectedDurationInMS != 0) {
				log.info("Stream duration: " + durationInMS);
				if ((durationInMS < (expectedDurationInMS - 2000)) || (durationInMS > (expectedDurationInMS + 2000))) {
					log.info("Failed: duration of the stream: " + durationInMS + " expected duration: " + expectedDurationInMS);
					if (absolutePath.endsWith(".m3u8") && absolutePath.indexOf(":") == -1) {
						try {
							String readFileToString = FileUtils.readFileToString(new File(absolutePath), Charset.defaultCharset());
							log.info("Content of the file ");
							log.info(readFileToString);
						} catch (IOException e) {
							e.printStackTrace();
						}

					}

					return false;
				}
			}
		}
		log.info("testing stream -> leaving: " + absolutePath);

		avformat_close_input(inputFormatContext);
		return true;

	}



}
