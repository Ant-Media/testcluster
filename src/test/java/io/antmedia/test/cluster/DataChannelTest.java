package io.antmedia.test.cluster;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.standalone.DataChannelStandaloneTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;

public class DataChannelTest extends SetUpTest{

	private static String streamId = "TestWebrtcStream_datachannel";

	@BeforeClass
	public static void setUpClass() {
		setUpCluster();
	}

	@Before
	public void setUp() {
		chromeDriver = FrontEndUtils.getChromeDriver();
	}

	@After
	public void after() {
		chromeDriver.quit();

	}	

	private ChromeDriver chromeDriver;
	private String publishWindowHandle;
	private String play1WindowHandle;
	private String play2WindowHandle;


	@Test
	public void testDataChannel()  {
		AppSettings settings;
		{
			String publisherMessage = "hello from publisher";
			String play1Message = "hello from play1";
			String play2Message = "hello from play2";

			//get settings from origin
			assertTrue(RestManager.login(Config.NGINX_IP));
			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
			{
				AppSettings settingsLocaL = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
				return settingsLocaL != null;
			});


			settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
			settings.setDataChannelEnabled(false);
			assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));

			startPeers();

			DataChannelStandaloneTest.sendMessageForPublisher(chromeDriver, publisherMessage, publishWindowHandle);

			Utils.mustSleep(3000);

			assertFalse(DataChannelStandaloneTest.isMessageFound(chromeDriver, play1WindowHandle, publisherMessage, false));
			assertFalse(DataChannelStandaloneTest.isMessageFound(chromeDriver, play2WindowHandle, publisherMessage,  false));


			stopPeers();
		}

		settings.setDataChannelEnabled(true);
		assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));




		startPeers();

		{
			String publisherMessage = RandomStringUtils.randomAlphanumeric(75);
			String play1Message =  RandomStringUtils.randomAlphanumeric(75);
			String play2Message =  RandomStringUtils.randomAlphanumeric(75);

			DataChannelStandaloneTest.sendMessageForPublisher(chromeDriver, publisherMessage, publishWindowHandle);
			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
			until(() -> {
				return DataChannelStandaloneTest.isMessageFound(chromeDriver, play1WindowHandle, publisherMessage, false)
						&& DataChannelStandaloneTest.isMessageFound(chromeDriver, play2WindowHandle, publisherMessage,  false);
			});
		}

		{

			//reset last message
			String publisherMessage = RandomStringUtils.randomAlphanumeric(75);
			String play1Message =  RandomStringUtils.randomAlphanumeric(75);
			String play2Message =  RandomStringUtils.randomAlphanumeric(75);


			settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
			assertEquals(AppSettings.DATA_CHANNEL_PLAYER_TO_ALL, settings.getDataChannelPlayerDistribution());

			DataChannelStandaloneTest.sendMessageForPlayer(chromeDriver, play1WindowHandle, play1Message);

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
			until(() -> {

				return DataChannelStandaloneTest.isMessageFound(chromeDriver, publishWindowHandle, play1Message,  true)
						&& DataChannelStandaloneTest.isMessageFound(chromeDriver, play2WindowHandle, play1Message,  false);
			});


			DataChannelStandaloneTest.sendMessageForPlayer(chromeDriver, play2WindowHandle, play2Message);


			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
			until(() -> {
				return  DataChannelStandaloneTest.isMessageFound(chromeDriver, publishWindowHandle, play2Message,  true)
						&& DataChannelStandaloneTest.isMessageFound(chromeDriver, play1WindowHandle, play2Message,  false);
			});

		}


		settings.setDataChannelPlayerDistribution(AppSettings.DATA_CHANNEL_PLAYER_TO_NONE);
		assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));

		{
			String publisherMessage = RandomStringUtils.randomAlphanumeric(75);
			String play1Message =  RandomStringUtils.randomAlphanumeric(75);
			String play2Message =  RandomStringUtils.randomAlphanumeric(75);

			DataChannelStandaloneTest.sendMessageForPlayer(chromeDriver, play1WindowHandle, play1Message);

			Utils.mustSleep(5000);

			assertFalse(DataChannelStandaloneTest.isMessageFound(chromeDriver, publishWindowHandle, play1Message, true));
			assertFalse(DataChannelStandaloneTest.isMessageFound(chromeDriver, play2WindowHandle, play1Message, false));

		}
		//reset last message

		{
			String publisherMessage = RandomStringUtils.randomAlphanumeric(75);
			String play1Message =  RandomStringUtils.randomAlphanumeric(75);
			String play2Message =  RandomStringUtils.randomAlphanumeric(75);
			settings.setDataChannelPlayerDistribution(AppSettings.DATA_CHANNEL_PLAYER_TO_PUBLISHER);
			assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));

			Utils.mustSleep(5000);

			DataChannelStandaloneTest.sendMessageForPlayer(chromeDriver, play1WindowHandle, play1Message);

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
			until(() -> {
				return DataChannelStandaloneTest.isMessageFound(chromeDriver, publishWindowHandle, play1Message, true)
						&& !DataChannelStandaloneTest.isMessageFound(chromeDriver, play2WindowHandle, play1Message, false);
			});
		}

		stopPeers();
	}

	private void startPeers() {

		FrontEndUtils.publishStream(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId); 
			if (broadcast != null) {
				return broadcast.isPublicStream();
			}
			return false;
		});

		publishWindowHandle = chromeDriver.getWindowHandle();

		assertEquals(0, RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount());

		chromeDriver.findElement(By.id("options")).click();




		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		play1WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> 
		{
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId);
			return broadcast != null && broadcast.getWebRTCViewerCount() == 1;
		});

		chromeDriver.findElement(By.id("options")).click();


		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		play2WindowHandle = chromeDriver.getWindowHandle();

		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_2_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> {
			
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId);
			return broadcast != null && broadcast.getWebRTCViewerCount() == 2; 
		 });
		chromeDriver.findElement(By.id("options")).click();



	}

	private void stopPeers() {

		chromeDriver.switchTo().window(play2WindowHandle);
		chromeDriver.findElement(By.id("stop_play_button")).click();


		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() ->{ 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId);
			return broadcast != null && broadcast.getWebRTCViewerCount() == 1;
		});


		chromeDriver.switchTo().window(play1WindowHandle);
		chromeDriver.findElement(By.id("stop_play_button")).click();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 0);

		//stop publisher
		chromeDriver.switchTo().window(publishWindowHandle);
		chromeDriver.switchTo().frame(0);
		chromeDriver.findElement(By.id("stop_publish_button")).click();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId); 
			return  (broadcast == null);
		});
	}

	@Test
	public void testSuccessiveMessages() {
		List<String> messageList = new ArrayList<>();
		int size = 100;

		for (int i=0; i< size; i++) {
			String msg = "a";
			for(int k=0;k<i;k++) {
				msg += "o";
			}
			//msg += RandomStringUtils.random(i);
			msg += "z";
			messageList.add(msg);
		}

		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
		{
			AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
			return settings != null;
		});

		AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
		settings.setDataChannelEnabled(true);
		settings.setDataChannelPlayerDistribution(AppSettings.DATA_CHANNEL_PLAYER_TO_ALL);
		assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));

		Utils.mustSleep(6000);

		startPeers();

		for (int i=0; i< size; i++) {
			DataChannelStandaloneTest.sendMessageForPublisher(chromeDriver, messageList.get(i), publishWindowHandle);
		}

		Utils.mustSleep(3000);


		DataChannelStandaloneTest.isMessageFound(chromeDriver, play2WindowHandle, play1WindowHandle, false);

		for (int i=0; i< size; i++) {

			assertTrue(DataChannelStandaloneTest.isMessageFound(chromeDriver, play1WindowHandle, messageList.get(i), false));


			assertTrue(DataChannelStandaloneTest.isMessageFound(chromeDriver, play2WindowHandle, messageList.get(i), false));
		}


		stopPeers();
	}

}
