package io.antmedia.test.cluster;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.EncoderSettings;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.datastore.db.types.Endpoint;
import io.antmedia.muxer.IAntMediaStreamHandler;
import io.antmedia.rest.model.Result;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;

public class PublishPlayTest extends SetUpTest{

	private static Logger log = LoggerFactory.getLogger(PublishPlayTest.class);

	private static String globalStreamId = "publishplaytest";

	@BeforeClass
	public static void setUp() {
		setUpCluster();
	}

	private ChromeDriver chromeDriver;
	
	
	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();

	}
	
	@After
	public void after() {
		chromeDriver.quit();
	}
	

	@Test
	public void testWebRTCVideo() {

		String streamId = globalStreamId + "webRTCVideo";

		webRTCPublishPlay("src/test/resources/test.mp4", streamId, true);
	}

	@Test
	public void testWebRTCAudioSFU() {
		assertTrue(RestManager.login(Config.NGINX_IP));
		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(()-> {
			AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);

			return settings != null;
		});
		AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
		ArrayList<EncoderSettings> encoderSettings = new ArrayList<>();
		settings.setEncoderSettings(encoderSettings);
		assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));
		Utils.mustSleep(6000);

		String streamId = globalStreamId + "WebRTCAudioSFU";
		webRTCPublishPlay("src/test/resources/audiotest.opus", streamId, true);
	}

	@Test
	public void testWebRTCAudioABR() {
		assertTrue(RestManager.login(Config.NGINX_IP));
		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(()-> {
			AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
			return settings != null;
		});

		AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
		ArrayList<EncoderSettings> encoderSettings = new ArrayList<>();
		encoderSettings.add(new EncoderSettings(240,500000,32000,true));
		settings.setEncoderSettings(encoderSettings);
		assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));
		Utils.mustSleep(6000);

		String streamId = globalStreamId + "WebRTCAudioABR";
		webRTCPublishPlay("src/test/resources/audiotest.opus", streamId, true);
	}




	public static void testRestProxyDirectly(ChromeDriver chromeDriver, String origin1IP, String edgeIP, String subnet) {

		try {
			
			assertTrue(RestManager.login(origin1IP));
			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(() -> {
				AppSettings settings = RestManager.getSettings(origin1IP, Constants.WEBRTCAPPEE);
				return settings != null;
			});

			AppSettings settings = RestManager.getSettings(origin1IP, Constants.WEBRTCAPPEE);
			String remoteAllowedCIDR = settings.getRemoteAllowedCIDR();
			settings.setRemoteAllowedCIDR(settings.getRemoteAllowedCIDR() + "," + subnet);
			assertTrue(RestManager.changeSettings(origin1IP, Constants.WEBRTCAPPEE, settings));

			String streamId = "streamId" + (int)(Math.random() * 99999999);
			
			
			
			FrontEndUtils.publishStream(chromeDriver, origin1IP, Constants.WEBRTCAPPEE, streamId);
			String publisherHandle = chromeDriver.getWindowHandle();
			

			//check that it's playing
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> 
			{
				Broadcast broadcast = RestManager.getBroadcast(origin1IP, Constants.WEBRTCAPPEE, streamId);
				if (broadcast != null) {
					return broadcast.isPublicStream() 
							&& IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus());
				}
				return false;
			});


			//delete broadcast through different node. It should forward the request to origin and it should delete it
			Result result = RestManager.deleteBroadcast(edgeIP, streamId, Constants.WEBRTCAPPEE, true);
			assertTrue(result.isSuccess());


			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
			until(() -> 
			{
				Broadcast broadcast = RestManager.getBroadcast(origin1IP, Constants.WEBRTCAPPEE, streamId);
				return broadcast == null;
			});


			chromeDriver.close();

			settings = RestManager.getSettings(origin1IP, Constants.WEBRTCAPPEE);
			settings.setRemoteAllowedCIDR(remoteAllowedCIDR);
			assertTrue(RestManager.changeSettings(origin1IP, Constants.WEBRTCAPPEE, settings));
		}
		catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public void testRestProxyPrivateIPAddress() {
		testRestProxyDirectly(chromeDriver, Config.ORIGIN_1_IP, Config.EDGE_1_IP, Config.SUBNET);
	}





	/**
	 * Following test calls the add-remove endpoint to the different instances on the fly. 
	 * It tests that backend is working as a unit. It finds the correct instance in the backend
	 */
	@Test
	public void testAddEndpoint(){ 

		//Change settings to access the REST API from the network
		assertTrue(RestManager.login(Config.ORIGIN_1_IP));
		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(() -> {
			AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
			return settings != null;
		});


		CookieStore cookieStore = new BasicCookieStore();
		HttpContext customContext = new BasicHttpContext();
		customContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

		String appName = Constants.WEBRTCAPPEE;
		final String streamId;
		assertTrue(RestManager.login(Config.EDGE_1_IP, customContext));

		try{
			//Send a stream to Origin IP
			//create broadcast in the edge
			Broadcast broadcast2 = RestManager.createBroadcast(Config.EDGE_1_IP, appName, customContext);
			streamId = broadcast2.getStreamId();

			FrontEndUtils.publishStream(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
			String publisherHandle = chromeDriver.getWindowHandle();
			
			//check that it's playing
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> {
				Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
				if (broadcast != null) {
					return broadcast.isPublicStream() 
							&& IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus());
				}
				return false;
			});


			//Add rtmp endpoint to the stream through Edge 1 IP

			String rtmpEndpointStreamId = "streamId" + (int)(Math.random() * 100000);
			String rtmpUrl = "rtmp://"+ Config.EDGE_2_IP +"/LiveApp/" + rtmpEndpointStreamId;
			Endpoint endpoint = new Endpoint();
			endpoint.setRtmpUrl(rtmpUrl);

			Result endpointResult = RestManager.addEndpoint(Config.EDGE_1_IP, Constants.WEBRTCAPPEE, customContext, streamId, endpoint);
			assertTrue(endpointResult.isSuccess());

			//check that rtmp endpoint is working

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(3,  TimeUnit.SECONDS).until(()-> 
			{
				Broadcast tmp2 = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
				return tmp2.getEndPointList() != null && tmp2.getEndPointList().size() > 0 && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(tmp2.getEndPointList().get(0).getStatus());
			});

			//check the stream directly
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(3,  TimeUnit.SECONDS).until(()-> 
			{
				Broadcast tmp2 = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.LIVEAPP, rtmpEndpointStreamId);
				return tmp2 != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(tmp2.getStatus());
			});

			//delete the rtmp endpoint through Edge 1 IP
			Result result = RestManager.removeEndpoint(Config.EDGE_1_IP, Constants.WEBRTCAPPEE, customContext, streamId, endpointResult.getDataId());
			assertTrue(result.isSuccess());

			//check that it's deleted and not streaming
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(3,  TimeUnit.SECONDS).until(()-> 
			{
				Broadcast tmp2 = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.LIVEAPP, rtmpEndpointStreamId);
				return tmp2 == null;
			});

			chromeDriver.close();


		}
		catch(Exception e){
			log.info(ExceptionUtils.getStackTrace(e));
			fail(e.getMessage());
		}







	}

	@Test
	public void testWebRTCWithKnownStream() {
		CookieStore cookieStore = new BasicCookieStore();
		HttpContext customContext = new BasicHttpContext();
		customContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

		assertTrue(RestManager.login(Config.EDGE_1_IP, customContext));
		String appName = Constants.WEBRTCAPPEE;
		try {
			//create broadcast in the edge
			Broadcast broadcast = RestManager.createBroadcast(Config.EDGE_1_IP, appName, customContext);
			String streamId = broadcast.getStreamId();

			log.info("Stream id " + streamId);
			webRTCPublishPlay("src/test/resources/test.mp4", streamId, false);
		}
		catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	public void webRTCPublishPlay(String file, String streamId, boolean zombiStream)  {
		
		FrontEndUtils.publishStream(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
		String publisherHandle = chromeDriver.getWindowHandle();
		
		assertTrue(RestManager.login(Config.NGINX_IP));
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId); 
			if (broadcast != null) {
				return IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus());
			}
			return false;
		});

		assertEquals(0, RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount());

		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player1WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);

		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player2WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_2_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 2);

		
		
		chromeDriver.close();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);

		chromeDriver.switchTo().window(player1WindowHandle);
		chromeDriver.close();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 0);

		chromeDriver.switchTo().window(publisherHandle);
		chromeDriver.close();

		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId); 
			if (zombiStream) {
				return  (broadcast == null);
			}
			else {
				return broadcast != null && "finished".equals(broadcast.getStatus());
			}
		});
	}

	/*
	 * This test is for testing calling the REST API successively
	 * Because of a bug in the backend, it returns 500 error after a while when request has cookie
	 * We will use Edge1 for REST API calls and Origin1 for publishing
	 */
	@Test
	public void testRestProxyWithCookie() {

		try {

			assertTrue(RestManager.login(Config.EDGE_1_IP));
			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(() -> {
				AppSettings settings = RestManager.getSettings(Config.EDGE_1_IP, Constants.WEBRTCAPPEE);
				return settings != null;
			});

			AppSettings settings = RestManager.getSettings(Config.EDGE_1_IP, Constants.WEBRTCAPPEE);
			String remoteAllowedCIDR = settings.getRemoteAllowedCIDR();
			settings.setRemoteAllowedCIDR(settings.getRemoteAllowedCIDR() + "," + Config.SUBNET);
			assertTrue(RestManager.changeSettings(Config.EDGE_1_IP, Constants.WEBRTCAPPEE, settings));

			String streamId = "streamId" + (int)(Math.random() * 99999999);



			FrontEndUtils.publishStream(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, streamId);
			String publisherHandle = chromeDriver.getWindowHandle();


			//check that it's playing
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
					until(() ->
					{
						Broadcast broadcast = RestManager.getBroadcast(Config.EDGE_1_IP, Constants.WEBRTCAPPEE, streamId);
						if (broadcast != null) {
							return broadcast.isPublicStream()
									&& IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus());
						}
						return false;
					});


			for (int i = 0; i < 100; i++) {
				Result result = RestManager.sendData(Config.EDGE_1_IP, streamId, Constants.WEBRTCAPPEE, "hello"+i);
				assertTrue(result.isSuccess());
			}

			chromeDriver.close();

			settings = RestManager.getSettings(Config.EDGE_1_IP, Constants.WEBRTCAPPEE);
			settings.setRemoteAllowedCIDR(remoteAllowedCIDR);
			assertTrue(RestManager.changeSettings(Config.EDGE_1_IP, Constants.WEBRTCAPPEE, settings));
		}
		catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
