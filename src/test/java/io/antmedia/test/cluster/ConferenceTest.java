package io.antmedia.test.cluster;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.util.Base32;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.datastore.db.types.ConferenceRoom;
import io.antmedia.datastore.db.types.Subscriber;
import io.antmedia.muxer.IAntMediaStreamHandler;
import io.antmedia.rest.model.Result;
import io.antmedia.security.ITokenService;
import io.antmedia.security.TOTPGenerator;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.antmedia.websocket.WebSocketConstants;
import io.vertx.core.file.FileSystemOptions;

public class ConferenceTest extends SetUpTest{
	
	private static Logger log = LoggerFactory.getLogger(ConferenceTest.class);

	
	@BeforeClass
	public static void setUp() {
		setUpCluster();
	}

	private ChromeDriver chromeDriver;

	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();

	}

	@After
	public void after() {
		chromeDriver.quit();

	}
	
	public static String generateJwtToken(String jwtSecretKey, String streamId, long expireDateUnixTimeStampMs, String type) 
	{
		Date expireDateType = new Date(expireDateUnixTimeStampMs);
		String jwtTokenId = null;
		try {
			Algorithm algorithm = Algorithm.HMAC256(jwtSecretKey);

			jwtTokenId = JWT.create().
					withClaim("streamId", streamId).
					withClaim("type", type).
					withExpiresAt(expireDateType).
					sign(algorithm);

		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e));
		}

		return jwtTokenId;
	}
	
	/**
	 * This test is to check the conference room availability and what happens if we terminate the origin of the user is in the conference
	 * 
	 */
	public void testConferenceAvailability() 
	{
		String roomId = "testConferenceAvailability";
		assertTrue(RestManager.login(Config.NGINX_IP));
		
		String user1 = "user1";
		//join conference with an user
		FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId, user1, null);
		String user1WindowHandle = chromeDriver.getWindowHandle();
		
		//check that it has joined
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId); 
			
			return  (broadcast != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus())
					&& StringUtils.isBlank(broadcast.getOriginAdress())); //origin address should be blank because it's a virtual stream
		});
		
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, user1); 
			return  (broadcast != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus()));
		});
		
		//join conference with another user
		String user2 = "user2";
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String user2WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId, user2, null);
		
		//check that it has joined
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, user2); 
			return  (broadcast != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus()));
		});
		
		//find the origin of one of the stream and terminate it
		Broadcast user2Broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, user2); 
		String firstOriginAdress = user2Broadcast.getOriginAdress();
		
		String originName = null;
	
		if (Config.ORIGIN_1_IP.equals(firstOriginAdress)) {
			originName = Config.ORIGIN_1_NAME;
		}
		else if (Config.ORIGIN_2_IP.equals(firstOriginAdress)) {
			originName = Config.ORIGIN_2_NAME;
		}
		else if (Config.EDGE_1_IP.equals(firstOriginAdress)) {
			originName = Config.EDGE_1_NAME;
		}
		else if (Config.EDGE_2_IP.equals(firstOriginAdress)) {
			originName = Config.EDGE_2_NAME;
		}
		else if (Config.EDGE_3_IP.equals(firstOriginAdress)) {
			originName = Config.EDGE_3_NAME;
		}
		else if (Config.EDGE_4_IP.equals(firstOriginAdress)) {
			originName = Config.EDGE_4_NAME;
		}
		assertNotNull(originName);
		
		assertTrue(dm.stopAndRemoveContainer(originName));
		//check that the reconnect is established
		
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, user2); 
			return  (broadcast != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(user2Broadcast.getStatus())
					&& StringUtils.isNotBlank(broadcast.getOriginAdress()) && StringUtils.equals(broadcast.getOriginAdress(), firstOriginAdress));
		});
		
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId); 
			return  (broadcast != null && IAntMediaStreamHandler.BROADCAST_STATUS_BROADCASTING.equals(user2Broadcast.getStatus()));
		});

		chromeDriver.switchTo().window(user1WindowHandle);
		chromeDriver.close();
		
		chromeDriver.switchTo().window(user2WindowHandle);
		chromeDriver.close();
		
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId); 
			return  (broadcast == null);
		});
		
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, user1); 
			return  (broadcast == null);
		});
		
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, user2); 
			return  (broadcast == null);
		});
		
		//restart the killed container 
		assertTrue(dm.createAntMediaContainer(firstOriginAdress, originName, Config.MONGO_1_IP, true));


	
	}

	@Test
	public void testMultiTrackConference() {
		String roomId = "testMultiTrackConference";
		assertTrue(RestManager.login(Config.ORIGIN_1_IP));


		FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId, "p1", null);
		String publisher1WindowHandle = chromeDriver.getWindowHandle();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId); 
			return  (broadcast != null);
		});

		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String publisher2WindowHandle = chromeDriver.getWindowHandle();

		FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId, "p2", null);

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId).getWebRTCViewerCount() == 2);


		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player1WindowHandle = chromeDriver.getWindowHandle();

		FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId, null, "playOnly=true");

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId).getWebRTCViewerCount() == 3);


		chromeDriver.switchTo().window(publisher1WindowHandle);
		chromeDriver.close();


		chromeDriver.switchTo().window(publisher2WindowHandle);
		chromeDriver.close();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId); 
			return  (broadcast == null);
		});

	}
	
	@Test
	public void testMultiTrackConferenceWithJWTandDelete() throws Exception {
		String roomId = "room_testMultiTrackConferenceWithJWTandDelete";
		
		assertTrue(RestManager.login(Config.ORIGIN_1_IP));

		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
		{
			AppSettings settingsLocaL = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
			return settingsLocaL != null;
		});


		AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
		settings.setTimeTokenSecretForPublish(RandomStringUtils.randomAlphanumeric(10));
		settings.setEnableTimeTokenForPublish(false);
		settings.setEnableTimeTokenForPlay(false);
		settings.setMaxAudioTrackCount(-1);
		settings.setMaxVideoTrackCount(-1);
		
		String secretKey = RandomStringUtils.randomAlphanumeric(32);
		settings.setJwtStreamSecretKey(secretKey);
		settings.setPublishJwtControlEnabled(true);
		
		assertTrue(RestManager.changeSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, settings));
		
		String p1 = roomId + "__" + "p1";
		
		//login with roomId token
		String token = generateJwtToken(secretKey, roomId, System.currentTimeMillis() + 5000, "publish");
		FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId, p1, "token=" + token);
		String publisher1WindowHandle = chromeDriver.getWindowHandle();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId); 
			return  (broadcast != null);
		});

		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String publisher2WindowHandle = chromeDriver.getWindowHandle();

		String p2 = roomId + "__" + "p2";

		//login with stream id token
		token = generateJwtToken(secretKey, p2, System.currentTimeMillis() + 5000, "publish");
		//publish to edge 1
		FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, roomId, p2, "token=" + token);

		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId).getWebRTCViewerCount() == 2);
		
		
		//confirm that p1 exists
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, p1) != null);
		
		//confirm that p2 exists
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, p2) != null);
		
	
		//delete the room
		RestManager.deleteBroadcast(Config.ORIGIN_1_IP, roomId, Constants.WEBRTCAPPEE, false);
		
		
		//all subtracks should be deleted 
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, p1) == null);
	
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, p2) == null);
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId) == null);
		
		
		settings.setJwtStreamSecretKey("");
		settings.setPublishJwtControlEnabled(false);
		
		assertTrue(RestManager.changeSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, settings));
		


	}


	@Test
	public void testMultiTrackConferenceTOTPAndBlock() {

		try  {
			String roomId = "room1_testMultiTrackConferenceTOTPAndBlock";
			assertTrue(RestManager.login(Config.ORIGIN_1_IP));

			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> 
			{
				AppSettings settingsLocaL = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
				return settingsLocaL != null;
			});


			AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
			settings.setTimeTokenSecretForPublish(RandomStringUtils.randomAlphanumeric(10));
			settings.setEnableTimeTokenForPublish(true);
			settings.setEnableTimeTokenForPlay(true);
			settings.setMaxAudioTrackCount(-1);
			settings.setMaxVideoTrackCount(-1);
			

			assertTrue(RestManager.changeSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, settings));
			
			

			//publish subscriber1 to origin1
			String publisherStreamId = "stream1";
			String subscriberId = "subscriber1";
			String secretCode = TOTPGenerator.getSecretCodeForNotRecordedSubscriberId(subscriberId, roomId, Subscriber.PUBLISH_TYPE, settings.getTimeTokenSecretForPublish());
			String totpCode = TOTPGenerator.generateTOTP(Base32.decode(secretCode.getBytes()), settings.getTimeTokenPeriod(), 6,  ITokenService.HMAC_SHA1);

			String publisher1WindowHandle = chromeDriver.getWindowHandle();
			FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId, publisherStreamId, "subscriberId="+subscriberId + "&subscriberCode=" + totpCode);


			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> { 
				Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId); 
				return  (broadcast != null);
			});

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> { 
				Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, publisherStreamId); 
				return  (broadcast != null);
			});



			//publish subscriber2 to edge1
			chromeDriver.switchTo().newWindow(WindowType.WINDOW);
			String publisher2WindowHandle = chromeDriver.getWindowHandle();


			String publisherStreamId2 = "stream2";
			String subscriberId2 = "subscriber2";
			secretCode = TOTPGenerator.getSecretCodeForNotRecordedSubscriberId(subscriberId2, roomId, Subscriber.PUBLISH_TYPE, settings.getTimeTokenSecretForPublish());
			totpCode = TOTPGenerator.generateTOTP(Base32.decode(secretCode.getBytes()), settings.getTimeTokenPeriod(), 6,  ITokenService.HMAC_SHA1);

			FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, roomId, publisherStreamId2, "subscriberId="+subscriberId2 + "&subscriberCode=" + totpCode);

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> { 
				Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, publisherStreamId2); 
				return  (broadcast != null);
			});


			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> {
				Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId);
				log.info("broadcast viewer count: " + broadcast.getWebRTCViewerCount());
				return broadcast.getWebRTCViewerCount() == 2;
			});

			//make request to the origin_1 while subscrbierId2 is on EDGE_1_IP
			Result subscriber2BlockRes = RestManager.blockSubscriber(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, subscriberId2, roomId, Subscriber.PUBLISH_AND_PLAY_TYPE, 10, false, null);
			assertTrue(subscriber2BlockRes.isSuccess());


			//number of viewer decreases to 1
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId).getWebRTCViewerCount() == 1);


			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
			until(() -> { 
				Broadcast broadcast = RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, publisherStreamId2); 
				return  (broadcast == null);
			});


			settings.setEnableTimeTokenForPublish(false);
			settings.setEnableTimeTokenForPlay(false);

			assertTrue(RestManager.changeSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, settings));

		}
		catch (Exception e) {
			LogEntries entry = chromeDriver.manage().logs().get(LogType.BROWSER);
			List<LogEntry> logs= entry.getAll();
			// Print one by one
			for(LogEntry logEntry: logs)
			{
				log.info("Browser log -> " + logEntry);
			}
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public void testConferenceDataChannelMessageDistribution(){

		/*
		 * - Join 3 participants to multi-track conference.
		 * - Test each participants message is received by other participants and also message is not received by the sender.
		 */

		try {
			String roomId = "room1_testConferenceDataChannelMessageDistribution";
			assertTrue(RestManager.login(Config.ORIGIN_1_IP));

			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() ->
			{
				AppSettings settingsLocal = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
				return settingsLocal != null;
			});


			AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
			settings.setDataChannelPlayerDistribution(AppSettings.DATA_CHANNEL_PLAYER_TO_ALL);
			assertTrue(RestManager.changeSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, settings));


			String participant1StreamId = "participant1StreamId";
			String participant2StreamId = "participant2StreamId";
			String participant3StreamId = "participant3StreamId";

			String participant1WindowHandle = chromeDriver.getWindowHandle();

			FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId,  participant1StreamId, null);

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {

				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
				return broadcast != null && broadcast.getSubTrackStreamIds().contains(participant1StreamId);
			});


			chromeDriver.switchTo().newWindow(WindowType.WINDOW);
			String participant2WindowHandle = chromeDriver.getWindowHandle();

			FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, roomId, participant2StreamId, null);

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
				return broadcast != null && broadcast.getSubTrackStreamIds().contains(participant2StreamId);
			});

			chromeDriver.switchTo().newWindow(WindowType.WINDOW);
			String participant3WindowHandle = chromeDriver.getWindowHandle();

			FrontEndUtils.joinMultiTrackConference(chromeDriver,  Config.EDGE_2_IP, Constants.WEBRTCAPPEE, roomId, participant3StreamId, null);

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
				return broadcast != null && broadcast.getSubTrackStreamIds().contains(participant3StreamId);
			});

			//send a single message from 1st participant and check its received by 2nd and 3rd participant but not 1st participant
			String participant1Message = "from1";
			String participant2Message = "from2";
			String participant3Message = "from3";

			FrontEndUtils.sendConferenceMessage(chromeDriver, participant1WindowHandle, participant1Message);


			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				boolean participant1Sent = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant1WindowHandle, participant1Message, true);
				boolean participant2Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant2WindowHandle, participant1Message, true);
				boolean participant3Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant3WindowHandle, participant1Message, true);
				return participant2Received && participant3Received && participant1Sent;
			});

			FrontEndUtils.sendConferenceMessage(chromeDriver, participant2WindowHandle, participant2Message);

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				boolean participant2Sent = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant2WindowHandle, participant2Message, true);
				boolean participant1Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant1WindowHandle, participant2Message, true);
				boolean participant3Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant3WindowHandle, participant2Message, true);
				return participant1Received && participant3Received && participant2Sent;
			});

			FrontEndUtils.sendConferenceMessage(chromeDriver, participant3WindowHandle, participant3Message);

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				boolean participant3Sent = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant3WindowHandle, participant3Message, true);
				boolean participant1Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant1WindowHandle, participant3Message, true);
				boolean participant2Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant2WindowHandle, participant3Message, true);
				return participant1Received && participant2Received && participant3Sent;
			});

			chromeDriver.switchTo().window(participant1WindowHandle);
			chromeDriver.close();


			chromeDriver.switchTo().window(participant2WindowHandle);
			chromeDriver.close();

			chromeDriver.switchTo().window(participant3WindowHandle);
			chromeDriver.close();

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
					until(() -> {
						Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
						return  (broadcast == null);
					});


		} catch (Exception e){
			e.printStackTrace();
			fail();
		}


	}
	
	
	@Test
	public void testConferenceDataChannelMessageDistributionWithPlayOnlyParticipants(){

		/*
		 * - Join 5 participants to multi-track conference. 
		 *   -  Origin 1: 1 normal participant + 1 Play only
		 *   -  Edge 1: 1 normal participant + 1 Play only
		 *   -  Edge 2: 1 Play only
		 * - Test each participants message is received by other participants and also message is not received by the sender.
		 */

		try {
			String roomId = "room_"+RandomStringUtils.randomAlphanumeric(5);
			assertTrue(RestManager.login(Config.ORIGIN_1_IP));

			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() ->
			{
				AppSettings settingsLocal = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
				return settingsLocal != null;
			});


			AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE);
			settings.setDataChannelPlayerDistribution(AppSettings.DATA_CHANNEL_PLAYER_TO_ALL);
			assertTrue(RestManager.changeSettings(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, settings));


			String participant1StreamId = "participant1StreamId";
			String participant2StreamId = "participant2StreamId";
			String participant3StreamId = "playOnlyParticipant1StreamId";
			String participant4StreamId = "playOnlyParticipant2StreamId";
			String participant5StreamId = "playOnlyParticipant3StreamId";


			String participant1WindowHandle = chromeDriver.getWindowHandle();

			FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId,  participant1StreamId, null);

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {

				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
				return broadcast != null && broadcast.getSubTrackStreamIds().contains(participant1StreamId);
			});
			
			chromeDriver.switchTo().newWindow(WindowType.WINDOW);
			String participant2WindowHandle = chromeDriver.getWindowHandle();
			
			FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, roomId,  participant2StreamId, null);

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {

				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
				return broadcast != null && broadcast.getSubTrackStreamIds().contains(participant2StreamId);
			});
			
			
			chromeDriver.switchTo().newWindow(WindowType.WINDOW);
			String participant3WindowHandle = chromeDriver.getWindowHandle();

			FrontEndUtils.joinMultiTrackConference(chromeDriver, Config.EDGE_1_IP, Constants.WEBRTCAPPEE, roomId, participant3StreamId, "playOnly=true");

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
				return broadcast != null && broadcast.getWebRTCViewerCount() == 3;
			});

			chromeDriver.switchTo().newWindow(WindowType.WINDOW);
			String participant4WindowHandle = chromeDriver.getWindowHandle();

			FrontEndUtils.joinMultiTrackConference(chromeDriver,  Config.EDGE_2_IP, Constants.WEBRTCAPPEE, roomId, participant4StreamId, "playOnly=true");

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
				return broadcast != null && broadcast.getWebRTCViewerCount() == 4;
			});
			
			chromeDriver.switchTo().newWindow(WindowType.WINDOW);
			String participant5WindowHandle = chromeDriver.getWindowHandle();

			FrontEndUtils.joinMultiTrackConference(chromeDriver,  Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, roomId, participant5StreamId, "playOnly=true");

			Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
				return broadcast != null && broadcast.getWebRTCViewerCount() == 5;
			});

			//send a single message from 1st participant and check its received by 2nd and 3rd participant but not 1st participant
			String participant1Message = "from1";
			String participant2Message = "from2";
			String participant3Message = "from3";
			String participant4Message = "from4";
			String participant5Message = "from5";

			FrontEndUtils.sendConferenceMessage(chromeDriver, participant1WindowHandle, participant1Message);


			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				boolean participant1Sent = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant1WindowHandle, participant1Message, true);
				boolean participant2Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant2WindowHandle, participant1Message, true);
				boolean participant3Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant3WindowHandle, participant1Message, true);
				boolean participant4Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant4WindowHandle, participant1Message, true);
				boolean participant5Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant5WindowHandle, participant1Message, true);
				
				System.out.println("part1 - part1:"+participant1Sent+" part2:"+participant2Received+" part3:"+participant3Received+" part4:"+participant4Received+" part5:"+participant5Received);
				return participant2Received && participant3Received && participant4Received && participant5Received && participant1Sent;
			});

			FrontEndUtils.sendConferenceMessage(chromeDriver, participant2WindowHandle, participant2Message);

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				boolean participant2Sent = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant2WindowHandle, participant2Message, true);
				boolean participant1Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant1WindowHandle, participant2Message, true);
				boolean participant3Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant3WindowHandle, participant2Message, true);
				boolean participant4Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant4WindowHandle, participant2Message, true);
				boolean participant5Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant5WindowHandle, participant2Message, true);

				System.out.println("part2 - part1:"+participant1Received+" part2:"+participant2Sent+" part3:"+participant3Received+" part4:"+participant4Received+" part5:"+participant5Received);
				return participant1Received && participant3Received && participant4Received && participant5Received && participant2Sent;
			});

			FrontEndUtils.sendConferenceMessage(chromeDriver, participant3WindowHandle, participant3Message);

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				boolean participant3Sent = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant3WindowHandle, participant3Message, false);
				boolean participant1Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant1WindowHandle, participant3Message, true);
				boolean participant2Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant2WindowHandle, participant3Message, true);
				boolean participant4Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant4WindowHandle, participant3Message, true);
				boolean participant5Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant5WindowHandle, participant3Message, true);

				System.out.println("part3 - part1:"+participant1Received+" part2:"+participant2Received+" part3:"+participant3Sent+" part4:"+participant4Received+" part5:"+participant5Received);
				return participant1Received && participant2Received && participant4Received && participant5Received && participant3Sent;
			});
			
			FrontEndUtils.sendConferenceMessage(chromeDriver, participant4WindowHandle, participant4Message);

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				boolean participant4Sent = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant4WindowHandle, participant4Message, false);
				boolean participant1Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant1WindowHandle, participant4Message, true);
				boolean participant2Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant2WindowHandle, participant4Message, true);
				boolean participant3Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant3WindowHandle, participant4Message, true);
				boolean participant5Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant5WindowHandle, participant4Message, true);

				System.out.println("part4 - part1:"+participant1Received+" part2:"+participant2Received+" part3:"+participant3Received+" part4:"+participant4Sent+" part5:"+participant5Received);
				return participant1Received && participant2Received && participant3Received && participant5Received && participant4Sent;
			});
			
			FrontEndUtils.sendConferenceMessage(chromeDriver, participant5WindowHandle, participant5Message);

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(() -> {
				boolean participant5Sent = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant5WindowHandle, participant5Message, false);
				boolean participant1Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant1WindowHandle, participant5Message, false);
				boolean participant2Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant2WindowHandle, participant5Message, true);
				boolean participant3Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant3WindowHandle, participant5Message, true);
				boolean participant4Received = FrontEndUtils.isConferenceMessageFound(chromeDriver, participant4WindowHandle, participant5Message, true);

				System.out.println("part5 - part1:"+participant1Received+" part2:"+participant2Received+" part3:"+participant3Received+" part4:"+participant4Received+" part5:"+participant5Sent);
				return participant1Received && participant2Received && participant3Received && participant4Received && participant5Sent;
			});

			chromeDriver.switchTo().window(participant1WindowHandle);
			chromeDriver.close();


			chromeDriver.switchTo().window(participant2WindowHandle);
			chromeDriver.close();

			chromeDriver.switchTo().window(participant3WindowHandle);
			chromeDriver.close();

			chromeDriver.switchTo().window(participant4WindowHandle);
			chromeDriver.close();
			
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
					until(() -> {
						Broadcast broadcast = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, roomId);
						return  (broadcast == null);
					});


		} catch (Exception e){
			e.printStackTrace();
			fail();
		}


	}
}
