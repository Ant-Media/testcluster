package io.antmedia.test.cluster;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.awaitility.Awaitility;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.Config;
import io.antmedia.tools.RTMPPublisher;
import io.antmedia.utils.Constants;
import io.antmedia.utils.DockerManager;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;


/* We comment out this test. If you run it you must change
 * makecluster.sh to adapt mongo replica then build ams docker image.
 */

public class MongoReplicaTest{

	private static Logger log = LoggerFactory.getLogger(MongoReplicaTest.class);

	
	static DockerManager dm = new DockerManager();
	static String newtwork;
	
	//@BeforeClass
	public static void setUpClass() {
		assertNotNull(dm);

		//we clear old containers
		dm.stopAndRemoveContainer(Config.MONGO_NAME+"1");
		dm.stopAndRemoveContainer(Config.MONGO_NAME+"2");
		dm.stopAndRemoveContainer(Config.MONGO_NAME+"3");
		dm.clearCluster();
	}

	//@AfterClass
	public static void tearDownClass() {
		log.info("Tear Down:");
		dm.stopAndRemoveContainer(Config.MONGO_NAME+"1");
		dm.stopAndRemoveContainer(Config.MONGO_NAME+"2");
		dm.stopAndRemoveContainer(Config.MONGO_NAME+"3");
		dm.clearCluster();
	}
	
	@Rule
	public TestRule watcher = new TestWatcher() {
		protected void starting(Description description) {
			log.info("\n**************************************\n"
					+ "Starting test: " + description.getDisplayName()
					+"\n**************************************\n");
		}

		protected void failed(Throwable e, Description description) {
			log.info("\n**************************************\n"
					+ "Failed test: " + description.getDisplayName() + " e: " + ExceptionUtils.getStackTrace(e)
					+"\n**************************************\n");
		}

		protected void finished(Description description) {
			log.info("\n**************************************\n"
					+ "Finishing test: " + description.getDisplayName()
					+"\n**************************************\n");
		}
	};

	//@Test
	public void testReplica()  {
		assertTrue(dm.createNetwork());
		assertTrue(dm.createMongoReplicaSet());
			
		assertTrue(dm.createAntMediaContainer(Config.ORIGIN_1_IP, Config.ORIGIN_1_NAME, Config.MONGO_1_IP, true));
		
		Awaitility.await().atMost(150, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS)
		.until(() -> RestManager.signUp(Config.ORIGIN_1_IP));
		
		assertTrue(RestManager.login(Config.ORIGIN_1_IP));
		
		RTMPPublisher rtmpPublisher1 = new RTMPPublisher(Config.ORIGIN_1_IP, "/usr/local/test/test.mp4", Constants.WEBRTCAPPEE, "test1", new File("/dev/null"), 1);
		rtmpPublisher1.start();
		
		assertNotNull(RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, "test1"));
		
		dm.stopAndRemoveContainer(Config.MONGO_NAME+"1");
		
		Utils.mustSleep(10000); //wait for replication primary election
		
		assertNotNull(RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, "test1"));
		RTMPPublisher rtmpPublisher2 = new RTMPPublisher(Config.ORIGIN_1_IP, "/usr/local/test/test.mp4", Constants.WEBRTCAPPEE, "test2", new File("/dev/null"), 2);
		rtmpPublisher2.start();
		assertNotNull(RestManager.getBroadcast(Config.ORIGIN_1_IP, Constants.WEBRTCAPPEE, "test2"));
		
		dm.stopAndRemoveContainer(Config.ORIGIN_1_NAME);
		dm.stopAndRemoveContainer(Config.MONGO_NAME+"2");
		dm.stopAndRemoveContainer(Config.MONGO_NAME+"3");
	}

}
