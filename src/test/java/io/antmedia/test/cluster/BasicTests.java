package io.antmedia.test.cluster;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.cluster.ClusterNode;
import io.antmedia.test.SetUpTest;
import io.antmedia.utils.Constants;
import io.antmedia.utils.DBManager;
import io.antmedia.utils.RestManager;

public class BasicTests extends SetUpTest{

	private static Logger log = LoggerFactory.getLogger(BasicTests.class);

	@BeforeClass
	public static void setUpClass() {
		log.info("Set Up:");
		setUpCluster();
	}
	
	
	@After
	public void after() {
		
		//let logged in to nginx beccause the container may be killed and it effects other tests
		assertTrue(RestManager.login(Config.NGINX_IP));
	}
	
	
	int newStartedNodeCount = 0;

	@Test
	public void testScale()  {


		assertTrue(RestManager.login(Config.EDGE_1_IP));
		

		List<ClusterNode> nodesFromDB = DBManager.getClusterNodes();
		int nodeSize = nodesFromDB.size();	
		log.info("Number of nodes while starting in the cluster: " + nodeSize);

		for (int i = 0; i < nodeSize; i++) {
			log.info("Node[{}] in db ip:{} last update time:{}", i, nodesFromDB.get(i).getIp(), nodesFromDB.get(i).getLastUpdateTime());
		}


		List<ClusterNode> nodesFromRest = RestManager.getClusterNodesFrom(Config.EDGE_1_IP);
		assertEquals(nodeSize, nodesFromRest.size());


		newStartedNodeCount = 0;
		//add a couple of more container
		if (!dm.isContainerRunningImmediate(Config.EDGE_3_NAME)) {
			assertTrue(dm.createAntMediaContainer(Config.EDGE_3_IP, Config.EDGE_3_NAME, Config.MONGO_1_IP, true));
			newStartedNodeCount++;
		}
		
		if (!dm.isContainerRunningImmediate(Config.EDGE_4_NAME)) {
			assertTrue(dm.createAntMediaContainer(Config.EDGE_4_IP, Config.EDGE_4_NAME, Config.MONGO_1_IP, true));
			newStartedNodeCount++;
		}
		
		assertTrue(newStartedNodeCount > 0);
		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			int size = RestManager.getClusterNodesFrom(Config.EDGE_1_IP).size();
			log.info("Number of nodes in the cluster: " + size + " initial node size: " + nodeSize + " new startedNodeCount: " + newStartedNodeCount);
			return size == nodeSize + newStartedNodeCount;
		});
		
		log.info("origin is ready");


		nodesFromDB = DBManager.getClusterNodes();
		assertEquals(nodeSize + newStartedNodeCount, nodesFromDB.size());

		nodesFromRest = RestManager.getClusterNodesFrom(Config.EDGE_1_IP);
		assertEquals(nodeSize + newStartedNodeCount, nodesFromRest.size());

		//close the origin server
		dm.stopAndRemoveContainer(Config.EDGE_3_NAME);
		assertTrue(!dm.isContainerRunning(Config.EDGE_3_NAME));

		//wait until edge 5 becomes dead in db
		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).
		until(() -> {
			List<ClusterNode> nodes = RestManager.getClusterNodesFrom(Config.EDGE_1_IP);
			return nodeSize + newStartedNodeCount - 1 == nodes.size(); 
			
		});
				
		
		dm.stopAndRemoveContainer(Config.EDGE_4_NAME);
		assertTrue(!dm.isContainerRunning(Config.EDGE_4_NAME));
		
		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).
		until(() -> {
			List<ClusterNode> nodes = RestManager.getClusterNodesFrom(Config.EDGE_1_IP);
			return nodeSize + newStartedNodeCount - 2 == nodes.size(); 
		});
		
		dm.stopAndRemoveContainer(Config.EDGE_3_NAME);
		dm.stopAndRemoveContainer(Config.EDGE_4_NAME);
		
		//wait 40 seconds for synch because after 30 secs, it's removed check the ClusterStore class
		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(() -> {
			
			var clusterNodesLocal = DBManager.getClusterNodes();
			log.info("Number of nodes in the cluster: " + clusterNodesLocal.size() + " it should be: " + nodeSize);
			for (int i = 0; i < clusterNodesLocal.size(); i++) {
				log.info("Node[{}] in db ip:{} last update time:{}", i, clusterNodesLocal.get(i).getIp(), clusterNodesLocal.get(i).getLastUpdateTime());
			}
			
			return  clusterNodesLocal.size() == nodeSize;
		});
		
		
		

	}
	
	@Test
	public void testLiveAppSettings()  {
		testSettings(Constants.LIVEAPP);
	}

	private void testSettings(String appName) {
		
		assertTrue(RestManager.login(Config.ORIGIN_1_IP));

		CookieStore cookieStore = new BasicCookieStore();
		HttpContext customContext = new BasicHttpContext();
		customContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
		assertTrue(RestManager.login(Config.EDGE_1_IP, customContext));
		
		
		AppSettings originSettings = RestManager.getSettings(Config.ORIGIN_1_IP, appName);
		assertFalse(originSettings.isMp4MuxingEnabled());
		
		AppSettings edgeSettings = RestManager.getSettings(Config.EDGE_1_IP, appName, customContext);
		assertFalse(edgeSettings.isMp4MuxingEnabled());
		
		originSettings.setMp4MuxingEnabled(true);
		
		assertTrue(RestManager.changeSettings(Config.ORIGIN_1_IP, appName, originSettings));
		
		originSettings = RestManager.getSettings(Config.ORIGIN_1_IP, appName);
		assertTrue(originSettings.isMp4MuxingEnabled());
		
		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(
				() -> {
					AppSettings settings = RestManager.getSettings(Config.EDGE_1_IP, appName, customContext);
					return settings.isMp4MuxingEnabled();
				});
		
		originSettings.setMp4MuxingEnabled(false);
		//set the edge
		assertTrue(RestManager.changeSettings(Config.EDGE_1_IP, appName, originSettings, customContext));
		edgeSettings = RestManager.getSettings(Config.EDGE_1_IP, appName, customContext);
		assertFalse(edgeSettings.isMp4MuxingEnabled());
		
		//check the settings from origin
		Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(() -> {
			AppSettings settings = RestManager.getSettings(Config.ORIGIN_1_IP, appName);
			return !settings.isMp4MuxingEnabled();
		});
		
		
	}
}
