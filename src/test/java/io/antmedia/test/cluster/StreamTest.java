package io.antmedia.test.cluster;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


import java.util.List;
import java.util.concurrent.TimeUnit;

import io.antmedia.AntMediaApplicationAdapter;
import io.antmedia.test.utils.OnvifCameraEmulatorUtils;
import io.antmedia.tools.HLSPlayer;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.awaitility.Awaitility;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.EncoderSettings;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.rest.model.Result;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;

public class StreamTest extends SetUpTest {

	private static String globalStreamId = "streamtest";

	private static Logger log = LoggerFactory.getLogger(StreamTest.class);


	@BeforeClass
	public static void setUp() {
		setUpCluster();
	}

	private ChromeDriver chromeDriver;


	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();

	}

	@After
	public void after() {
		chromeDriver.quit();
	}


	@Test
	public void testStreamAutoStartStop() {
		try {
			assertTrue(RestManager.login(Config.NGINX_IP));
			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(() -> {
				AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);

				return settings != null;
			});
			AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);

			assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));
			OnvifCameraEmulatorUtils.startCameraEmulator();

			String appName = Constants.WEBRTCAPPEE;

			String streamId = "rtspTestStream" + (int) (Math.random() * 100000);

			String streamSourceUrl = "rtsp://" + Config.HOST_MACHINE_IP + ":6554/test.flv";
			Broadcast broadcast = new Broadcast("rtsp_source", null, null, null, streamSourceUrl,
					AntMediaApplicationAdapter.STREAM_SOURCE);
			broadcast.setStreamId(streamId);

			//Add stream source
			Result addStreamRes = RestManager.addStreamSource(broadcast, Config.NGINX_IP, appName, true);
			assertTrue(addStreamRes.isSuccess());

			//check that it's broadcasting
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(
					() -> {
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return AntMediaApplicationAdapter.BROADCAST_STATUS_BROADCASTING.equals(broadcastRetrieved.getStatus());
					});
			int timeElapseToStopAfterNoViewer = 5; //seconds

			//Enable auto start
			Broadcast broadcastUpdate = new Broadcast();
			broadcastUpdate.setAutoStartStopEnabled(true);
			Result setAutoStartStopRes = RestManager.updateBroadcast(Config.NGINX_IP, appName, streamId, broadcastUpdate, false, null);

			assertTrue(setAutoStartStopRes.isSuccess());

			//check that it should be finished because there is no viewer
			Awaitility.await().atMost(timeElapseToStopAfterNoViewer + 15, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).until(
					() -> {
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return broadcastRetrieved != null
								&& AntMediaApplicationAdapter.BROADCAST_STATUS_FINISHED.equals(broadcastRetrieved.getStatus());
					});

			//try to play it
			FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_1_IP, appName, streamId, "webrtc", null, false);

			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(
					() -> {
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return AntMediaApplicationAdapter.BROADCAST_STATUS_BROADCASTING.equals(broadcastRetrieved.getStatus());
					});
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);

			chromeDriver.close();

			//it should return finished again because driver is quit
			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(
					() -> {
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return broadcastRetrieved != null
								&& AntMediaApplicationAdapter.BROADCAST_STATUS_FINISHED.equals(broadcastRetrieved.getStatus());
					});

			ChromeDriver player2Driver = FrontEndUtils.getChromeDriver();

			FrontEndUtils.playStreamWithBrowser(player2Driver, Config.EDGE_2_IP, appName, streamId, "hls", null, false);

			//Check that it's started again
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(
					() -> {
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return AntMediaApplicationAdapter.BROADCAST_STATUS_BROADCASTING.equals(broadcastRetrieved.getStatus());
					});

			player2Driver.quit();

			//Check that it should return finished again
			Awaitility.await().atMost(35, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(
					() -> {
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return broadcastRetrieved != null
								&& AntMediaApplicationAdapter.BROADCAST_STATUS_FINISHED.equals(broadcastRetrieved.getStatus());
					});


			assertTrue(RestManager.changeSettings(Config.NGINX_IP, appName, settings));

			OnvifCameraEmulatorUtils.stopCameraEmulator();

		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e));

			LogEntries entry = chromeDriver.manage().logs().get(LogType.BROWSER);
			List<LogEntry> logs = entry.getAll();
			// Print one by one
			for (LogEntry logContent : logs) {
				log.info(logContent.toString());
			}
			fail(e.getMessage());
		}

	}


	/**
	 * Tests the correct handling of auto-start stream source transcoding if auto start is triggered through edge node.
	 * Architectural Context:
	 * - Origin nodes: High-performance servers dedicated to transcoding operations
	 * - Edge nodes: Lower-capacity servers optimized for stream playback only
	 *
	 * Problem Being Tested:
	 * Previously, when a user requested an offline stream playback on a auto start stop enabled broadcast, edge nodes would:
	 * 1. Receive the play request
	 * 2. Start fetching the stream directly since auto start/stop on viewer is enabled.
	 * 3. Perform transcoding locally
	 * This was problematic as edge nodes aren't designed for transcoding operations.
	 *
	 * Expected Behavior:
	 * After the fix, the flow is:
	 * 1. Edge node receives play request
	 * 2. Edge forwards request to the broadcast's origin node (determined by reading the broadcast's originAdress field)
	 * 3. If origin is up it fetches and transcodes the stream
	 * 	  If origin is down edge tries 3 times and if origin does not reply edge fetches the stream as last resort.

	 * Verification Method:
	 * Verify origin has the mp4 temp file (means it is the owner of the stream thus does transcoding), edge does not have the mp4 temp file.
	 * Verify if origin does not reply edge fetches the stream and has the mp4 temp file.
	 */
	@Test
	public void testStreamAutoStartStopEdgeForwardToOrigin() {

		try {
			assertTrue(RestManager.login(Config.NGINX_IP));
			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(() -> {
				AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);

				return settings != null;
			});
			AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
			settings.setEncoderSettings(List.of(new EncoderSettings(360, 400000, 64000, true)));

			assertTrue(RestManager.changeSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE, settings));
			OnvifCameraEmulatorUtils.startCameraEmulator();

			String appName = Constants.WEBRTCAPPEE;

			String streamId = "rtspTestStream" + (int) (Math.random() * 100000);

			String streamSourceUrl = "rtsp://" + Config.HOST_MACHINE_IP + ":6554/test.flv";

			Broadcast broadcast = new Broadcast("rtsp_source", null, null, null, streamSourceUrl,
					AntMediaApplicationAdapter.STREAM_SOURCE);
			broadcast.setStreamId(streamId);

			assertTrue(RestManager.login(Config.ORIGIN_1_IP));

			Result addStreamRes = RestManager.addStreamSource(broadcast, Config.ORIGIN_1_IP, appName, true);
			assertTrue(addStreamRes.isSuccess());

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(
					() -> {
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return AntMediaApplicationAdapter.BROADCAST_STATUS_BROADCASTING.equals(broadcastRetrieved.getStatus());
					});

			Broadcast broadcastUpdate = new Broadcast();
			broadcastUpdate.setAutoStartStopEnabled(true);
			Result setAutoStartStopRes = RestManager.updateBroadcast(Config.ORIGIN_1_IP, appName, streamId, broadcastUpdate, false, null);

			assertTrue(setAutoStartStopRes.isSuccess());

			Awaitility.await().atMost(35, TimeUnit.SECONDS).pollInterval(10, TimeUnit.SECONDS).until(
					() -> {
						log.info("Checking if broadcast is finished for streamId:{}", streamId);
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return broadcastRetrieved != null
								&& AntMediaApplicationAdapter.BROADCAST_STATUS_FINISHED.equals(broadcastRetrieved.getStatus());
					});


			//enable mp4 muxing here to verify mp4 recording will be only created on owner of the stream, origin node.
			settings.setMp4MuxingEnabled(true);
			assertTrue(RestManager.changeSettings(Config.NGINX_IP, appName, settings));

			//try to play it from edge. edge should redirect request to origin and origin should fetch the stream, thus handle the transcoding.
			FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_1_IP, appName, streamId, "webrtc", null, false);

			Awaitility.await().atMost(16, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS).until(
					() -> {
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return AntMediaApplicationAdapter.BROADCAST_STATUS_BROADCASTING.equals(broadcastRetrieved.getStatus());
					});

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> 
			{ 
				Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId);
				if (broadcastRetrieved != null) {
					log.info("WebRTC viewer count: {} - origin address:{}", broadcastRetrieved.getWebRTCViewerCount(), broadcastRetrieved.getOriginAdress());

					//even though play request is sent to edge, origin should fetch and transcode the stream, so origin must be in the address.

					return broadcastRetrieved.getWebRTCViewerCount() == 1 && Config.ORIGIN_1_IP.equals(broadcastRetrieved.getOriginAdress());

				}
				return false;
			});


			chromeDriver.close();

			//it should return finished again because driver is quit
			Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(5, TimeUnit.SECONDS).until(
					() -> {
						Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, appName, streamId);
						return broadcastRetrieved != null
								&& AntMediaApplicationAdapter.BROADCAST_STATUS_FINISHED.equals(broadcastRetrieved.getStatus());
					});

			//simulate origin down scenario. edge should handle if it cannot reach origin after 3 attempts.
			broadcastUpdate.setOriginAdress("123.1.1.1");

			setAutoStartStopRes = RestManager.updateBroadcast(Config.NGINX_IP, appName, streamId, broadcastUpdate, false, null);

			assertTrue(setAutoStartStopRes.isSuccess());

			chromeDriver = FrontEndUtils.getChromeDriver();

			FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.EDGE_1_IP, appName, streamId, "webrtc", null, false);

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(5, TimeUnit.SECONDS)
			.until(() -> {
				Broadcast broadcastRetrieved = RestManager.getBroadcast(Config.NGINX_IP, Constants.WEBRTCAPPEE, streamId);
				if (broadcastRetrieved != null) {
					log.info("WebRTC viewer count: {} - origin address:{}", broadcastRetrieved.getWebRTCViewerCount(), broadcastRetrieved.getOriginAdress());

					//even though play request is sent to edge, origin should fetch and transcode the stream, so origin must be in the address.

					return Config.EDGE_1_IP.equals(broadcastRetrieved.getOriginAdress());

				}
				return false;

			});

			settings.setMp4MuxingEnabled(false);
			settings.setEncoderSettings(null);

			assertTrue(RestManager.changeSettings(Config.NGINX_IP, appName, settings));

			OnvifCameraEmulatorUtils.stopCameraEmulator();

		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e));
			LogEntries entry = chromeDriver.manage().logs().get(LogType.BROWSER);
			List<LogEntry> logs = entry.getAll();
			for (LogEntry logContent : logs) {
				log.info(logContent.toString());
			}
			OnvifCameraEmulatorUtils.stopCameraEmulator();
			fail();
		}

	}

}