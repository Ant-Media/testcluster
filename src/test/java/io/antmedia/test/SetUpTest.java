package io.antmedia.test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.awaitility.Awaitility;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.cluster.ClusterNode;
import io.antmedia.utils.Constants;
import io.antmedia.utils.DockerManager;
import io.antmedia.utils.RestManager;
import io.github.bonigarcia.wdm.WebDriverManager;

public abstract class SetUpTest {

	public static File testFolder = new File(Config.TEST_LOG_DIRECTORY + "/" + System.currentTimeMillis());
	protected static DockerManager dm = new DockerManager();
	private static boolean clusterReady = false;
	private static boolean standaloneReady = false;
	private static boolean networkReady = false;

	private static Logger log = LoggerFactory.getLogger(SetUpTest.class);
	private static boolean standaloneReady2 = false;

	@Rule
	public TestRule watcher = new TestWatcher() {
		protected void starting(Description description) {
			log.info("\n**************************************\n"
					+ "Starting test: " + description.getDisplayName()
					+"\n**************************************\n");
		}

		protected void failed(Throwable e, Description description) {
			log.info("\n**************************************\n"
					+ "Failed test: " + description.getDisplayName() + " e: " + ExceptionUtils.getStackTrace(e)
					+"\n**************************************\n");
			collectLogs();
		}



		protected void finished(Description description) {
			log.info("\n**************************************\n"
					+ "Finishing test: " + description.getDisplayName()
					+"\n**************************************\n");
		}
	};

	public String getStreamAppWar(String containerName) 
	{

		String fileNameCommand = "docker exec " + containerName +" bash -c 'ls /usr/local/antmedia/StreamApp*.war'"; 
		Process execCommand = DockerManager.execCommand(fileNameCommand);

		InputStream inputStream = execCommand.getInputStream();
		StringBuilder buffer = new StringBuilder();
		byte[] data = new byte[1024];
		int length = 0;
		try {
			while ((length = inputStream.read(data, 0, data.length)) > 0) {
				buffer.append(new String(data, 0, length));
			}

			//strip trailing for the end of line
			String result = buffer.toString().stripTrailing();
			log.info("StreamApp full path is " + result);

			String warFile = "StreamApp.war";
			String command = "docker cp " + containerName + ":" + result +  " " + warFile;


			if (DockerManager.execCommand(command).waitFor() == 0)  {
				return warFile;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}


	public void collectLogs() {

		log.info("Collecting container logs");
		String[] containers = new String[] {
				Config.EDGE_1_NAME, 
				Config.EDGE_2_NAME,
				Config.EDGE_3_NAME,
				Config.EDGE_4_NAME,
				Config.ORIGIN_1_NAME,
				Config.ORIGIN_2_NAME,
				Config.ORIGIN_3_NAME,
				Config.STANDALONE};

		File f = new File("../container-logs");
		f.mkdir();
		for (String containerName : containers) 
		{
			if (DockerManager.isContainerRunningImmediate(containerName)) {

				log.info("Getting log from " + containerName);
				File containerFolder = new File(f, containerName);
				containerFolder.mkdir();
				String command = "docker cp " + containerName + ":/var/log/antmedia/  " + containerFolder.getAbsolutePath() + "/";
				Process execCommand = DockerManager.execCommand(command);

				byte[] data = new byte[1024];
				InputStream errorStream = execCommand.getErrorStream();
				int length = 0;
				try {
					while ( (length = errorStream.read(data, 0, data.length)) > 0) {
						log.info(new String(data, 0, length));
					}

					int exit = execCommand.waitFor();
					log.info("{} command exited with :{}", command, exit);
					
					
					//copy app settings
					{
						command = "docker cp " + containerName + ":/usr/local/antmedia/webapps/"+ Constants.LIVEAPP + "/WEB-INF/red5-web.properties  " + containerFolder.getAbsolutePath() + "/"+ Constants.LIVEAPP +"-red5-web.properties";
						execCommand = DockerManager.execCommand(command);
						exit = execCommand.waitFor();
						log.info("{} command exited with :{}", command, exit);
					}
					
					{
						command = "docker cp " + containerName + ":/usr/local/antmedia/webapps/"+ Constants.WEBRTCAPPEE + "/WEB-INF/red5-web.properties  " + containerFolder.getAbsolutePath() + "/"+ Constants.WEBRTCAPPEE +"-red5-web.properties";
						execCommand = DockerManager.execCommand(command);
						exit = execCommand.waitFor();
						log.info("{} command exited with :{}", command, exit);
					}
					
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				catch (IOException e1) {
					e1.printStackTrace();
				}

			}
			else {
				log.info("Container:"+ containerName +" is not running so log files are not copied");
			}

		}

	}

	@Before 
	public void testBefore() {
		testCheckCrash();
	}

	public static void testCheckCrash() {
		log.info("Checking if crash exists");
		String command = "bash -c  'ls /usr/local/antmedia/hs*.log &> /dev/null;'";

		String[] clusterImages = new String[] {
				Config.EDGE_1_NAME, 
				Config.EDGE_2_NAME,
				Config.EDGE_3_NAME,
				Config.EDGE_4_NAME,
				Config.ORIGIN_1_NAME };

		if (clusterReady) {
			for (String containerName : clusterImages) {
				//the crash file should not exist
				assertFalse(dm.runCommand(containerName, command));
			}
		}


		if (standaloneReady) {
			//the crash file should not exist
			assertFalse(dm.runCommand(Config.STANDALONE, command));
		}

	}



	@AfterClass
	public static void afterClass() {
		testCheckCrash();
	}
	
	public static void resetClusterReady() {
		clusterReady = false;
	}

	public synchronized static void setUpCluster()  {

		WebDriverManager.chromedriver().setup();

		if (!clusterReady) {
			//we need to remove the components before removing network
			dm.clearCluster();
		}

		if (!networkReady) {
			dm.removeNetwork();
			dm.createNetwork();
			networkReady = true;
		}

		if (!clusterReady) 
		{
			long start = System.currentTimeMillis();
			log.info("Clearing the containers if exist");
			testFolder.mkdirs();
			log.info("setUpCluster");

			assertTrue(dm.createMongoContainer());
			log.info("mongo is ready");


			assertTrue(dm.createAntMediaContainer(Config.EDGE_1_IP, Config.EDGE_1_NAME, Config.MONGO_1_IP, false));
			assertTrue(dm.createAntMediaContainer(Config.EDGE_2_IP, Config.EDGE_2_NAME, Config.MONGO_1_IP, false));
			assertTrue(dm.createAntMediaContainer(Config.ORIGIN_1_IP, Config.ORIGIN_1_NAME, Config.MONGO_1_IP, false));
			
			
			assertTrue(dm.isContainerRunning(Config.EDGE_1_NAME));
			assertTrue(dm.isContainerRunning(Config.EDGE_2_NAME));
			assertTrue(dm.isContainerRunning(Config.ORIGIN_1_NAME));
			
			
			//wait until origin can become ready to signup 
			Awaitility.await().atMost(150, TimeUnit.SECONDS).pollDelay(10, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> { 
				log.info("Trying to signup for the web panel user");
				return RestManager.signUp(Config.ORIGIN_1_IP); 
			});
			log.info("User is signed up for the web panel");

			assertTrue(dm.createNginxContainer());
			log.info("nginx is ready");

			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(2, TimeUnit.SECONDS)
			.until(() -> {
				return RestManager.login(Config.NGINX_IP);
			});

			List<ClusterNode> nodesFromRest = RestManager.getClusterNodesFrom(Config.NGINX_IP);
			assertEquals(3, nodesFromRest.size());


			Awaitility.await().atMost(15, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(() -> {
				AppSettings settings = RestManager.getSettings(Config.NGINX_IP, Constants.WEBRTCAPPEE);
				return settings != null;
			});

			long clusterSetuptime = System.currentTimeMillis() - start;
			log.info("Cluster setup time: " + clusterSetuptime);
			clusterReady = true;
		}
		else {
			//containers may be killed in some test, start them if they are not running
			if (!dm.isContainerRunningImmediate(Config.ORIGIN_1_NAME)) {
				//make sure it's removed
				dm.stopAndRemoveContainer(Config.ORIGIN_1_NAME);
				assertTrue(dm.createAntMediaContainer(Config.ORIGIN_1_IP, Config.ORIGIN_1_NAME, Config.MONGO_1_IP, false));
			}

			if (!dm.isContainerRunningImmediate(Config.EDGE_1_NAME)) {
				//make sure it's removed
				dm.stopAndRemoveContainer(Config.EDGE_1_NAME);

				assertTrue(dm.createAntMediaContainer(Config.EDGE_1_IP, Config.EDGE_1_NAME, Config.MONGO_1_IP, false));
			}

			if (!dm.isContainerRunningImmediate(Config.EDGE_2_NAME)) {
				//make sure it's removed
				dm.stopAndRemoveContainer(Config.EDGE_2_NAME);

				assertTrue(dm.createAntMediaContainer(Config.EDGE_2_IP, Config.EDGE_2_NAME, Config.MONGO_1_IP, false));
			}
			
			assertTrue(dm.isContainerRunning(Config.EDGE_1_NAME));
			assertTrue(dm.isContainerRunning(Config.EDGE_2_NAME));
			assertTrue(dm.isContainerRunning(Config.ORIGIN_1_NAME));

		}

	}


	public synchronized static void setUpStandalone()  
	{
		WebDriverManager.chromedriver().setup();
		if (!networkReady) {
			dm.removeNetwork();
			dm.createNetwork();
			networkReady = true;
		}

		if (!standaloneReady) 
		{
			log.info("Clearing the containers if exist");
			testFolder.mkdirs();
			dm.clearStandalone();
			log.info("setUpStandalone");

			assertTrue(dm.createAntMediaStandaloneContainer(Config.STANDALONE_IP, Config.STANDALONE));
			Awaitility.await().atMost(150, TimeUnit.SECONDS).pollDelay(10, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> { 
				log.info("Trying to signup for the web panel user");
				return RestManager.signUp(Config.STANDALONE_IP); 
			});
			log.info("standalone is ready");
			assertTrue(RestManager.login(Config.STANDALONE_IP));
			standaloneReady = true;
		}
	}
	
	public synchronized static void setUpStandalone2()  
	{
		WebDriverManager.chromedriver().setup();
		if (!networkReady) {
			dm.removeNetwork();
			dm.createNetwork();
			networkReady = true;
		}

		if (!standaloneReady2 ) 
		{
			log.info("Clearing the containers if exist");
			testFolder.mkdirs();
			dm.clearStandalone2();
			log.info("setUpStandalone2");

			assertTrue(dm.createAntMediaStandaloneContainer(Config.STANDALONE_2_IP, Config.STANDALONE_2));
			Awaitility.await().atMost(150, TimeUnit.SECONDS).pollDelay(10, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> { 
				log.info("Trying to signup for the web panel user");
				return RestManager.signUp(Config.STANDALONE_2_IP); 
			});
			log.info("standalone is ready");
			assertTrue(RestManager.login(Config.STANDALONE_2_IP));
			standaloneReady2 = true;
		}
	}

}
