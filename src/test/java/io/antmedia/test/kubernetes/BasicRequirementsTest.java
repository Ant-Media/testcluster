package io.antmedia.test.kubernetes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AntMediaApplicationAdapter;
import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.test.cluster.WebRTCCodecTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.tools.RTMPPublisher;
import io.antmedia.utils.Constants;
import io.antmedia.utils.DockerManager;
import io.antmedia.utils.KubernetesManager;
import io.antmedia.utils.RestManager;

public class BasicRequirementsTest {
	
	private static Logger log = LoggerFactory.getLogger(BasicRequirementsTest.class);

	public static File testFolder = new File(Config.TEST_LOG_DIRECTORY + "/" + System.currentTimeMillis());
		
	public static String originIP = "127.0.0.1";
	
	private static KubernetesManager kubernetesManager = new KubernetesManager();
	
	@Rule
	public TestRule watcher = new TestWatcher() {
		protected void starting(Description description) {
			log.info("\n**************************************\n"
					+ "Starting test: " + description.getDisplayName()
					+"\n**************************************\n");
		}

		protected void failed(Throwable e, Description description) {
			log.info("\n**************************************\n"
					+ "Failed test: " + description.getDisplayName() + " e: " + ExceptionUtils.getStackTrace(e)
					+"\n**************************************\n");
		}

		protected void finished(Description description) {
			log.info("\n**************************************\n"
					+ "Finishing test: " + description.getDisplayName()
					+"\n**************************************\n");
		}
	};
	
	
	@BeforeClass
	public static void beforeClass() {
		testFolder.mkdirs();
		//clear mongodb cluster database
		DockerManager.execCommand("sudo mongo clusterdb --eval \"db.dropDatabase();\"");
		
		
		boolean createDeployment = kubernetesManager.createDeployment(new File("ams-k8s-mongodb.yaml"), "mongo");
		assertTrue(createDeployment);
		
		createDeployment = kubernetesManager.createDeployment(new File("ams-k8s-deployment.yaml"), "ant-media-server");
		assertTrue(createDeployment);
		
		Awaitility.await().atMost(150, TimeUnit.SECONDS).pollDelay(10, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			log.info("Trying to signup for the web panel user");
			return RestManager.signUp(originIP); 
		});

		assertTrue(RestManager.login(originIP));
	}
	
	@AfterClass
	public static void afterClass() {
		kubernetesManager.deleteDeployment("ant-media-server");
		
	}
	
	private ChromeDriver chromeDriver;
	
	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();

	}
	
	@After
	public void after() {
		chromeDriver.quit();

	}
	
	@Test
	public void testRTMPPublishWebRTCPlay() 
	{
		String streamId = "test" + (int)(Math.random()*10000);
		RTMPPublisher rtmpPublisher = new RTMPPublisher(originIP, "src/test/resources/Test_600kbps.mp4", Constants.WEBRTCAPPEE, streamId, testFolder, 1);

		rtmpPublisher.start();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			log.info("Getting stream from Load Balancer");
			Broadcast broadcast = RestManager.getBroadcast(originIP, Constants.WEBRTCAPPEE, streamId);
			if (broadcast != null) {
				return broadcast.isPublicStream(); 
			}
			return false;
		});
		
		FrontEndUtils.playStreamWithBrowser(chromeDriver, originIP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
				
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(originIP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		
		chromeDriver.close();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(originIP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 0);
		
		rtmpPublisher.stop();
		
	}
	
	
	@Test
	public void testWebRTCPublishWebRTCPlay() {
		String streamId = "test" + (int)(Math.random()*10000);
		
		
		FrontEndUtils.publishStream(chromeDriver, originIP, Constants.WEBRTCAPPEE, streamId);
		String publisherWindowHandle = chromeDriver.getWindowHandle();
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(originIP, Constants.WEBRTCAPPEE, streamId); 
			if (broadcast != null) {
				return AntMediaApplicationAdapter.BROADCAST_STATUS_BROADCASTING.equals(broadcast.getStatus());
			}
			return false;
		});
		
		assertEquals(0, RestManager.getBroadcast(originIP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount());
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);

		FrontEndUtils.playStreamWithBrowser(chromeDriver, originIP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(originIP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		chromeDriver.close();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(originIP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 0);
	
		chromeDriver.switchTo().window(publisherWindowHandle);
		chromeDriver.close();
		
	}
	
	

}
