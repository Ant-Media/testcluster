package io.antmedia.test.self;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import io.antmedia.utils.FileInfo;
import io.antmedia.utils.FileManager;

public class EnvironmentTest {
	
	@Test
	public void testVP8SourceFiles() {
		FileInfo info = FileManager.getFileInfo("src/test/resources/test.webm");
		assertTrue(info.isExist);
		assertEquals("vp8", info.videoCodec);
	}
	
	@Test
	public void testH264SourceFiles() {
		FileInfo info = FileManager.getFileInfo("src/test/resources/test.mp4");
		assertTrue(info.isExist);
		assertEquals("h264", info.videoCodec);
	}
	
	@Test
	public void testOpusSourceFiles() {
		FileInfo info = FileManager.getFileInfo("src/test/resources/audiotest.opus");
		assertTrue(info.isExist);
		assertEquals("opus", info.audioCodec);
	}
}
