package io.antmedia.test.noneprivatenetwork;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.cluster.ClusterNode;
import io.antmedia.test.cluster.PublishPlayTest;
import io.antmedia.test.kubernetes.BasicRequirementsTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.DockerManager;
import io.antmedia.utils.RestManager;

public class RestCommunicationTest {
	
	/**
	 * This class is just a start for none private networks
	 * We can make multilevel cluster tests here 
	 */
	
	protected static DockerManager dm = new DockerManager();
	private ChromeDriver chromeDriver;

	private static Logger log = LoggerFactory.getLogger(RestCommunicationTest.class);

	
	public void removeNonePrivateComponents() {

		//kill containers
		dm.execCommand("docker " + " kill " + Config.NONE_PRIVATE_MONGO_1_NAME + " " + Config.NONE_PRIVATE_ORIGIN_1_NAME + " " + Config.NONE_PRIVATE_ORIGIN_2_NAME + " "
				);

		//remove containers
		dm.execCommand("docker " + " rm " + Config.NONE_PRIVATE_MONGO_1_NAME + " " + Config.NONE_PRIVATE_ORIGIN_1_NAME + " " + Config.NONE_PRIVATE_ORIGIN_2_NAME + " " 

				);
		
		dm.removeNetwork(Config.NONE_PRIVATE_SUBNET_NAME);

	}
	
	@Before
	public void before() {
		chromeDriver = FrontEndUtils.getChromeDriver();

	}
	
	@After
	public void after() {
		chromeDriver.quit();

	}
	
	
	
	//THIS IS A BUG SCENARIO, forwarding the origin is not working in none private address
	@Test
	public void testRestProxyDirectlyForNonePrivateAddress() {

		
		//remove components if it exists to not encounter any problem
		removeNonePrivateComponents();
		
		//so we're creating a none private addrss
		dm.createNetwork(Config.NONE_PRIVATE_SUBNET, Config.NONE_PRIVATE_SUBNET_NAME);
		//create another mongodb 

		assertTrue(dm.createMongoContainer(Config.NONE_PRIVATE_MONGO_1_NAME, Config.NONE_PRIVATE_SUBNET_NAME, Config.NONE_PRIVATE_MONGO_1_IP));

		//create another origin 1
		assertTrue(dm.createAntMediaContainer(Config.NONE_PRIVATE_ORIGIN_1_IP, Config.NONE_PRIVATE_ORIGIN_1_NAME, Config.NONE_PRIVATE_MONGO_1_IP, Config.NONE_PRIVATE_SUBNET_NAME, true));

		//create another origin 2
		assertTrue(dm.createAntMediaContainer(Config.NONE_PRIVATE_ORIGIN_2_IP, Config.NONE_PRIVATE_ORIGIN_2_NAME, Config.NONE_PRIVATE_MONGO_1_IP, Config.NONE_PRIVATE_SUBNET_NAME, true));

		log.info("Will try to sign up for " + Config.NONE_PRIVATE_ORIGIN_2_NAME);
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			log.info("Trying to signup for the web panel user to " + Config.NONE_PRIVATE_ORIGIN_2_NAME);
			return RestManager.signUp(Config.NONE_PRIVATE_ORIGIN_2_IP); 
		});
		log.info("User is signed up for the web panel " +  Config.NONE_PRIVATE_ORIGIN_1_NAME);
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			log.info("Login to the web panel to " + Config.NONE_PRIVATE_ORIGIN_2_NAME);
			return RestManager.login(Config.NONE_PRIVATE_ORIGIN_2_IP); 
		});

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			List<ClusterNode> nodesFromRest = RestManager.getClusterNodesFrom(Config.NONE_PRIVATE_ORIGIN_2_IP);
			return nodesFromRest != null && nodesFromRest.size() == 2;
		});
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).until(() -> {
			AppSettings settings = RestManager.getSettings(Config.NONE_PRIVATE_ORIGIN_2_IP, Constants.WEBRTCAPPEE);
			return settings != null;
		});

		
		

		PublishPlayTest.testRestProxyDirectly(chromeDriver, Config.NONE_PRIVATE_ORIGIN_1_IP, Config.NONE_PRIVATE_ORIGIN_2_IP, Config.NONE_PRIVATE_SUBNET);


		removeNonePrivateComponents();
	}
	

}
