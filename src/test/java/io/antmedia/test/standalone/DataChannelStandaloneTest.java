package io.antmedia.test.standalone;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;

public class DataChannelStandaloneTest extends SetUpTest
{

	protected static Logger logger = LoggerFactory.getLogger(DataChannelStandaloneTest.class);


	private static String streamId = "TestWebrtcStreamDataChannelStandaloneTest";

	@Before
	public void setUp() {
		setUpStandalone();
		assertTrue(RestManager.login(Config.STANDALONE_IP));

		chromeDriver = FrontEndUtils.getChromeDriver();
	}

	@After
	public void after() {

		chromeDriver.quit();

	}

	private ChromeDriver chromeDriver;
	private String publishWindowHandle;
	private String play1WindowHandle;
	private String play2WindowHandle;


	public static void sendMessageForPublisher(ChromeDriver driver, String publisherMessage, String windowHandle) {
		driver.switchTo().window(windowHandle);
		driver.switchTo().frame(0);
		WebElement dataTextbox = driver.findElement(By.id("dataTextbox"));
		WebElement sendButton = driver.findElement(By.id("send"));

		dataTextbox.sendKeys(publisherMessage);
		sendButton.click();
	}

	public static void sendMessageForPlayer(ChromeDriver driver, String windowHandle, String message) {
		driver.switchTo().window(windowHandle);
		WebElement dataTextbox = driver.findElement(By.id("dataTextbox"));
		WebElement sendButton = driver.findElement(By.id("send"));

		dataTextbox.sendKeys(message);
		sendButton.click();
	}


	/**
	 * Pay attention that log buffers are reset after each call, meaning that available log entries
	 * correspond to those entries not yet returned for a given log type. In practice, this means that
	 * this call will return the available log entries since the last call, or from the start of the
	 * session.
	 * 
	 * This is why we call separately in awaitility
	 * 
	 * @param driver
	 * @param windowHandle
	 * @param message
	 * @return
	 */
	public static boolean isMessageFound(ChromeDriver driver, String windowHandle, String message, boolean publisher) {
		driver.switchTo().window(windowHandle);
		String value = null;

		if (publisher) {
			driver.switchTo().frame(0);
		}
		//all-messages are box in the index.html and player.html
		value = driver.findElement(By.id("all-messages")).getText();



		return value.contains(message);
	}

	@Test
	public void testSendDataAll() {

		String play1Message = "play1Message " + RandomStringUtils.randomAlphanumeric(50);
		String play2Message = "play2Message " + RandomStringUtils.randomAlphanumeric(50);

		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setDataChannelEnabled(true);
		settings.setDataChannelPlayerDistribution(AppSettings.DATA_CHANNEL_PLAYER_TO_ALL);

		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		startPeers();

		sendMessageForPlayer(chromeDriver, play1WindowHandle, play1Message);

		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return isMessageFound(chromeDriver, publishWindowHandle, play1Message, true);
		});

		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return isMessageFound(chromeDriver, play2WindowHandle, play1Message, false);
		});



		sendMessageForPlayer(chromeDriver, play2WindowHandle, play2Message);

		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return isMessageFound(chromeDriver, publishWindowHandle, play2Message, true);
		});

		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return isMessageFound(chromeDriver, play2WindowHandle, play2Message, false);
		});

		stopPeers();


	}	

	@Test
	public void testSendDataNone() {

		String play1Message = "play1Message " + RandomStringUtils.randomAlphanumeric(50);
		String play2Message = "play2Message " + RandomStringUtils.randomAlphanumeric(50);

		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setDataChannelEnabled(true);
		settings.setDataChannelPlayerDistribution(AppSettings.DATA_CHANNEL_PLAYER_TO_NONE);

		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		startPeers();

		sendMessageForPlayer(chromeDriver, play1WindowHandle, play1Message);

		Awaitility.await().atMost(7, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return !isMessageFound(chromeDriver, publishWindowHandle, play1Message, true);
		});

		Awaitility.await().atMost(7, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return !isMessageFound(chromeDriver, play2WindowHandle, play1Message, false);
		});

		sendMessageForPlayer(chromeDriver, play2WindowHandle, play2Message);

		Awaitility.await().atMost(7, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return !isMessageFound(chromeDriver, publishWindowHandle, play1Message, true);
		});

		Awaitility.await().atMost(7, TimeUnit.SECONDS).pollDelay(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return !isMessageFound(chromeDriver, play2WindowHandle, play1Message, false);
		});

		assertFalse(isMessageFound(chromeDriver, publishWindowHandle, play1Message, true));
		assertFalse(isMessageFound(chromeDriver, play2WindowHandle, play1Message, false));

		stopPeers();

	}

	@Test
	public void testSendDataPublisher() {

		String play1Message = "play1Message " + RandomStringUtils.randomAlphanumeric(50);

		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setDataChannelEnabled(true);
		settings.setDataChannelPlayerDistribution(AppSettings.DATA_CHANNEL_PLAYER_TO_PUBLISHER);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		startPeers();

		sendMessageForPlayer(chromeDriver, play1WindowHandle, play1Message);

		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return isMessageFound(chromeDriver, publishWindowHandle, play1Message, true);
		});

		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return !isMessageFound(chromeDriver, play2WindowHandle, play1Message, false);
		});

		stopPeers();
	}


	@Test
	public void testDataChannelStandalone()  {

		String publisherMessage = "publisherMessage " + RandomStringUtils.randomAlphanumeric(50);

		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		//Set the default codec settings
		settings.setH264Enabled(true);
		settings.setVp8Enabled(false);
		settings.setDataChannelEnabled(false);

		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));


		startPeers();

		settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		assertFalse(settings.isDataChannelEnabled());

		sendMessageForPublisher(chromeDriver, publisherMessage, publishWindowHandle);

		Utils.mustSleep(3000);

		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollDelay(3, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return !isMessageFound(chromeDriver, play1WindowHandle, publisherMessage, false);
		});

		stopPeers();


		settings.setDataChannelPlayerDistribution(AppSettings.DATA_CHANNEL_PLAYER_TO_ALL);
		settings.setDataChannelEnabled(true);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		startPeers();

		sendMessageForPublisher(chromeDriver, publisherMessage, publishWindowHandle);

		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return isMessageFound(chromeDriver, play1WindowHandle, publisherMessage, false);
		});

		Awaitility.await().atMost(5, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			return isMessageFound(chromeDriver, play2WindowHandle, publisherMessage, false); 
		});


		stopPeers();
	}

	private void startPeers() {

		FrontEndUtils.publishStream(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId);

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId); 
			if (broadcast != null) {
				return broadcast.isPublicStream();
			}
			return false;
		});

		publishWindowHandle = chromeDriver.getWindowHandle();

		chromeDriver.findElement(By.id("options")).click();


		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		play1WindowHandle = chromeDriver.getWindowHandle();


		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId);
			//due to unexpected cases, there may be some reconnection so check the broadcast is not null
			
			return broadcast != null &&  broadcast.getWebRTCViewerCount() == 1;

		});


		chromeDriver.findElement(By.id("options")).click();


		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		play2WindowHandle = chromeDriver.getWindowHandle();

		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> {
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId);
			//due to unexpected cases, there may be some reconnection so check the broadcast is not null

			return broadcast != null && broadcast.getWebRTCViewerCount() == 2;
		});
		chromeDriver.findElement(By.id("options")).click();




	}

	private void stopPeers() {

		//stop player 2
		chromeDriver.switchTo().window(play2WindowHandle);
		chromeDriver.close();

		//stop player 1
		chromeDriver.switchTo().window(play1WindowHandle);
		chromeDriver.close();

		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId);
			return broadcast != null && broadcast.getWebRTCViewerCount() == 0; 
		});

		//stop publisher
		chromeDriver.switchTo().window(publishWindowHandle);
		chromeDriver.switchTo().frame(0);
		chromeDriver.findElement(By.id("stop_publish_button")).click();

		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId); 
			return  (broadcast == null);
		});

	}

	@Test
	public void testEnableDataChannelAfterPeerConnection()  {
		startPeers();

		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setDataChannelEnabled(false);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));
		settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		assertFalse(settings.isDataChannelEnabled());

		settings.setDataChannelEnabled(true);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		stopPeers();
	}

}
