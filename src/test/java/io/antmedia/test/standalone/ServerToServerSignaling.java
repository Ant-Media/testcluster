package io.antmedia.test.standalone;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;

public class ServerToServerSignaling extends SetUpTest{

	private static String streamId = "ServerToServerSignaling";

	@BeforeClass
	public static void setUp() {
		setUpStandalone();
		
		
	}

	private ChromeDriver chromeDriver;
	
	@Before
	public void before() {
		assertTrue(RestManager.login(Config.STANDALONE_IP));
		chromeDriver = FrontEndUtils.getChromeDriver();
		

	}
	
	@After
	public void after() {
		chromeDriver.quit();

	}
	
	/**
	 * This is testing this case
	 * https://antmedia.io/publish-and-play-streams-behind-nat/
	 */
	@Test
	public void testServerToServerSignaling() {
		
		setUpStandalone2();
		
		//enable the standonlone_server for server to server signaling
		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setSignalingEnabled(true);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));
		
		//configure the second standalone server to register itself to the first standalone server
		assertTrue(RestManager.login(Config.STANDALONE_2_IP));
		settings = RestManager.getSettings(Config.STANDALONE_2_IP, Constants.WEBRTCAPPEE);
		settings.setSignalingAddress("ws://"+Config.STANDALONE_IP +":5080/" + Constants.WEBRTCAPPEE + "/websocket/signaling");
		assertTrue(RestManager.changeSettings(Config.STANDALONE_2_IP, Constants.WEBRTCAPPEE, settings));


		//publish the seecond server 
		FrontEndUtils.publishStream(chromeDriver, Config.STANDALONE_2_IP, Constants.WEBRTCAPPEE, streamId);
		
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		//it can be player in the standalone true signaling
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", "signaling=true", false);
		
		
		dm.clearStandalone2();
	}
	


}
