package io.antmedia.test.standalone;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.EncoderSettings;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;

public class WebRTCCodecStandaloneTest extends SetUpTest{

	private static String STREAM_ID_BASE = "TestWebrtcStreamWebRTCCodecStandaloneTest";

	@BeforeClass
	public static void setUp() {
		setUpStandalone();
	}

	private ChromeDriver chromeDriver;

	
	@Before
	public void before() {
		assertTrue(RestManager.login(Config.STANDALONE_IP));
		chromeDriver = FrontEndUtils.getChromeDriver();

	}
	
	@After
	public void after() {
		chromeDriver.quit();

	}
	
	@Test
	public void testH264WhenOnlyH264CodecEnabled() {
		 
		testCodec(true, false, "src/test/resources/test.mp4", "h264");
	}
	
	@Test
	public void testH264WhenH264AndVP8CodecEnabled() {
		testCodec(true, true, "src/test/resources/test.mp4", "h264");
	}

	@Test
	public void testVP8WhenOnlyVP8CodecEnabled() {
		testCodec(false, true, "src/test/resources/test.webm", "VP8");
	}
	
	@Test
	public void testVP8WhenH264AndVP8CodecEnabled() {
		testCodec(true, true, "src/test/resources/test.webm", "VP8");
	}
	
	public void testCodec(boolean h264Enabled, boolean VP8Enabled, String file, String codec)  {
		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setH264Enabled(h264Enabled);
		settings.setVp8Enabled(VP8Enabled);
		if(h264Enabled && VP8Enabled) {
			settings.setEncoderSettings(Arrays.asList(new EncoderSettings(240,500000,32000,true)));
		}
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));
		
		AppSettings settings2 = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		assertEquals(h264Enabled, settings2.isH264Enabled());
		assertEquals(VP8Enabled, settings2.isVp8Enabled());
		
		String player1Codec = "h264";
		String player2Codec = "h264";
		if(VP8Enabled && h264Enabled) {
			player1Codec = "h264";
			player2Codec = "VP8";
		}
		else if(h264Enabled) {
			player1Codec = "h264";
			player2Codec = "h264";
		}
		else if(VP8Enabled) {
			player1Codec = "VP8";
			player2Codec = "VP8";
		}
		FrontEndUtils.publishStream(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE);
		String publishWindowHandle = chromeDriver.getWindowHandle();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE); 
			if (broadcast != null) {
				return broadcast.isPublicStream();
			}
			return false;
		});
		
		
		assertEquals(0, RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE).getWebRTCViewerCount());
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player1WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE, "webrtc", null, false);

		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE).getWebRTCViewerCount() == 1);
		

		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player2WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE, "webrtc", null, false);

		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE).getWebRTCViewerCount() == 2);
	
		chromeDriver.close();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE).getWebRTCViewerCount() == 1);
		
		chromeDriver.switchTo().window(player1WindowHandle);
		chromeDriver.close();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE).getWebRTCViewerCount() == 0);
		
		chromeDriver.switchTo().window(publishWindowHandle);
		chromeDriver.close();
		
		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, STREAM_ID_BASE); 
			return  (broadcast == null);
		});
	}

}
