package io.antmedia.test.standalone;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;

public class ViewerLimitTest extends SetUpTest{

	private static String streamId = "TestWebrtcStreamViewerLimitTest";

	@BeforeClass
	public static void setUp() {
		setUpStandalone();
		
		
	}

	private ChromeDriver chromeDriver;
	
	@Before
	public void before() {
		assertTrue(RestManager.login(Config.STANDALONE_IP));
		chromeDriver = FrontEndUtils.getChromeDriver();

	}
	
	@After
	public void after() {
		chromeDriver.quit();

	}
	
	@Test
	public void testWebRTCViewerLimit()  {
		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		//Set the default codec settings
		settings.setH264Enabled(true);
		settings.setVp8Enabled(false);
		settings.setDataChannelEnabled(false);
		
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));
		
		FrontEndUtils.publishStream(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId);
		String publisherHandle = chromeDriver.getWindowHandle();
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId); 
			if (broadcast != null) {
				return broadcast.isPublicStream();
			}
			return false;
		});
		
		assertEquals(0, RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount());
		Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId);
		broadcast.setWebRTCViewerLimit(2);
		boolean result = RestManager.changeBroadcastProperties(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, broadcast, null);
		assertTrue(result);
		broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId);
		assertEquals(2, broadcast.getWebRTCViewerLimit());
		
		
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player1WindowHandle = chromeDriver.getWindowHandle();
		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player2WindowHandle = chromeDriver.getWindowHandle();

		FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
		
		
		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 2);
	
		
		chromeDriver.switchTo().newWindow(WindowType.WINDOW);
		String player3WindowHandle = chromeDriver.getWindowHandle();

		try {
			FrontEndUtils.playStreamWithBrowser(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId, "webrtc", null, false);
			fail("This test should fail due to webrtc viewer limit");
		}
		catch (Exception e) {
			
		}
		chromeDriver.switchTo().window(player3WindowHandle);
		chromeDriver.close();
		
		Utils.mustSleep(5000);
		
		//WebRTC player count should be still 2 due to limit
		assertEquals(2, RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount());

		chromeDriver.switchTo().window(player2WindowHandle);
		chromeDriver.close();

		
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 1);
		
		chromeDriver.switchTo().window(player1WindowHandle);
		chromeDriver.close();
		
		Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId).getWebRTCViewerCount() == 0);
		
		chromeDriver.switchTo().window(publisherHandle);
		chromeDriver.close();
		
		Awaitility.await().atMost(20, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
		until(() -> { 
			return  (RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId) == null);
		});
	}

}
