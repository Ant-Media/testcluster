package io.antmedia.test.standalone;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.AppSettings;
import io.antmedia.Config;
import io.antmedia.EncoderSettings;
import io.antmedia.datastore.db.types.Broadcast;
import io.antmedia.test.SetUpTest;
import io.antmedia.test.noneprivatenetwork.RestCommunicationTest;
import io.antmedia.test.utils.FrontEndUtils;
import io.antmedia.utils.Constants;
import io.antmedia.utils.FileInfo;
import io.antmedia.utils.FileManager;
import io.antmedia.utils.RestManager;
import io.antmedia.utils.Utils;

public class WebMRecordingTest extends SetUpTest{

	private static Logger log = LoggerFactory.getLogger(WebMRecordingTest.class);

	
	@BeforeClass
	public static void setUp() {
		setUpStandalone();

	}
	
	private ChromeDriver chromeDriver;
	
	@Before
	public void before() {
		assertTrue(RestManager.login(Config.STANDALONE_IP));
		chromeDriver = FrontEndUtils.getChromeDriver();

	}
	
	@After
	public void after() {
		chromeDriver.quit();

	}

	@Test
	public void testWebMRecordingVP8SFU() {
		String streamId = "TestWebrtcStream"+ (int)(Math.random() * 10000);
		String file = "src/test/resources/test.webm";
		String codec = "VP8";
		
		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setH264Enabled(false);
		settings.setVp8Enabled(true);
		settings.setEncoderSettings(new ArrayList<EncoderSettings>());
		settings.setMp4MuxingEnabled(false);
		settings.setWebMMuxingEnabled(true);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		startStopStream(streamId, file, codec, 10000);


		assertTrue(downloadFile(streamId+".webm"));
		FileInfo info1 = FileManager.getFileInfo(testFolder.getAbsolutePath()+"/"+streamId+".webm");
		assertTrue(info1.isExist);
	}

	@Test
	public void testWebMRecordingVP8ABR() {
		String streamId = "TestWebrtcStream"+ (int)(Math.random() * 10000);
		String file = "src/test/resources/test.webm";
		String codec = "VP8";
		
		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setH264Enabled(false);
		settings.setVp8Enabled(true);
		settings.setEncoderSettings(Arrays.asList(new EncoderSettings(240,500000,32000,true)));
		settings.setMp4MuxingEnabled(false);
		settings.setWebMMuxingEnabled(true);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		startStopStream(streamId, file, codec, 10000);

		assertTrue(downloadFile(streamId+"_240p500kbps.webm"));
		FileInfo info1 = FileManager.getFileInfo(testFolder.getAbsolutePath()+"/"+streamId+"_240p500kbps.webm");

		assertTrue(info1.isExist);
	}
	
	@Test
	public void testWebMRecordingH264ABR() {
		String streamId = "TestWebrtcStream"+ (int)(Math.random() * 10000);
		String file = "src/test/resources/test.mp4";
		String codec = "h264";
		
		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setH264Enabled(true);
		settings.setVp8Enabled(true);
		settings.setEncoderSettings(Arrays.asList(new EncoderSettings(240,500000,32000,true)));
		settings.setMp4MuxingEnabled(false);
		settings.setWebMMuxingEnabled(true);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		startStopStream(streamId, file, codec, 10000);
		
		assertTrue(downloadFile(streamId+"_240p500kbps.webm"));
		FileInfo info1 = FileManager.getFileInfo(testFolder.getAbsolutePath()+"/"+streamId+"_240p500kbps.webm");

		assertTrue(info1.isExist);
	}
	
	//In this case VP8 will not be enabled, so WebM will not be created
	@Test
	public void testWebMNotRecordingH264ABR() {
		String streamId = "TestWebrtcStream"+ (int)(Math.random() * 10000);
		String file = "src/test/resources/test.mp4";
		String codec = "h264";
		
		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setH264Enabled(true);
		settings.setVp8Enabled(false);
		settings.setEncoderSettings(Arrays.asList(new EncoderSettings(240,500000,32000,true)));
		settings.setMp4MuxingEnabled(false);
		settings.setWebMMuxingEnabled(true);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		startStopStream(streamId, file, codec, 10000);

		String url = "http://"+Config.STANDALONE_IP+":5080/"+Constants.WEBRTCAPPEE+"/streams/"+streamId+"_240p_500kbps.webm";
		assertFalse(isUrlExist(url));

	}
	
	//In this case WebM recording will not be enabled, so WebM will not be created
	@Test
	public void testWebMNotRecordingVP8SFU() {
		String streamId = "TestWebrtcStream"+ (int)(Math.random() * 10000);
		String file = "src/test/resources/test.webm";
		String codec = "VP8";
		
		AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
		settings.setH264Enabled(false);
		settings.setVp8Enabled(true);
		settings.setEncoderSettings(new ArrayList<EncoderSettings>());
		settings.setMp4MuxingEnabled(false);
		settings.setWebMMuxingEnabled(false);
		assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

		startStopStream(streamId, file, codec, 10000);

		String url = "http://"+Config.STANDALONE_IP+":5080/"+Constants.WEBRTCAPPEE+"/streams/"+streamId+".webm";
		assertFalse(isUrlExist(url));
	}
	
	
		//TODO: Migrate standalone tests to enterprise project. 
		//It is hard to debug and takes time to re-test
		@Test
		public void testWebMRecordingOnTheFly() {
			String streamId = "TestWebrtcStream"+ (int)(Math.random() * 10000);
			String file = "src/test/resources/test.webm";
			String codec = "VP8";
			
			AppSettings settings = RestManager.getSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE);
			settings.setH264Enabled(false);
			settings.setVp8Enabled(true);
			settings.setEncoderSettings(new ArrayList<EncoderSettings>());
			settings.setMp4MuxingEnabled(false);
			//WebM recording is disabled
			settings.setWebMMuxingEnabled(false);
			assertTrue(RestManager.changeSettings(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, settings));

			FrontEndUtils.publishStream(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId);
			
			Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> { 
				Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId); 
				if (broadcast != null) {
					return broadcast.isPublicStream();
				}
				return false;
			});

			Utils.mustSleep(5000);
			//Check there is no created WebM file so far
			String url = "http://"+Config.STANDALONE_IP+":5080/"+Constants.WEBRTCAPPEE+"/streams/"+streamId+".webm";
			String urlTempFile = "http://"+Config.STANDALONE_IP+":5080/"+Constants.WEBRTCAPPEE+"/streams/"+streamId+".webm.tmp_extension";
			assertFalse(isUrlExist(url));
			assertFalse(isUrlExist(urlTempFile));

			//Now enable WebM recording for the stream
			assertTrue(RestManager.setStreamRecording(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId, true, "webm", null));
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
				until(() -> isUrlExist(urlTempFile));
			assertFalse(isUrlExist(url));
			assertTrue(isUrlExist(urlTempFile));
			
			Utils.mustSleep(5000);

			//Now disable WebM recording for the stream and download
			assertTrue(RestManager.setStreamRecording(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId, false, "webm", null));
			Awaitility.await().atMost(10, TimeUnit.SECONDS).pollInterval(1, TimeUnit.SECONDS).
				until(() -> isUrlExist(url));
			
			assertFalse(isUrlExist(urlTempFile));
			assertTrue(isUrlExist(url));
			
			chromeDriver.close();

			Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
			until(() -> { 
				Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId); 
				return  (broadcast == null);
			});
			
			assertTrue(downloadFile(streamId+".webm"));
			FileInfo info2 = FileManager.getFileInfo(testFolder.getAbsolutePath()+"/"+streamId+".webm");
			assertTrue(info2.isExist);

		}

		private boolean isUrlExist(String url) {
			try {
				return ((HttpURLConnection) new URL(url).openConnection()).getResponseCode() == 200;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
	
	
	private void startStopStream(String streamId, String file, String codec, int duration) {
		
		FrontEndUtils.publishStream(chromeDriver, Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId);


		Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId); 
			if (broadcast != null) {
				return broadcast.isPublicStream();
			}
			return false;
		});

		Utils.mustSleep(duration);

		chromeDriver.close();

		Awaitility.await().atMost(40, TimeUnit.SECONDS).pollInterval(3, TimeUnit.SECONDS).
		until(() -> { 
			Broadcast broadcast = RestManager.getBroadcast(Config.STANDALONE_IP, Constants.WEBRTCAPPEE, streamId); 
			return  (broadcast == null);
		});
	}




	private boolean downloadFile(String fileName) {
		String url = "http://"+Config.STANDALONE_IP+":5080/"+Constants.WEBRTCAPPEE+"/streams/"+fileName;
		
		try {
			log.info("Checking url exists ->" + url);
			if(isUrlExist(url)) {
				log.info("Downloading url -> " + url);
				FileUtils.copyURLToFile(new URL(url), new File(testFolder, fileName), 5000, 10000);
				return true;
			}
			else {
				log.info("Url does not exist -> " + url);
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	
	/*
	 * We've moved test code to functional Tests in Enterprise project. 
	 * Because it's hard to debug test codes in cluster environment
	 */
	public void testWebMSync() {
		//it's moved to enterprise project
	}

}
