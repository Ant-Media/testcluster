./install_dependencies.sh
rclone copy "drive:ant-media-server-enterprise-latest.zip" .
docker build --network=host --file=Dockerfile_Server -t antmediaserver --build-arg AntMediaServer=ant*.zip --build-arg MongoDBServer=172.58.0.2:27017 .
docker build --network=host --file=Dockerfile_Server -t antmediaserverstandalone --build-arg AntMediaServer=ant*.zip .
docker build --network=host --file=Dockerfile_Mongo -t mongoserver .  
docker build --network=host --file=Dockerfile_HAProxy -t haproxy .  
#docker build --network=host --file=Dockerfile_NVIDIA -t amsgpu --build-arg AntMediaServer=ant*.zip .   

mvn test 