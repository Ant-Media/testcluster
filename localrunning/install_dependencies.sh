SUDO="sudo"
if ! [ -x "$(command -v sudo)" ]; then
  SUDO=""
fi

#install docker
if [ ! -x "$(command -v docker)" ]; then
  echo "Docker is installing now..."
  $SUDO apt-get install apt-transport-https ca-certificates curl software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | $SUDO apt-key add -
  $SUDO add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  $SUDO apt-get update
  $SUDO apt-get install docker-ce -y
else
  echo "Docker is already installed!"
fi

#install ffmpeg
if [ ! -x "$(command -v ffmpeg)" ]; then
  echo "FFmpeg is installing now..."
  $SUDO apt-get install ffmpeg -y
else
  echo "FFmpeg is already installed!"
fi

#install git
if [ ! -x "$(command -v git)" ]; then
  echo "git is installing now..."
  $SUDO apt-get install git -y
else
  echo "git is already installed!"
fi

#install Java
if [ ! -x "$(command -v javac)" ]; then
  echo "Java is installing now..."
  $SUDO apt-get install openjdk-8-jdk -y
else
  echo "Java is already installed!"
fi

#install Maven
if [ ! -x "$(command -v mvn)" ]; then
  echo "Maven is installing now..."
  $SUDO apt-get install maven -y
else
  echo "Maven is already installed!"
fi

#install curl
if [ ! -x "$(command -v curl)" ]; then
  echo "curl is installing now..."
  $SUDO apt-get install curl -y
else
  echo "curl is already installed!"
fi

#install unzip
if [ ! -x "$(command -v unzip)" ]; then
  echo "unzip is installing now..."
  $SUDO apt-get install unzip -y
else
  echo "unzip is already installed!"
fi

#install rclone
if [ ! -x "$(command -v rclone)" ]; then
  echo "rclone is installing now..."
  $SUDO curl https://rclone.org/install.sh | $SUDO bash
else
  echo "rclone is already installed!"
fi

#download test tools
if [ ! -x /usr/local/test ]; then
  echo "Test tools are downloading..."
  $SUDO mkdir /usr/local/test
  $SUDO mkdir /usr/local/test/temp
  cd /usr/local/test


else
  echo "Test tools are already downloaded!"
fi

#install nvidia docker
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
  $SUDO apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
  $SUDO tee /etc/apt/sources.list.d/nvidia-docker.list
$SUDO apt-get update

# Install nvidia-docker2 and reload the Docker daemon configuration
$SUDO apt-get install -y nvidia-docker2
$SUDO pkill -SIGHUP dockerd

#restart docker
$SUDO systemctl daemon-reload
$SUDO systemctl restart docker
